#include "gd32e501_i2c.h"
#include "CMIS_MSA.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "CMIS_MSA.h"
#include "BRCM_DSP87540_RW.h"
#include <string.h>
#include "GD_FlahMap.h"
#include "BRCM_DSP_87540.h"
#include "Macom_TIA_03819.h"
#include "LDD_Control.h"

//-----------------------------------------------------------------------------
// Page Select define
//-----------------------------------------------------------------------------
#define BL_ERASE_PAGE0     0x08000000

#define LDD_Ccontrol       0x82
#define MA03819_R14        0x8A

#define Calibration        0x90
#define CTLE_EM            0x91
#define Calibration_1      0x92
#define BRCM_87540		   0x85
#define BRCM_87540_LS_PHY0 0x86
#define BRCM_87540_SS_PHY0 0x87

#define ID_Debug       	   0xB0
#define BL_MODE            0xD0

#define Page0              0x00
#define Page1              0x01
#define Page2              0x02
#define Page3              0x03

#define Page10             0x10
#define Page11             0x11
#define Page13             0x13

#define LUT_BIAS0          0xA0
#define LUT_BIAS1          0xA1
#define LUT_BIAS2          0xA2
#define LUT_BIAS3          0xA3

#define LUT_MOD0           0xA8
#define LUT_MOD1           0xA9
#define LUT_MOD2           0xAA
#define LUT_MOD3           0xAB

// Luxshare-ICT Password
#define Leve1_PS0   0x93
#define Leve1_PS1   0x78
#define Leve1_PS2   0xCC
#define Leve1_PS3   0xAE

#define Leve2_PS0   0x3A
#define Leve2_PS1   0x18
#define Leve2_PS2   0xB6
#define Leve2_PS3   0x6F

//-----------------------------------------------------------------------------
// ID Debug Info table define
//-----------------------------------------------------------------------------
uint8_t ID_Debug_Info[9] =
{
    'G','D','3','2','E','5','0','1',' ',     // MCU                         // 00,01,02,03,04,05,06,07,08
};
#define ID_Debug_Info_SIZE sizeof(ID_Debug_Info)

uint8_t I2C_Data_Buffer[256] ;
uint8_t MSA_Memory_ADR = 0 ;
uint8_t Data_count_I2C0 = 0;
uint8_t Slave_RW_STOP_Flag = 2 ;
uint8_t HOSTPASSWROD = 0;
uint8_t UPDATE_FLAG;
uint8_t DATA_LEGHT ;
uint8_t I2C_READY_FLG = 0 ;
uint8_t configaddress = 0;

uint8_t Clear_flag = 0 ;
uint8_t Clear_ADR  = 0 ;
uint8_t Clear_Correct_ADR = 0 ;
//-----------------------------------------------------------------------------
// Update SRAM Data Value
//-----------------------------------------------------------------------------
void PassWord_Function()
{
	if ( ( QSFPDD_A0[122] == PW_LEVE1[0] ) &&
		 ( QSFPDD_A0[123] == PW_LEVE1[1] ) &&
		 ( QSFPDD_A0[124] == PW_LEVE1[2] ) &&
		 ( QSFPDD_A0[125] == PW_LEVE1[3] ) )
		HOSTPASSWROD = 1;
	else if(( QSFPDD_A0[122] == Leve2_PS0 ) &&
		    ( QSFPDD_A0[123] == Leve2_PS1 ) &&
		    ( QSFPDD_A0[124] == Leve2_PS2 ) &&
		    ( QSFPDD_A0[125] == Leve2_PS3 ) )
		HOSTPASSWROD = 2;
	else
		HOSTPASSWROD = 0;
}

//-----------------------------------------------------------------------------
// Update SRAM Data Value 
//-----------------------------------------------------------------------------
void Update_SRAM_Data()
{
	switch(QSFPDD_A0[0x7F])
	{

		case LDD_Ccontrol :
					if( HOSTPASSWROD >= 1 )
                    {
                        //GDMCU_FMC_READ_FUNCTION( FS_VCSEL_P82 , &LDD_Control_MEMORY_MAP.LDD_Control_CHIPID_MSB , 128 );
                        ADC_Heater_Mon();
                        ADC_EAM_CH1_CH4_Mon();
                        memcpy(  &QSFPDD_A0[128] , &LDD_Control_MEMORY_MAP , 128 );
                    }
					break;

		case BRCM_87540 :
					if( HOSTPASSWROD > 1 )
					{
						memcpy(  &QSFPDD_A0[128] , &BRCM_D87540_DSP_MEMORY_MAP.BRCM_REG_ADDR_0 , 128 );
					}
					break;

		case BRCM_87540_LS_PHY0 :
					if( HOSTPASSWROD >= 1 )
					{
						// Read DSP Data Value
						memcpy(  &QSFPDD_A0[128] , &BRCM_87540_DSP_LS_PHY0_MEMORY_MAP , 128 );
					}
					break;

		case BRCM_87540_SS_PHY0 :
					if( HOSTPASSWROD >= 1 )
					{
						// Read DSP Data Value
						memcpy(  &QSFPDD_A0[128] , &BRCM_87540_DSP_SS_PHY0_MEMORY_MAP , 128 );
					}
					break;

		case MA03819_R14 :
					if( HOSTPASSWROD >= 1 )
					{
						MATA03819_P8A_READ();
						memcpy(  &QSFPDD_A0[128] , &MATA03819_TIA_MEMORY_MAP , 128 );
					}
					break;

		case Calibration :

					if( HOSTPASSWROD > 1 )
						memcpy( &QSFPDD_A0[128] , &CALIB_MEMORY_MAP, 128);
					else
						memset( &QSFPDD_A0[128] , 0xff , 128 ) ;

					break;

		case Calibration_1 :
					if( HOSTPASSWROD > 1 )
                        memcpy( &QSFPDD_A0[128] , &CALIB_MEMORY_1_MAP, 128);
					else
						memset( &QSFPDD_A0[128] , 0xff , 128 ) ;
                    
					break;

		case CTLE_EM :
					if( HOSTPASSWROD > 1 )
						GDMCU_FMC_READ_FUNCTION( FS_MSA_O_P91 , &QSFPDD_A0[128], 128 );
					
					break;
                    
        case LUT_BIAS0:
					if( HOSTPASSWROD > 1 )
						GDMCU_FMC_READ_FUNCTION( FS_Ibias0_PA0 , &QSFPDD_A0[128], 128 );            
        
                    break;
                    
        case LUT_BIAS1:
					if( HOSTPASSWROD > 1 )
						GDMCU_FMC_READ_FUNCTION( FS_Ibias1_PA1 , &QSFPDD_A0[128], 128 );            
        
                    break;
  
        case LUT_BIAS2:
					if( HOSTPASSWROD > 1 )
						GDMCU_FMC_READ_FUNCTION( FS_Ibias2_PA2 , &QSFPDD_A0[128], 128 );            
        
                    break;
                
        case LUT_BIAS3:
					if( HOSTPASSWROD > 1 )
						GDMCU_FMC_READ_FUNCTION( FS_Ibias3_PA3 , &QSFPDD_A0[128], 128 );            
        
                    break;   

        case LUT_MOD0:
					if( HOSTPASSWROD > 1 )
						GDMCU_FMC_READ_FUNCTION( FS_IMod0_PA8 , &QSFPDD_A0[128], 128 );            
        
                    break;       

        case LUT_MOD1:
					if( HOSTPASSWROD > 1 )
                        GDMCU_FMC_READ_FUNCTION( FS_IMod1_PA9 , &QSFPDD_A0[128], 128 );            
        
                    break;                    
                    
        case LUT_MOD2:
					if( HOSTPASSWROD > 1 )
						GDMCU_FMC_READ_FUNCTION( FS_IMod2_PAA , &QSFPDD_A0[128], 128 );            
        
                    break;
                    
        case LUT_MOD3:
					if( HOSTPASSWROD > 1 )
						GDMCU_FMC_READ_FUNCTION( FS_IMod3_PAB , &QSFPDD_A0[128], 128 );            
        
                    break;                    
                    
		case ID_Debug :
					if( HOSTPASSWROD > 1 )
					{
						memcpy( &QSFPDD_A0[128] , &ID_Debug_Info , 9 );
				    	GDMCU_FMC_READ_FUNCTION( ( FS_ID_INFO_PB0+9 ) , &QSFPDD_A0[137], 119 );
					}
					else
						memset( &QSFPDD_A0[128] , 0xff , 128 ) ;

					break;
		default :
						memset( &QSFPDD_A0[128] , 0xff , 128 ) ;
			     break;
	}
}

//-----------------------------------------------------------------------------
// Write SRAM to Flash Value
//-----------------------------------------------------------------------------

void Write_SRAM_TO_Flash()
{
	switch(QSFPDD_A0[0x7F])
	{
		case Page0 :
					if( HOSTPASSWROD >= 1 )
					{
                        GDMCU_Flash_Erase(FS_QSFPDD_A0);
						GDMCU_FMC_BytesWRITE_FUNCTION( FS_QSFPDD_A0 , &QSFPDD_A0[0]   , 128 );
                        GDMCU_Flash_Erase(FS_QSFPDD_P0);
						GDMCU_FMC_BytesWRITE_FUNCTION( FS_QSFPDD_P0 , &QSFPDD_A0[128] , 128 );
                        memcpy(  &QSFPDD_P0[0] , &QSFPDD_A0[128] , 128 );
					}
					break;
		case Page1 :
					if( HOSTPASSWROD >= 1 )
					{
                        GDMCU_Flash_Erase(FS_QSFPDD_P1);
						GDMCU_FMC_BytesWRITE_FUNCTION( FS_QSFPDD_P1 , &QSFPDD_A0[128] , 128 );
						memcpy(  &QSFPDD_P1[0] , &QSFPDD_A0[128] , 128 );
					}
					break;

		case Page2 :
					if( HOSTPASSWROD >= 1 )
					{
                        GDMCU_Flash_Erase(FS_QSFPDD_P2);
						GDMCU_FMC_BytesWRITE_FUNCTION( FS_QSFPDD_P2 , &QSFPDD_A0[128]  , 128 );
						memcpy(  &QSFPDD_P2[0] , &QSFPDD_A0[128] , 128 );
					}

					break;

		case Page10 :
					memcpy(  &QSFPDD_P10[0] , &QSFPDD_A0[128] , 128 );
					break;

		case Page13 :
					 memcpy(  &QSFPDD_P13[0] , &QSFPDD_A0[128] , 128 );
					 break;

		case LDD_Ccontrol :
					if( HOSTPASSWROD > 1 )
                    {
                        memcpy(  &LDD_Control_MEMORY_MAP , &QSFPDD_A0[128] , 128 );
                        SET_DAC_ALLCH_Value();
                        SET_Bias_Current_ALLCH();
                        GDMCU_Flash_Erase(FS_VCSEL_P82);
						GDMCU_FMC_BytesWRITE_FUNCTION( FS_VCSEL_P82 , &LDD_Control_MEMORY_MAP.LDD_Control_CHIPID_MSB  , 128 );
                    }
					break;

		case BRCM_87540 :
					if( HOSTPASSWROD > 1 )
						memcpy(  &BRCM_D87540_DSP_MEMORY_MAP.BRCM_REG_ADDR_0 , &QSFPDD_A0[128] , 128 );
		
					break ;

		case BRCM_87540_LS_PHY0 :
					if( HOSTPASSWROD > 1 )
					{
						// Line-side save flash form QSFPDD_A0 byte 128 - 256
                        GDMCU_Flash_Erase(FS_DSP_LS_P86);
						GDMCU_FMC_BytesWRITE_FUNCTION( FS_DSP_LS_P86 , &QSFPDD_A0[128] , 128 );
						memcpy(  &BRCM_87540_DSP_LS_PHY0_MEMORY_MAP , &QSFPDD_A0[128] , 128 );
					}
					break;

		case BRCM_87540_SS_PHY0 :
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_DSP_SS_P87);
						GDMCU_FMC_BytesWRITE_FUNCTION( FS_DSP_SS_P87 , &QSFPDD_A0[128], 128 );
						memcpy(  &BRCM_87540_DSP_SS_PHY0_MEMORY_MAP , &QSFPDD_A0[128] , 128 );

					}
					break;

		case MA03819_R14 :
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_TIA_P8A);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_TIA_P8A , &QSFPDD_A0[128] , 128 );
                        memcpy( &MATA03819_TIA_MEMORY_MAP , &QSFPDD_A0[128], 128 );
                        MATA03819_P8A_Write();
					}
					break;

		case Calibration :
					if( HOSTPASSWROD > 1 )
					{
						memcpy( &QSFPDD_A0[248] , &CALIB_MEMORY_MAP.CHECKSUM_V, 8);
                        GDMCU_Flash_Erase(FS_Cal0_P90);
						GDMCU_FMC_BytesWRITE_FUNCTION( FS_Cal0_P90 , &QSFPDD_A0[128] , 128 );
						GDMCU_FMC_READ_FUNCTION( FS_Cal0_P90 , &CALIB_MEMORY_MAP.VCC_SCALEM ,128 );
					}
					break ;

		case Calibration_1 :
					if( HOSTPASSWROD > 1 )
					{
						memcpy( &CALIB_MEMORY_1_MAP , &QSFPDD_A0[128] , 128);
					}
					break ;

	    case CTLE_EM :
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_MSA_O_P91);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_MSA_O_P91 ,&QSFPDD_A0[128], 128 );
                    }
					break ;
                    
        case LUT_BIAS0:
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_Ibias0_PA0);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_Ibias0_PA0 ,&QSFPDD_A0[128], 128 );
                    }            
                    break;

        case LUT_BIAS1:
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_Ibias1_PA1);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_Ibias1_PA1 ,&QSFPDD_A0[128], 128 );
                    }            
                    break;
                    
        case LUT_BIAS2:
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_Ibias2_PA2);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_Ibias2_PA2 ,&QSFPDD_A0[128], 128 );
                    }            
                    break;  

        case LUT_BIAS3:
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_Ibias3_PA3);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_Ibias3_PA3 ,&QSFPDD_A0[128], 128 );
                    }            
                    break;  

        case LUT_MOD0:
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_IMod0_PA8);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_IMod0_PA8 ,&QSFPDD_A0[128], 128 );
                    }            
                    break;  

        case LUT_MOD1:
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_IMod1_PA9);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_IMod1_PA9 ,&QSFPDD_A0[128], 128 );
                    }            
                    break; 

        case LUT_MOD2:
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_IMod2_PAA);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_IMod2_PAA ,&QSFPDD_A0[128], 128 );
                    }            
                    break;      

        case LUT_MOD3:
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_IMod3_PAB);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_IMod3_PAB ,&QSFPDD_A0[128], 128 );
                    }            
                    break;                    

		case ID_Debug :
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_ID_INFO_PB0);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_ID_INFO_PB0 , &QSFPDD_A0[128] , 128 );
                    }
					break;
		case BL_MODE :
					if( HOSTPASSWROD > 1 )
					{
                        if( MSA_Memory_ADR == 0x24 )
						{
                            if( (I2C_Data_Buffer[0]==0x11) && (I2C_Data_Buffer[1]==0x22) && (I2C_Data_Buffer[2]==0x33) &&
                                (I2C_Data_Buffer[3]==0x55) && (I2C_Data_Buffer[4]==0x66) && (I2C_Data_Buffer[5]==0x55)	)
                            {
                                //i2c_disable(I2C0);   
                                fwdgt_config( 10 , FWDGT_PSC_DIV64 );
                                fwdgt_enable();                                
                                GDMCU_Flash_Erase(BL_ERASE_PAGE0);
                                //nvic_system_reset();
                            } 
                        }
						
					}
					break;
		default :
			     break;
	}
}

/*!
    \brief      handle I2C0 event interrupt request
    \param[in]  none
    \param[out] none
    \retval     none
*/
void I2C0_EV_IRQHandler(void)
{
    // slave address get pass flag 
    if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_ADDSEND))
    {
        /* clear the ADDSEND bit */
        i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_ADDSEND);  
        //configaddress = i2c_recevied_address_get(I2C0)<<1;         
        /* clear I2C_TDATA register */
        I2C_STAT(I2C0) |= I2C_STAT_TBE;
    }
    // Save data / move data 
    else if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_RBNE))
    {
        if(Data_count_I2C0 == 0)
            MSA_Memory_ADR = i2c_data_receive(I2C0);
        else
            I2C_Data_Buffer[Data_count_I2C0-1] = i2c_data_receive(I2C0);

    //    Slave_RW_STOP_Flag = 0 ;
        Data_count_I2C0 ++ ;
    }
    // send data to I2C
    else if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_TI))
    {
        if(MSA_Memory_ADR<128)
        {
            if(( MSA_Memory_ADR > 117 ) && ( MSA_Memory_ADR < 127 ))
                QSFPDD_A0[MSA_Memory_ADR] = 0x00;

            i2c_data_transmit(I2C0,QSFPDD_A0[MSA_Memory_ADR]);
            // ISR into 2 time so Correct address must to be -1
            Clear_Correct_ADR = (MSA_Memory_ADR-1);
            if( Clear_Correct_ADR == 8 )
                Clear_Module_state_Byte8();
            else if( Clear_Correct_ADR == 9 )
                Clear_VCC_TEMP_Flag();     
                  
            MSA_Memory_ADR++;
        }
        else
        {
            if(QSFPDD_A0[127]==0x00)
                i2c_data_transmit(I2C0,QSFPDD_P0[MSA_Memory_ADR-128]);
            else if(QSFPDD_A0[127]==0x01)
                i2c_data_transmit(I2C0,QSFPDD_P1[MSA_Memory_ADR-128]);
            else if(QSFPDD_A0[127]==0x02)
                i2c_data_transmit(I2C0,QSFPDD_P2[MSA_Memory_ADR-128]);
            else if(QSFPDD_A0[127]==0x10)
                i2c_data_transmit(I2C0,QSFPDD_P10[MSA_Memory_ADR-128]);
            else if(QSFPDD_A0[127]==0x11)
            {
                i2c_data_transmit(I2C0,QSFPDD_P11[MSA_Memory_ADR-128]);
                
                Clear_Correct_ADR = (MSA_Memory_ADR-1);
                if( ( Clear_Correct_ADR >= 134 ) && ( Clear_Correct_ADR <= 152 ) )
                    Clear_Flag( Clear_Correct_ADR-128 );
            }
            else if(QSFPDD_A0[127]==0x13)
                i2c_data_transmit(I2C0,QSFPDD_P13[MSA_Memory_ADR-128]);
            else if(QSFPDD_A0[127]==0x14)
                i2c_data_transmit(I2C0,QSFPDD_P14[MSA_Memory_ADR-128]);
            else
                i2c_data_transmit(I2C0,QSFPDD_A0[MSA_Memory_ADR]);
            MSA_Memory_ADR++;
        }         
    //    Slave_RW_STOP_Flag = 1 ;
    }
    // Stop flag clear
    else if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_STPDET))
    {
        if( Data_count_I2C0 >= 2 )
        {
            I2C_READY_FLG = 1 ;
            DATA_LEGHT = ( Data_count_I2C0 - 1 );
            if(( MSA_Memory_ADR == 0x7F ) && (Data_count_I2C0 == 2))
            {
                QSFPDD_A0[127]=I2C_Data_Buffer[0];
                
                if(QSFPDD_A0[127]==0x00)
                {
                    memcpy(  &QSFPDD_A0[128] , &QSFPDD_P0[0] , 128 );
                    UPDATE_FLAG = 0 ;
                }
                else if(QSFPDD_A0[127]==0x01)
                {
                    memcpy(  &QSFPDD_A0[128] , &QSFPDD_P1[0] , 128 );
                    UPDATE_FLAG = 0 ;
                }
                else if(QSFPDD_A0[127]==0x02)
                {
                    memcpy(  &QSFPDD_A0[128] , &QSFPDD_P2[0] , 128 );
                    UPDATE_FLAG = 0 ;
                }
                else if(QSFPDD_A0[127]==0x10)
                {
                    memcpy(  &QSFPDD_A0[128] , &QSFPDD_P10[0] , 128 );
                    UPDATE_FLAG = 0 ;
                }
                else if(QSFPDD_A0[127]==0x11)
                {
                    memcpy(  &QSFPDD_A0[128] , &QSFPDD_P11[0] , 128 );
                    UPDATE_FLAG = 0 ;
                }
                else if(QSFPDD_A0[127]==0x13)
                {
                    memcpy(  &QSFPDD_A0[128] , &QSFPDD_P13[0] , 128 );
                    UPDATE_FLAG = 0 ;
                }
                else if(QSFPDD_A0[127]==0x14)
                {
                    memcpy(  &QSFPDD_A0[128] , &QSFPDD_P14[0] , 128 );
                    UPDATE_FLAG = 0 ;
                }
                //For Password auto check use on gui
                else if(QSFPDD_A0[127]==0x90)
                {
                    UPDATE_FLAG = 1;
                }
                else
                {
                    if(HOSTPASSWROD>1)
                    {
                        UPDATE_FLAG = 1;
                    }
                    else
                    {
                        GDMCU_FMC_READ_FUNCTION( FS_QSFPDD_P0 , &QSFPDD_P0[0] , 128 );
                        QSFPDD_A0[127]=0x00;
                        UPDATE_FLAG = 0;
                    }
                }

            }
            else if( ( MSA_Memory_ADR >= 122 ) && ( MSA_Memory_ADR <= 125 ))
                UPDATE_FLAG = 5;
            else
            {
                if( HOSTPASSWROD >= 1 )
                    memcpy(&QSFPDD_A0[MSA_Memory_ADR], &I2C_Data_Buffer[0], DATA_LEGHT);
                else
                {
                    if(MSA_Memory_ADR==26)
                        QSFPDD_A0[MSA_Memory_ADR] = I2C_Data_Buffer[0];
                    else if(MSA_Memory_ADR==31)
                        QSFPDD_A0[MSA_Memory_ADR] = ( I2C_Data_Buffer[0] & 0x01 );
                    else if(MSA_Memory_ADR==32)
                        QSFPDD_A0[MSA_Memory_ADR] = I2C_Data_Buffer[0];
                    else if(MSA_Memory_ADR==33)
                        QSFPDD_A0[MSA_Memory_ADR] = I2C_Data_Buffer[0];
                    else if(MSA_Memory_ADR==34)
                        QSFPDD_A0[MSA_Memory_ADR] = I2C_Data_Buffer[0];
                }

                if(QSFPDD_A0[127]==0x10)
                {
                    if(MSA_Memory_ADR>127)
                    {
                        memcpy(&QSFPDD_A0[MSA_Memory_ADR], &I2C_Data_Buffer[0], DATA_LEGHT);
                        memcpy(  &QSFPDD_P10[0] , &QSFPDD_A0[128] , 128 );
                    }
                }
                else if(QSFPDD_A0[127]==0x13)
                {
                    if(MSA_Memory_ADR>143)
                    {
                        memcpy(&QSFPDD_A0[MSA_Memory_ADR], &I2C_Data_Buffer[0], DATA_LEGHT);
                        memcpy(  &QSFPDD_P13[16] , &QSFPDD_A0[144] , 111 );
                    }
                }
                else
                    UPDATE_FLAG = 7 ;
            }
        }
        /* clear STPDET interrupt flag */
        i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_STPDET);
        Data_count_I2C0 = 0 ;
        //Slave_RW_STOP_Flag = 2 ;
    }
}

/*!
    \brief      handle I2C0 error interrupt request
    \param[in]  none
    \param[out] none
    \retval     none
*/
void I2C0_ER_IRQHandler(void)
{
    /* bus error */
    if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_BERR)){
        i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_BERR);
        i2c_deinit(I2C0);
    }

    /* arbitration lost */
    if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_LOSTARB)){
        i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_LOSTARB);
    }

    /* over-run or under-run when SCL stretch is disabled */
    if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_OUERR)){
        i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_OUERR);
    }

    /* PEC error */
    if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_PECERR)){
       i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_PECERR);
    }

    /* timeout error */
    if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_TIMEOUT)){
        i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_TIMEOUT);
        i2c_deinit(I2C0);
    }

    /* SMBus alert */
    if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_SMBALT)){
        i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_SMBALT);
    }

    /* disable the I2C0 interrupt */
    //i2c_interrupt_disable(I2C0, I2C_INT_ERR | I2C_INT_STPDET | I2C_INT_TI | I2C_INT_TC);
}


void SRMA_Flash_Function()
{
	uint16_t Data_count ;

	if( I2C_READY_FLG==1 )
	{
		switch(UPDATE_FLAG)
		{
			case 1 :
				    Update_SRAM_Data();
					break ;

			case 5 :
					for( Data_count=0 ; Data_count < DATA_LEGHT ; Data_count++ )
						QSFPDD_A0[ ( MSA_Memory_ADR + Data_count ) ] = I2C_Data_Buffer[Data_count];
					PassWord_Function();
					break ;

			case 7 :
					Write_SRAM_TO_Flash();
					break ;
			default :
				break ;
		}
		I2C_READY_FLG = 0 ;
	}

}
