#include "gd32e501.h"
#include "core_cm33.h"
#include "string.h"
#include "GD_FlahMap.h"
#include "BRCM_DSP_87540.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "CMIS_MSA.h"
#include "Macom_TIA_03819.h"
#include "LDD_Control.h"

#define MSA_Version  0x40
uint16_t MCU_FW_VERSION = 0xA103;

uint8_t QSFPDD_A0[256]={0};
uint8_t QSFPDD_P0[128]={0};
uint8_t QSFPDD_P1[128]={0};
uint8_t QSFPDD_P2[128]={0};
uint8_t QSFPDD_P3[128]={0};
uint8_t QSFPDD_P10[128]={0};
uint8_t QSFPDD_P11[128]={0};
uint8_t QSFPDD_P13[128]={0};
uint8_t QSFPDD_P14[128]={0};
uint8_t CTEL_Table[16];
uint8_t Pre_Table[16];
uint8_t OP_AMP_Table[16];
uint8_t Post_Table[16];
uint8_t PW_LEVE1[4];
uint8_t	bef_Pre_Cursor_RX1;
uint8_t	bef_Pre_Cursor_RX2;
uint8_t	bef_Pre_Cursor_RX3;
uint8_t	bef_Pre_Cursor_RX4;
uint8_t	bef_Post_Cursor_RX1;
uint8_t	bef_Post_Cursor_RX2;
uint8_t	bef_Post_Cursor_RX3;
uint8_t	bef_Post_Cursor_RX4;
uint8_t	bef_AMP_C_RX1;
uint8_t	bef_AMP_C_RX2;
uint8_t	bef_AMP_C_RX3;
uint8_t	bef_AMP_C_RX4;
// Tx Squelch is disable
uint8_t Bef_Tx_output;
// Rx output is enable
uint8_t Bef_Rx_output;
// Tx Disable is Power up
uint8_t Bef_TxDIS_Power = 0xFF;

uint8_t Exended_RateS_Version = 2 ;

struct CALIB_MEMORY CALIB_MEMORY_MAP;
struct CALIB_MEMORY_1 CALIB_MEMORY_1_MAP;

void Get_NRZ_Default_System_Side()
{
    // RX Pre-EM
    QSFPDD_P10[34] = 0x33 ;
    QSFPDD_P10[35] = 0x33 ;

    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH0=0xF4FF;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH1=0xF4FF;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH2=0xF4FF;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH3=0xF4FF;

    // RX Post-EM
    QSFPDD_P10[38] = 0x00 ;
    QSFPDD_P10[39] = 0x00 ;

    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH0 = 0x0000;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH1 = 0x0000;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH2 = 0x0000;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH3 = 0x0000;

    // Rx AMP
    QSFPDD_P10[42] = 0x22 ;
    QSFPDD_P10[43] = 0x22 ;

    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH0 = 0x7000;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH1 = 0x7000;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH2 = 0x7000;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH3 = 0x7000;
}
//----------------------------------------------//
// Channel 0 , 2 , 4 , 6 on Bit0-3              //
// Channel 1 , 3 , 5 , 7 on Bit4-7              //
//----------------------------------------------//
uint8_t Get_CH_Data (uint8_t Data,uint8_t CH)
{
	uint8_t Data_Buffer = 0;
	if( CH%2 == 0 )
		Data_Buffer = ( Data & 0x0F ) ;
	else
		Data_Buffer = ( ( Data & 0xF0 ) >>4 ) ;
	
	return Data_Buffer;
}

void CheckSum_Calculate()
{
	uint16_t TEMP_BUF=0;
	uint32_t IIcount;
    uint16_t CHECKSUM = 0;

	for( IIcount=0x08000000 ; IIcount < MCU_Code_End ; IIcount++ )
	{
		TEMP_BUF = GDMCU_FMC_READ_DATA(IIcount);
		CHECKSUM = CHECKSUM + TEMP_BUF ;
	}
	CALIB_MEMORY_MAP.CHECKSUM_V = Swap_Bytes( CHECKSUM );
	CALIB_MEMORY_MAP.FW_VERSION = Swap_Bytes(MCU_FW_VERSION); 
    CALIB_MEMORY_MAP.CheckSum_EN = 0x00 ;
    GDMCU_Flash_Erase(FS_Cal0_P90);
	GDMCU_FMC_BytesWRITE_FUNCTION( FS_Cal0_P90  , &CALIB_MEMORY_MAP.VCC_SCALEM    , 128 );
}

void Init_Rx_Output_Values ()
{
	uint8_t Data_Buffer[128];
    
    //PAM4 Mode
    if (Signal_Status==PAM4_to_PAM4)
    {
        // RX Pre-EM
        QSFPDD_P10[34] = CALIB_MEMORY_MAP.R12_Pre_Level ;
        QSFPDD_P10[35] = CALIB_MEMORY_MAP.R34_Pre_Level ;
        
        // RX Post-EM
        QSFPDD_P10[38] = CALIB_MEMORY_MAP.R12_Post_Level ;
        QSFPDD_P10[39] = CALIB_MEMORY_MAP.R34_Post_Level ;

        // Rx AMP
        QSFPDD_P10[42] = CALIB_MEMORY_MAP.R12_Swining_Level_2 ;
        QSFPDD_P10[43] = CALIB_MEMORY_MAP.R34_Swining_Level_2 ;

        // DSP Line-side & System-side CH0 - CH7
        GDMCU_FMC_READ_FUNCTION( FS_DSP_LS_P86        , &Data_Buffer[0] , 128 );
        memcpy(  &BRCM_87540_DSP_LS_PHY0_MEMORY_MAP   , &Data_Buffer[0] , 128 );
        GDMCU_FMC_READ_FUNCTION( FS_DSP_SS_P87        , &Data_Buffer[0] , 128 );
        memcpy(  &BRCM_87540_DSP_SS_PHY0_MEMORY_MAP   , &Data_Buffer[0] , 128 );
    }
    //NRZ Mode Values by default
    else
    {
        Get_NRZ_Default_System_Side();
    }

	QSFPDD_P11[95] = QSFPDD_P10[34];
	QSFPDD_P11[96] = QSFPDD_P10[35];

	QSFPDD_P11[99]  = QSFPDD_P10[38];
	QSFPDD_P11[100] = QSFPDD_P10[39];

	QSFPDD_P11[103] = QSFPDD_P10[42];
	QSFPDD_P11[104] = QSFPDD_P10[43];
}

void QSFPDD_Table_GetDate()
{
	GDMCU_FMC_READ_FUNCTION( FS_QSFPDD_A0 , &QSFPDD_A0[0] , 128 );
    GDMCU_FMC_READ_FUNCTION( FS_QSFPDD_P0 , &QSFPDD_P0[0] , 128 );
	GDMCU_FMC_READ_FUNCTION( FS_QSFPDD_P1 , &QSFPDD_P1[0] , 128 );
	GDMCU_FMC_READ_FUNCTION( FS_QSFPDD_P2 , &QSFPDD_P2[0] , 128 );
}

void Get_QDD_MSA_Optional_Control_Table()
{
	GDMCU_FMC_READ_FUNCTION(   FS_MSA_O_P91      , &CTEL_Table[0]   , 16 );
	GDMCU_FMC_READ_FUNCTION( ( FS_MSA_O_P91+16 ) , &Pre_Table[0]    , 16 );
	GDMCU_FMC_READ_FUNCTION( ( FS_MSA_O_P91+48 ) , &PW_LEVE1[0]     , 4  );
	GDMCU_FMC_READ_FUNCTION( ( FS_MSA_O_P91+32 ) , &OP_AMP_Table[0] , 16 );
	GDMCU_FMC_READ_FUNCTION( ( FS_MSA_O_P91+64 ) , &Post_Table[0]   , 16 );
}

void Device_Table_Get_Data()
{
	uint8_t Temp_Buffer[128];
	// Tx LDD AFSI_N74C4S
	GDMCU_FMC_READ_FUNCTION( FS_VCSEL_P82  , &LDD_Control_MEMORY_MAP.LDD_Control_CHIPID_MSB     , 128 );
	// Rx TIA MATA38434
	GDMCU_FMC_READ_FUNCTION( FS_TIA_P8A    , &MATA03819_TIA_MEMORY_MAP.CHIP_ID , 128 );
	// DSP Line-side & System-side CH0 - CH7
	GDMCU_FMC_READ_FUNCTION( FS_DSP_LS_P86        , &Temp_Buffer[0] , 128 );
	memcpy(  &BRCM_87540_DSP_LS_PHY0_MEMORY_MAP   , &Temp_Buffer[0] , 128 );
	GDMCU_FMC_READ_FUNCTION( FS_DSP_SS_P87        , &Temp_Buffer[0] , 128 );
	memcpy(  &BRCM_87540_DSP_SS_PHY0_MEMORY_MAP   , &Temp_Buffer[0] , 128 );
	GDMCU_FMC_READ_FUNCTION( FS_Cal0_P90          , &CALIB_MEMORY_MAP.VCC_SCALEM , 128 );
}


void QDD_MSA_Init_SETTING()
{
	memset(&QSFPDD_A0, 0x00, 84);
	// ID
	QSFPDD_A0[0] = QSFPDD_P0[0];
	// MSA Version 4.0 = 40h
	QSFPDD_A0[1] = MSA_Version ;
	// MSA IntL bit
	QSFPDD_A0[2] = 0x00 ;
	// MSA Module Status
	QSFPDD_A0[3] = 0x00 ;
	// MSA module Type Advertising code
	// 01 : MMF
	//QSFPDD_A0[85] = 0x04 ;
	//QSFPDD_A0[86] = 0x0B ;
	//QSFPDD_A0[87] = 0x01 ;
	//QSFPDD_A0[88] = 0x88 ;
	//QSFPDD_A0[89] = 0x01 ;

    // bit4 Force LowPwr = 1
	// Bit6 LowPwr = 1
	QSFPDD_A0[26] = 0x50 ;
}

void QDD_MSA_PAGE11_ControlByte()
{
	memset(&QSFPDD_P10, 0x00, 128);
	memset(&QSFPDD_P11, 0x00, 128);
	// Page10 RW Control
	// Page11 Read only
	// Lan0 - Lan7 State
	QSFPDD_P10[17]  = 0x11 ;
	QSFPDD_P10[18]  = 0x11 ;
	QSFPDD_P10[19]  = 0x11 ;
	QSFPDD_P10[20]  = 0x11 ;
	QSFPDD_P10[21]  = 0x00 ;
	QSFPDD_P10[22]  = 0x00 ;
	QSFPDD_P10[23]  = 0x00 ;
	QSFPDD_P10[24]  = 0x00 ;

	QSFPDD_P10[25]  = 0x00 ;

	QSFPDD_P11[78]  = QSFPDD_P10[17];
	QSFPDD_P11[79]  = QSFPDD_P10[18];
	QSFPDD_P11[80]  = QSFPDD_P10[19];
	QSFPDD_P11[81]  = QSFPDD_P10[20];
	QSFPDD_P11[82]  = QSFPDD_P10[21];
	QSFPDD_P11[83]  = QSFPDD_P10[22];
	QSFPDD_P11[84]  = QSFPDD_P10[23];
	QSFPDD_P11[85]  = QSFPDD_P10[24];
	// Tx CDR ON
	QSFPDD_P10[32] = 0x0F ;
	QSFPDD_P11[93] = QSFPDD_P10[32] ;
	// Rx CDR ON
	QSFPDD_P10[33] = 0x0F ;
	QSFPDD_P11[94] = QSFPDD_P10[33] ;
	// Page11 Data Path Deactivated
	// 04 DataPathActivated State
	QSFPDD_P11[0] = 0x11 ;
	QSFPDD_P11[1] = 0x11 ;
	QSFPDD_P11[2] = 0x11 ;
	QSFPDD_P11[3] = 0x11 ;

	// DataPathPwrUp
	QSFPDD_P10[0] = 0x00 ;
	// Polarity Flip
	QSFPDD_P10[1] = 0x00 ;
	// Tx output Disable
	QSFPDD_P10[2] = 0x00 ;
	// TX input EQ Setting
	QSFPDD_P10[28] = 0x00 ;
	QSFPDD_P10[29] = 0x00 ;
	QSFPDD_P10[30] = 0x00 ;
	QSFPDD_P10[31] = 0x00 ;
    //Init before value by sram
    //Bef_TxDIS_Power=QSFPDD_P10[2];
    Bef_Tx_output=QSFPDD_P10[3];
    Bef_Rx_output=QSFPDD_P10[10];
}


void Page13_Default()
{
	memset(&QSFPDD_P13, 0x00, 128);
	// LoopBack Supported Setting
	QSFPDD_P13[0]  = 0xFF ;          // 128 pre-lane and all
	QSFPDD_P13[1]  = 0x00 ;          // 129 BER Not Supported
	QSFPDD_P13[2]  = 0x00 ;          // 130 Not Supported
	QSFPDD_P13[3]  = 0x88 ;          // 131 only Media side Pre-FEC PRBS generator
	QSFPDD_P13[4]  = 0xFF ;          // 132 Host Side Generator ( System Side )
	QSFPDD_P13[5]  = 0x0F ;          // 133 Host Side Generator ( System Side )

	QSFPDD_P13[6]  = 0xFF ;          // 134 Media Side Generator ( Line Side )
	QSFPDD_P13[7]  = 0x0F ;          // 135 Media Side Generator ( Line Side )

	QSFPDD_P13[8]  = 0x00 ;          // 136 Host Side Checker Not Supported
	QSFPDD_P13[9]  = 0x00 ;          // 137 Host Side Checker Not Supported
	QSFPDD_P13[10] = 0x00 ;          // 138 Media Side Checker Not Supported
	QSFPDD_P13[11] = 0x00 ;          // 139 Media Side Checker Not Supported

	QSFPDD_P13[12] = 0x00 ;          // 140 Recovered clock for generator not supported
	QSFPDD_P13[13] = 0x00 ;          // 141 Host Side Checker Data not supported
	QSFPDD_P13[14] = 0x00 ;          // 142 Media Side Checker Data not supported
	QSFPDD_P13[15] = 0x00 ;          // 143 RSVD
}


void PowerON_Table_Init()
{
	QSFPDD_Table_GetDate();
	Device_Table_Get_Data();

	QDD_MSA_Init_SETTING();
	QDD_MSA_PAGE11_ControlByte();
	Page13_Default();
	Get_QDD_MSA_Optional_Control_Table();

	memset(&CALIB_MEMORY_1_MAP, 0x00, 128);
}



