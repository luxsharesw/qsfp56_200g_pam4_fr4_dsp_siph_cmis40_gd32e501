#include "gd32e501.h"
#include "core_cm33.h"
#include "string.h"
#include "GD_FlahMap.h"
#include "BRCM_DSP_87540.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "CMIS_MSA.h"
#include "Macom_TIA_03819.h"
#include "LDD_Control.h"

int main()
{
    // GD MCU Initialize
    GD32E501_Power_on_Initial();
    // MSA HW RESET Check 
    PowerOn_Reset_Check();
    // Flash & SRAM Upload
    PowerON_Table_Init();
    // Power on timing control
    PowerOn_Sequencing_Control();
    // LDD Initiialize
    WriteSpan_Setttin_100mA_Range(0x06);
    PowerON_DAC_Initialize();
    // TIA Initilaize    
    MATA03819_Init();    
    // Enable DDMI Function
    CALIB_MEMORY_MAP.DDMI_DISABLE = 0 ;    
    //i2c_enable(I2C0);
    while(1)
    {
        // I2C Enable Control
        ModSelL_Function();
        // MSA StateMachine Control
        QSFPDD_MSA_StateMachine(); 
        // Luxshare-OET internal control
        MCU_READ_WRITE_DEIVCE_COMMAND_CONTROL(); 
        // MSA HW RESET
        RESET_L_Function();
        // Move Sram & Flash read/write
        SRMA_Flash_Function();
    }
}




