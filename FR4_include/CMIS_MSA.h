//----------------------------------------------//
// Data SRAM include
//----------------------------------------------//
//----------------------------------------------------//
//AW
//----------------------------------------------------//
#define ADDR_DPCF          6
#define ADDR_TXFAULT       7
#define ADDR_TXLOS         8
#define ADDR_TXCDR_LOL     9
#define ADDR_TX_EQ_FAULT  10
#define ADDR_TXP_HA       11
#define ADDR_TXP_LA       12
#define ADDR_TXP_HW       13
#define ADDR_TXP_LW       14
#define ADDR_TXB_HA       15
#define ADDR_TXB_LA       16
#define ADDR_TXB_HW       17
#define ADDR_TXB_LW       18
#define ADDR_RXLOS        19
#define ADDR_RXCDR_LOL    20
#define ADDR_RXP_HA       21
#define ADDR_RXP_LA       22
#define ADDR_RXP_HW       23
#define ADDR_RXP_LW       24

#define TXFAULT_INT    0x0001
#define TXLOS_INT      0x0002
#define TXCDR_LOL_INT  0x0004
#define TXEQ_FAULT_INT 0x0008
#define TXPHA_INT      0x0010
#define TXPLA_INT      0x0020
#define TXPHW_INT      0x0040
#define TXPLW_INT      0x0080
#define TXBHA_INT      0x0100
#define TXBLA_INT      0x0200
#define TXBHW_INT      0x0400
#define TXBLW_INT      0x0800

#define RX_ROS_INT     0x0001
#define RXCDR_LOL_INT  0x0002
#define RXHA_INT       0x0004
#define RXLA_INT       0x0008
#define RXHW_INT       0x0010
#define RXLW_INT       0x0020
#define VCC_TEMP_INT   0x0040
//---------------------------------------------------//
// Configuration Error Code Page11h 202-205
// 202 Lane1 0-3   Lane2 4-7
// 203 Lane3 0-3   Lane4 4-7
// 204 Lane5 0-3   Lane6 4-7
// 205 Lane7 0-3   Lane8 4-7
// feedback error code define
//---------------------------------------------------//
#define No_Status                      0x00
#define Config_In_Process              0x00
#define Accepted                       0x01
#define Rejected_Unknown               0x02
#define Rejected_Invalid_Code          0x03
#define Rejected_Invalid_Combo         0x04
#define Rejected_Invalid_SI            0x05
#define Rejected_In_Use                0x06
#define Rejected_Incomplete_Lane_Info  0x07
#define All_CH 4
//----------------------------------------------------//
//Rx Output Control Max Limit - PAM4
//Rx Output Control Max Limit - NRZ
//----------------------------------------------------//
#define PAM4_Pre_Cursor_Max_Vaule        7
#define PAM4_Post_Cursor_Max_Vaule       7
#define PAM4_AMP_Max_Vaule               3
#define Max_Output_Value                16
#define NRZ_Pre_Cursor_Max_Vaule         3
#define NRZ_Post_Cursor_Max_Vaule        3
#define NRZ_AMP_Max_Vaule                2
//----------------------------------------------------//
//Signal Status
//----------------------------------------------------//
#define PAM4_to_PAM4 0
#define NRZ_to_NRZ   1
#define NRZ_to_PAM4  2

extern uint8_t QSFPDD_A0[256];
extern uint8_t QSFPDD_P0[128];
extern uint8_t QSFPDD_P1[128];
extern uint8_t QSFPDD_P2[128];
extern uint8_t QSFPDD_P3[128];
extern uint8_t QSFPDD_P10[128];
extern uint8_t QSFPDD_P11[128];
extern uint8_t QSFPDD_P13[128];
extern uint8_t QSFPDD_P14[128];
extern uint8_t CTEL_Table[16];
extern uint8_t Pre_Table[16];
extern uint8_t OP_AMP_Table[16];
extern uint8_t Post_Table[16];
extern uint8_t PW_LEVE1[4];
extern uint8_t DSP_MODE_SET;

extern uint8_t Clear_flag;
extern uint8_t Clear_ADR;
//----------------------------------------------//
// GD32E501_Initialize.c
//----------------------------------------------//
void GD32E501_Power_on_Initial();
//----------------------------------------------//
// GD32E501_ADC_Control.c
//----------------------------------------------//
uint16_t GET_ADC_Value_Data(uint16_t ADC_CH);
uint16_t GET_GD_Temperature();
//----------------------------------------------//
// QSFP56_SR4_PowerOn_Sequencing.c
//----------------------------------------------//
void ModSelL_Function();
void PowerOn_Sequencing_Control();
void SET_Power_Control(uint8_t SET_Value);
uint8_t Get_Power_C_Status();
void MCU_READ_WRITE_DEIVCE_COMMAND_CONTROL();
void CheckSum_Calculate();
void RESET_L_Function();
void PowerOn_Reset_Check();
//----------------------------------------------//
// QSFP56_SR4_PowerOn_Table.c
//----------------------------------------------//
void PowerON_Table_Init();
uint8_t Get_CH_Data (uint8_t Data,uint8_t CH);
extern void Init_Rx_Output_Values ();
extern void Get_NRZ_Default_System_Side();
extern void Device_Table_Get_Data();
extern uint8_t Bef_Tx_output;
extern uint8_t Bef_Rx_output;
extern uint8_t Bef_TxDIS_Power;
//----------------------------------------------//
// GD32E501_Flash_RW.c
//----------------------------------------------//
void GDMCU_Flash_Erase(uint32_t FMC_ADR);
void GDMCU_FMC_READ_FUNCTION(uint32_t FMC_ADR,uint8_t *DataBuffer,uint16_t DATAL);
void GDMCU_FMC_BytesWRITE_FUNCTION(uint32_t FMC_ADR,uint8_t *Write_data,uint8_t DATAL);
uint8_t GDMCU_FMC_READ_DATA(uint32_t FMC_ADR);
uint16_t Swap_Bytes(uint16_t Data);
//----------------------------------------------//
// QSFPDD_MSA_AW.c
//----------------------------------------------//
void DDMI_AW_Monitor();
void Clear_Flag(uint8_t Current_AD);
void Clear_VCC_TEMP_Flag();
void Clear_Module_state_Byte8();
void CMIS40_TXLOS_LOL_AW();
void CMIS40_RXLOS_LOL_AW();
void ALL_Clear_Funciton();
extern uint8_t Rx_LOS_Buffer;
extern uint8_t Tx_LOS_Buffer;
//----------------------------------------------//
// QSFPDD_MSA_DDMI.c
//----------------------------------------------//
void Temperature_Monitor(int16_t tempsensor_value);
void VCC_Monitor( uint16_t VCC_Vaule);
void QSFPDD_DDMI_StateMachine(uint8_t StateMachine );
//----------------------------------------------//
// QSFPDD_MSA_StateMachine.c
//----------------------------------------------//
void Get_module_Power_Monitor();
void QSFPDD_MSA_StateMachine();
extern uint8_t Tx_Disable_Flag;
extern uint8_t DSP_MODE_SET;
extern uint8_t Error_code_Pass;
extern uint8_t Signal_Status;
//----------------------------------------------//
// Slave_I2C0_PB67.c
//----------------------------------------------//
void SRMA_Flash_Function();
//----------------------------------------------//
// QSFPDD_MSA_Optional_Control.c
//----------------------------------------------//
void Configuration_Error_code_Feedback(uint8_t Lane_CH,uint8_t Write_Error_code);
void P13_diagnostic_features();
void QDD_MSA_CTLE_PRE_POST_AMP_CONTROL();
uint8_t Get_Rx_Output_Max_Value (uint8_t PAM4_Max ,uint8_t NRZ_Max);