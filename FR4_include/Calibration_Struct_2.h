/*
 * Calibration_Struct_2.h
 *
 *  Created on: 2017�~10��25��
 *      Author: Ivan_Lin
 */

#ifndef INC_CALIBRATION_STRUCT_2_H_
#define INC_CALIBRATION_STRUCT_2_H_

struct CALIB_MEMORY_1
{
    uint8_t  VCC1_SCALEM;          //Address = 0x80
    uint8_t  VCC1_SCALEL;          //Address = 0x81
    uint8_t  VCC1_OFFSET_MSB;      //Address = 0x82
    uint8_t  VCC1_OFFSET_LSB;      //Address = 0x83
    uint8_t  TEMP1_SCALE1M;        //Address = 0x84
    uint8_t  TEMP1_SCALE1L;        //Address = 0x85
    uint8_t  TEMP1_OFFSET1;        //Address = 0x86
    uint8_t  IBias4_SCALEM;        //Address = 0x87
    uint8_t  IBias4_SCALE1L;       //Address = 0x88
    uint8_t  IBias4_OFFSET_MSB;    //Address = 0x89
    uint8_t  IBias4_OFFSET_LSB;    //Address = 0x8A
    uint8_t  IBias5_SCALEM;        //Address = 0x8B
    uint8_t  IBias5_SCALE1L;       //Address = 0x8C
    uint8_t  IBias5_OFFSET_MSB;    //Address = 0x8D
    uint8_t  IBias5_OFFSET_LSB;    //Address = 0x8E
    uint8_t  IBias6_SCALEM;        //Address = 0x8F
    uint8_t  IBias6_SCALE1L;       //Address = 0x90
    uint8_t  IBias6_OFFSET_MSB;    //Address = 0x91
    uint8_t  IBias6_OFFSET_LSB;    //Address = 0x92
    uint8_t  IBias7_SCALEM;        //Address = 0x93
    uint8_t  IBias7_SCALE1L;       //Address = 0x94
    uint8_t  IBias7_OFFSET_MSB;    //Address = 0x95
    uint8_t  IBias7_OFFSET_LSB;    //Address = 0x96
    uint8_t  TXP4_SCALEM;          //Address = 0x97
    uint8_t  TXP4_SCALEL;          //Address = 0x98
    uint8_t  TXP4_OFFSET_MSB;      //Address = 0x99
    uint8_t  TXP4_OFFSET_LSB;      //Address = 0x9A
    uint8_t  TXP5_SCALEM;          //Address = 0x9B
    uint8_t  TXP5_SCALEL;          //Address = 0x9C
    uint8_t  TXP5_OFFSET_MSB;      //Address = 0x9D
    uint8_t  TXP5_OFFSET_LSB;      //Address = 0x9E
    uint8_t  TXP6_SCALEM;          //Address = 0x9F
    uint8_t  TXP6_SCALEL;          //Address = 0xA0
    uint8_t  TXP6_OFFSET_MSB;      //Address = 0xA1
    uint8_t  TXP6_OFFSET_LSB;      //Address = 0xA2
    uint8_t  TXP7_SCALEM;          //Address = 0xA3
    uint8_t  TXP7_SCALEL;          //Address = 0xA4
    uint8_t  TXP7_OFFSET_MSB;      //Address = 0xA5
    uint8_t  TXP7_OFFSET_LSB;      //Address = 0xA6
    uint8_t  RX4_SCALEM;           //Address = 0xA7
    uint8_t  RX4_SCALEL;           //Address = 0xA8
    uint8_t  RX4_OFFSET_MSB;       //Address = 0xA9
    uint8_t  RX4_OFFSET_LSB;       //Address = 0xAA
    uint8_t  RX5_SCALEM;           //Address = 0xAB
    uint8_t  RX5_SCALEL;           //Address = 0xAC
    uint8_t  RX5_OFFSET_MSB;       //Address = 0xAD
    uint8_t  RX5_OFFSET_LSB;       //Address = 0xAE   
    uint8_t  RX6_SCALEM;           //Address = 0xAF
    uint8_t  RX6_SCALEL;           //Address = 0xB0
    uint8_t  RX6_OFFSET_MSB;       //Address = 0xB1
    uint8_t  RX6_OFFSET_LSB;       //Address = 0xB2
    uint8_t  RX7_SCALEM;           //Address = 0xB3
    uint8_t  RX7_SCALEL;           //Address = 0xB4
    uint8_t  RX7_OFFSET_MSB;       //Address = 0xB5
    uint8_t  RX7_OFFSET_LSB;       //Address = 0xB6
    uint8_t  Rx4_LOS_Assret_MSB;   //Address = 0xB7
    uint8_t  Rx4_LOS_Assret_LSB;   //Address = 0xB8    
    uint8_t  Rx4_LOS_DeAssret_MSB; //Address = 0xB9
    uint8_t  Rx4_LOS_DeAssret_LSB; //Address = 0xBA                
    uint8_t  Rx5_LOS_Assret_MSB;   //Address = 0xBB
    uint8_t  Rx5_LOS_Assret_LSB;   //Address = 0xBC     
    uint8_t  Rx5_LOS_DeAssret_MSB; //Address = 0xBD
    uint8_t  Rx5_LOS_DeAssret_LSB; //Address = 0xBE        
    uint8_t  Rx6_LOS_Assret_MSB;   //Address = 0xBF
    uint8_t  Rx6_LOS_Assret_LSB;   //Address = 0xC0    
    uint8_t  Rx6_LOS_DeAssret_MSB; //Address = 0xC1
    uint8_t  Rx6_LOS_DeAssret_LSB; //Address = 0xC2    
    uint8_t  Rx7_LOS_Assret_MSB;   //Address = 0xC3 
    uint8_t  Rx7_LOS_Assret_LSB;   //Address = 0xC4    
    uint8_t  Rx7_LOS_DeAssret_MSB; //Address = 0xC5
    uint8_t  Rx7_LOS_DeAssret_LSB; //Address = 0xC6
    
    uint8_t  TX_P2V5_ADC_MSB;      //Address = 0xC7
    uint8_t  TX_P2V5_ADC_LSB;      //Address = 0xC8    
    uint8_t  TRX_P4V5_ADC_MSB;     //Address = 0xC9
    uint8_t  TRX_P4V5_ADC_LSB;     //Address = 0xCA    
    uint8_t  TX_P3V3_ADC_MSB;      //Address = 0xCB
    uint8_t  TX_P3V3_ADC_LSB;      //Address = 0xCC
    uint8_t  RX_P3V3_ADC_MSB;      //Address = 0xCD
    uint8_t  RX_P3V3_ADC_LSB;      //Address = 0xCE    
    uint8_t  RX_CDR_P1V8_ADC_MSB;  //Address = 0xCF
    uint8_t  RX_CDR_P1V8_ADC_LSB;  //Address = 0xD0   
    uint8_t  TX_CDR_P1V8_ADC_MSB;  //Address = 0xD1
    uint8_t  TX_CDR_P1V8_ADC_LSB;  //Address = 0xD2    
    uint8_t  TEMP_ADC_MSB;         //Address = 0xD3
    uint8_t  TEMP_ADC_LSB;         //Address = 0xD4
    
    uint8_t  Power_C_Status;       //Address = 0xD5
    uint8_t  Power_SET_Value;	   //Address = 0xD6
    uint8_t  Power_SET_Start;      //Address = 0xD7
    uint8_t  I2C_MADR_TEST;        //Address = 0xD8
    uint8_t  I2C_RBUFF_TEST;       //Address = 0xD9
    uint8_t  I2C_WBUFF_TEST;       //Address = 0xDA
    uint8_t  I2C_EN_TEST;          //Address = 0xDB
    uint8_t  CAL_Buffer[36];       //Address = 0xD9 - 0xFF
};

extern struct CALIB_MEMORY_1 CALIB_MEMORY_1_MAP;


#endif /* INC_CALIBRATION_STRUCT_2_H_ */
