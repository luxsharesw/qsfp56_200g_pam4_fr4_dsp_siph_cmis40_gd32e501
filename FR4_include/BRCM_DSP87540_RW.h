/*
 * BRCM_DSP_D87540.h
 *
 *  Created on: 2018Ǿ11ū13ũ
 *      Author: Ivan_Lin
 */

#ifndef INC_BRCM_DSP_D87540_H_
#define INC_BRCM_DSP_D87540_H_

//----------------------------------------------------------------------------------------//
// DSP D87540 I2C Command
//----------------------------------------------------------------------------------------//
//#define BRCM_11181_I2C_SAD    0xA0    // BSC Slave Address
#define BRCM_PAGE_SELECT      0x7F    // QSFP Page Select Register
#define BRCM_IND_ADDR0        0x80    // Indirect Address register 0
#define BRCM_IND_ADDR1        0x81    // Indirect Address register 1
#define BRCM_IND_ADDR2        0x82    // Indirect Address register 2
#define BRCM_IND_ADDR3        0x83    // Indirect Address register 3
// BRCM Read/Write Data length
#define BRCM_IND_LEN0         0x84    // Indirect Length register 0
#define BRCM_IND_LEN1         0x85    // Indirect Length register 1
// R/W Command Address
// 0x03 Write Command
// 0x01 Read  Command
#define BRCM_IND_CTRL         0x86    // Indirect Read/Write Control register
#define BRCM_WFIFO            0x87    // Write FIFO register
#define BRCM_RFIFO            0x90    // Read  FIFO register

void BRCM_READ_Data( uint8_t *BRCM_ADDR , uint8_t *DataBuffer , uint8_t Data_Length );
void BRCM_WRITE_Data( uint8_t *BRCM_ADDR , uint8_t *DataBuffer , uint8_t Data_Length );
uint32_t BRCM_READ_REG_DATA( uint8_t *BRCM_ADDR );
void TEST_DSP_Command_Direct_Control();

struct BRCM_D87540_DSP_MEMORY
{
    uint8_t BRCM_REG_ADDR_0 ;         // RW  SRAM Address 80
    uint8_t BRCM_REG_ADDR_1 ;         // RW  SRAM Address 81
    uint8_t BRCM_REG_ADDR_2 ;         // RW  SRAM Address 82
    uint8_t BRCM_REG_ADDR_3 ;         // RW  SRAM Address 83
    uint8_t WRITE_BUFFER_0 ;          // RW  SRAM Address 84
    uint8_t WRITE_BUFFER_1 ;          // RW  SRAM Address 85
    uint8_t WRITE_BUFFER_2 ;          // RW  SRAM Address 86
    uint8_t WRITE_BUFFER_3 ;          // RW  SRAM Address 87
    uint8_t READ_BUFFER_0 ;           // RW  SRAM Address 88
    uint8_t READ_BUFFER_1 ;           // RW  SRAM Address 89
    uint8_t READ_BUFFER_2 ;           // RW  SRAM Address 8A
    uint8_t READ_BUFFER_3 ;           // RW  SRAM Address 8B
    uint8_t LENGITH_MSB ;             // RW  SRAM Address 8C
	uint8_t LENGITH_LSB ;             // RW  SRAM Address 8D
	uint8_t Direct_Control ;          // RW  SRAM Address 8E
	uint8_t RW_EN ;                   // RW  SRAM Address 8F
	uint8_t CHIP_INFO_LSB[4];         // RW  SRAM Address 90 - 93
	uint8_t CHIP_INFO_MSB[4];         // RW  SRAM Address 94 - 97
	uint8_t Buffer[8];                // RW  SRAM Address 98 - 9F
	uint8_t Phy_ID;                   // RW  SRAM Address A0
	uint8_t BRCM_ADDR_DATA;           // RW  SRAM Address A1
	uint8_t Debug_flag;			      // RW  SRAM Address A2
    uint8_t Ready_Byte_MSB;           // RW  SRAM Address A3
    uint8_t Ready_Byte_LSB;           // RW  SRAM Address A4   
    uint8_t BRCM_CHID_MSB;            // RW  SRAM Address A5
    uint8_t BRCM_CHID_LSB;            // RW  SRAM Address A6
	uint8_t CMD_CON;                  // RW  SRAM Address A7
	uint8_t Trigger_CMD;              // RW  SRAM Address A8
	uint8_t CHIP_MODE_VALUE;          // RW  SRAM Address A9 
    uint8_t DSP_Delay_Count_MSB;      // RW  SRAM Address AA
    uint8_t DSP_Delay_Count_LSB;      // RW  SRAM Address AB
	uint8_t Temp_Buffer[84];          // RW  SRAM Address AC - FF
};

extern struct BRCM_D87540_DSP_MEMORY BRCM_D87540_DSP_MEMORY_MAP;

#endif /* INC_BRCM_DSP_1181_H_ */