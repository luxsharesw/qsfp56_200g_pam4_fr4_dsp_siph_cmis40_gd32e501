
//=====================================================================================//
//maxim 5825 DAC control
//=====================================================================================//
void Max5825_DAC_CLR_C();
void Max5825_DAC_REG_Relaod();
void Max5825_initial();
void MAX5825_ADC_output(uint8_t Channel ,uint8_t ADC_Vaule_MSB,uint8_t ADC_Vaule_LSB);
void PowerON_DAC_Initialize();
void SET_DAC_ALLCH_Value();
//=====================================================================================//
// ADC Monitor
//=====================================================================================//
void ADC_Heater_Mon();
void ADC_EAM_CH1_CH4_Mon();
//=====================================================================================//
//LTC 2662 Bias current source control
//=====================================================================================//
void SPI_LTC2662_Command_TEST();
void SET_Bias_Current_ALLCH();
void WriteSpan_Setttin_100mA_Range(uint8_t Data_Value);
//-------------------------------------------------------//
// LTC2662 Current source Control
//         - Bias current 0 - 100 mA
// Max5825 DAC control 
//         - EAM/EML Mod Control CH1-CH4
//         - Heater Control      CH5-CH8 
//-------------------------------------------------------//

struct LDD_Control_MEMORY
{
    uint8_t LDD_Control_CHIPID_MSB;    // SRAM Address 80
    uint8_t LDD_Control_CHIPID_LSB;    // SRAM Address 81
    uint8_t EAM_ADC_MSB_CH1;           // SRAM Address 82
    uint8_t EAM_ADC_LSB_CH1;           // SRAM Address 83
    uint8_t EAM_ADC_MSB_CH2;           // SRAM Address 84
    uint8_t EAM_ADC_LSB_CH2;           // SRAM Address 85
    uint8_t EAM_ADC_MSB_CH3;           // SRAM Address 86
    uint8_t EAM_ADC_LSB_CH3;           // SRAM Address 87
    uint8_t EAM_ADC_MSB_CH4;           // SRAM Address 88
    uint8_t EAM_ADC_LSB_CH4;           // SRAM Address 89    
    uint8_t Heater_ADC_MSB_CH1;        // SRAM Address 8A
    uint8_t Heater_ADC_LSB_CH1;        // SRAM Address 8B
    uint8_t Heater_ADC_MSB_CH2;        // SRAM Address 8C
    uint8_t Heater_ADC_LSB_CH2;        // SRAM Address 8D
    uint8_t Heater_ADC_MSB_CH3;        // SRAM Address 8E
    uint8_t Heater_ADC_LSB_CH3;        // SRAM Address 8F
    uint8_t Heater_ADC_MSB_CH4;        // SRAM Address 90
    uint8_t Heater_ADC_LSB_CH4;        // SRAM Address 91    
    uint8_t READ_ONLY_BUFFER[14];      // SRAM Address 92-9F 
    uint8_t EAM_DAC_MSB_CH0;           // SRAM Address A0
    uint8_t EAM_DAC_LSB_CH0;           // SRAM Address A1
    uint8_t EAM_DAC_MSB_CH1;           // SRAM Address A2
    uint8_t EAM_DAC_LSB_CH1;           // SRAM Address A3
    uint8_t EAM_DAC_MSB_CH2;           // SRAM Address A4
    uint8_t EAM_DAC_LSB_CH2;           // SRAM Address A5        
    uint8_t EAM_DAC_MSB_CH3;           // SRAM Address A6
    uint8_t EAM_DAC_LSB_CH3;           // SRAM Address A7    
    uint8_t Heater_DAC_MSB_CH4;        // SRAM Address A8
    uint8_t Heater_DAC_LSB_CH4;        // SRAM Address A9  
    uint8_t Heater_DAC_MSB_CH5;        // SRAM Address AA    
    uint8_t Heater_DAC_LSB_CH5;        // SRAM Address AB         
    uint8_t Heater_DAC_MSB_CH6;        // SRAM Address AC
    uint8_t Heater_DAC_LSB_CH6;        // SRAM Address AD
    uint8_t Heater_DAC_MSB_CH7;        // SRAM Address AE
    uint8_t Heater_DAC_LSB_CH7;        // SRAM Address AF    
    uint8_t BiasC_MSB_CH0;             // SRAM Address B0
    uint8_t BiasC_LSB_CH0;             // SRAM Address B1       
    uint8_t BiasC_MSB_CH1;             // SRAM Address B2
    uint8_t BiasC_LSB_CH1;             // SRAM Address B3
    uint8_t BiasC_MSB_CH2;             // SRAM Address B4
    uint8_t BiasC_LSB_CH2;             // SRAM Address B5
    uint8_t BiasC_MSB_CH3;             // SRAM Address B6
    uint8_t BiasC_LSB_CH3;             // SRAM Address B7    
    uint8_t WRITE_C_Buffer[8];         // SRAM Address B8 - BF

    uint8_t LT2622_Contr_EN;           // SRAM Address C0
    uint8_t Read_data[4];              // SRAM Address C1 - C4
    uint8_t Write_data[4];             // SRAM Address C5 - C8
    uint8_t LDD_Buffer[55];            // SRAM Address C9 - FF
};

extern struct LDD_Control_MEMORY LDD_Control_MEMORY_MAP;

