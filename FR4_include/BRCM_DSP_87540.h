#define Mode1_4x53G_PAM4_4x53G_PAM4_1PORT       0x11
#define Mode3_4x25G_NRZ_4x25G_NRZ_1PORT         0x21
#define Mode9_1x10G_NRZ_1x10G_NRZ_1PORT         0x31
#define Mode2_2x53G_PAM4_2x53G_PAM4_2PORT       0x41
#define Mode4_1x53G_PAM4_1x53G_PAM4_4PORT       0x51
#define Mode13_1x25G_NRZ_1x25G_NRZ_4PORT        0x61
#define Mode9_1x10G_NRZ_1x10G_NRZ_4PORT         0x71

//Not use modes




#define Mode5_4x26G_NRZ_2x53G_PAM4_1PORT       	0xA4
#define Mode6_2x26G_NRZ_1x53G_PAM4_2PORT       	0xA5
#define Mode7_2x25G_NRZ_1x50G_PAM4_2PORT       	0xA6
#define Mode8_4X25G_NRZ_2X50G_PAM4_1PORT       	0xA7

#define Mode10_2x26G_NRZ_1x53G_PAM4_2PORT      	0xA9
#define Mode11_2x25G_NRZ_1x50G_PAM4_2PORT      	0xAA
#define Mode12_4x26G_NRZ_4x26G_NRZ_1PORT       	0xAB

#define Mode14_2x26G_NRZ_2x26G_NRZ_2PORT       	0xAD
#define Mode15_2x25G_NRZ_2x25G_NRZ_2PORT       	0xAE
#define Mode16_2x25G_NRZ_1x53G_PAM4_2PORT      	0xAF
#define Mode17_4x25G_NRZ_2x53G_PAM4_1PORT      	0xB0
#define Mode18_4x50G_PAM4_4x50G_PAM4_1PORT     	0xB1
#define Mode19_2x50G_NRZ_1x53G_PAM4_2PORT      	0xB2

#define BRCM87540_I2C_ADR                 0xA0
#define BRCM_Default_Length             0x0004
#define COMMAND_Base_ADR            0x00047000
#define Remote_Loopback_mode                 0
#define Digital_LoopBack_mode                1
#define DSP_PAM4_FIR_MAX                   168
#define DSP_NRZ_FIR_MAX                    128
#define TX_Side                              1
#define RX_Side                              0
#define Function_EN                          1
#define Function_DIS                         0
//DSP Pattern setting value
#define PRBS7Q_SystemSide      0x00
#define PRBS9Q_SystemSide      0x01
#define PRBS13Q_SystemSide     0x0A
#define PRBS15Q_SystemSide     0x03
#define PRBS23Q_SystemSide     0x04
#define PRBS31Q_SystemSide     0x05
#define SSPRQ_SystemSide       0xBB
#define PRBS7Q_LineSide        0x07
#define PRBS9Q_LineSide        0x09
#define PRBS13Q_LineSide       0x0D
#define PRBS15Q_LineSide       0x0F
#define PRBS23Q_LineSide       0x17
#define PRBS31Q_LineSide       0x1F
#define SSPRQ_LineSide         0xAA

extern uint32_t COMMAND_4700_DATA[18];

uint32_t BRCM_Control_READ_Data( uint32_t BRCM_ADDR );
void BRCM_Control_WRITE_Data( uint32_t BRCM_ADDR , uint32_t Data_Value , uint16_t Data_Length );
void DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(uint32_t Read_ADDRESS);
void DSP87540_CAPI_CW_CMD(uint32_t INTF_ADR0,uint32_t INTF_DATA0,uint32_t INTF_ADR1,uint32_t INTF_DATA1);
void DSP87540_CAPI_Command_response(uint32_t CHK_ADR0,uint32_t CHK_ADR1);
void Trigger_CMD_Update_DSP_REG();
void DSP87540_Init( uint8_t DSP_CHIP_MODE );
void DSP87540_SystemSide_TRX_Squelch_SET(uint8_t Lane_CH ,uint8_t TRX_Side,uint8_t Enable);
void DSP87540_LineSide_Loopback_SET(uint8_t LoopBack_mode,uint8_t Lane_CH,uint8_t Enable);
void DSP87540_LineSide_TRX_Squelch_SET(uint8_t Lane_CH ,uint8_t TRX_Side,uint8_t Enable);
void DSP87540_SystemSide_Loopback_SET(uint8_t LoopBack_mode,uint8_t Lane_CH,uint8_t Enable);
void DSP87540_SystemSide_TRX_Polarity_SET(uint8_t Lane_CH , uint8_t TRX_Side_SEL);
void DSP87540_SystemSide_TX_FIR_SET( uint8_t Lane_CH );
void SET_CHIP_MODE( uint8_t DSP_CHIP_MODE );
void DSP87540_SystemSide_PRBS_SET(uint8_t Pattern,uint8_t Lane_CH ,uint8_t Enable);
void DSP87540_LineSide_PRBS_SET(uint8_t Pattern,uint8_t Lane_CH ,uint8_t Enable);
uint16_t GET_DSP87540_Temperature();

struct BRCM_87540_DSP_LS_PHY0_MEMORY
{
	uint16_t DSP_CHIP_MODE ;              // RW DSP 5200C8C7  SRAM Address 80 - 81
	uint16_t Trigger_MODE ;               // RW DSP 5200C884  SRAM Address 82 - 83

	uint16_t Line_Side_PRE1_CH0 ;         // RW DSP 580034D0  SRAM Address 84 - 85
	uint16_t Line_Side_PRE1_CH1 ;         // RW DSP 580037D0  SRAM Address 86 - 87
	uint16_t Line_Side_PRE1_CH2 ;         // RW DSP 58003BD0  SRAM Address 88 - 89
	uint16_t Line_Side_PRE1_CH3 ;         // RW DSP 58003FD0  SRAM Address 8A - 8B

	uint16_t Line_Side_Main_CH0 ;         // RW DSP 580034D4  SRAM Address 8C - 8D
	uint16_t Line_Side_Main_CH1 ;         // RW DSP 580037D4  SRAM Address 8E - 8F
	uint16_t Line_Side_Main_CH2 ;         // RW DSP 58003BD4  SRAM Address 90 - 91
	uint16_t Line_Side_Main_CH3 ;         // RW DSP 58003FD4  SRAM Address 92 - 93

	uint16_t Line_Side_POST1_CH0 ;        // RW DSP 580034D8  SRAM Address 94 - 95
	uint16_t Line_Side_POST1_CH1 ;        // RW DSP 580037D8  SRAM Address 96 - 97
	uint16_t Line_Side_POST1_CH2 ;        // RW DSP 58003BD8  SRAM Address 98 - 99
	uint16_t Line_Side_POST1_CH3 ;        // RW DSP 58003FD8  SRAM Address 9A - 9B

	uint16_t Line_Side_POST2_CH0 ;        // RW DSP 580034DC  SRAM Address 9C - 9D
	uint16_t Line_Side_POST2_CH1 ;        // RW DSP 580037DC  SRAM Address 9E - 9F
	uint16_t Line_Side_POST2_CH2 ;        // RW DSP 58003BDC  SRAM Address A0 - A1
	uint16_t Line_Side_POST2_CH3 ;        // RW DSP 58003FDC  SRAM Address A2 - A3

	uint16_t Line_Side_POST3_CH0 ;        // RW DSP 580034E0  SRAM Address A4 - A5
	uint16_t Line_Side_POST3_CH1 ;        // RW DSP 580037E0  SRAM Address A6 - A7
	uint16_t Line_Side_POST3_CH2 ;        // RW DSP 58003BE0  SRAM Address A8 - A9
	uint16_t Line_Side_POST3_CH3 ;        // RW DSP 58003FE0  SRAM Address AA - AB

	uint16_t Line_Side_PRE2_CH0 ;         // RW DSP 580034CC  SRAM Address AC - AD
	uint16_t Line_Side_PRE2_CH1 ;         // RW DSP 580037CC  SRAM Address AE - AF
	uint16_t Line_Side_PRE2_CH2 ;         // RW DSP 58003BCC  SRAM Address B0 - B1
	uint16_t Line_Side_PRE2_CH3 ;         // RW DSP 58003FCC  SRAM Address B2 - B3

	uint16_t Line_Side_TX_Polarity_CH0 ;  // RW DSP 580035CC  SRAM Address B4 - B5
	uint16_t Line_Side_TX_Polarity_CH1 ;  // RW DSP 580075CC  SRAM Address B6 - B7
	uint16_t Line_Side_TX_Polarity_CH2 ;  // RW DSP 5800B5CC  SRAM Address B8 - B9
	uint16_t Line_Side_TX_Polarity_CH3 ;  // RW DSP 5800F5CC  SRAM Address BA - BB

	uint16_t Line_Side_RX_Polarity_CH0 ;  // RW DSP 5800110C  SRAM Address BC - BD
	uint16_t Line_Side_RX_Polarity_CH1 ;  // RW DSP 5800510C  SRAM Address BE - BF
	uint16_t Line_Side_RX_Polarity_CH2 ;  // RW DSP 5800910C  SRAM Address C0 - C1
	uint16_t Line_Side_RX_Polarity_CH3 ;  // RW DSP 5800D10C  SRAM Address C2 - C3
    
    uint8_t Line_side_LShift_00_CH0;      // RW DSP 58001828  SRAM Address C4
    uint8_t Line_side_LShift_01_CH0;      // RW DSP 58001828  SRAM Address C5
    uint8_t Line_side_LShift_10_CH0;      // RW DSP 58001828  SRAM Address C6
    uint8_t Line_side_LShift_11_CH0;      // RW DSP 58001828  SRAM Address C7
    
    uint8_t Line_side_LShift_00_CH1;      // RW DSP 58001828  SRAM Address C8
    uint8_t Line_side_LShift_01_CH1;      // RW DSP 58001828  SRAM Address C9
    uint8_t Line_side_LShift_10_CH1;      // RW DSP 58001828  SRAM Address CA
    uint8_t Line_side_LShift_11_CH1;      // RW DSP 58001828  SRAM Address CB
    
    uint8_t Line_side_LShift_00_CH2;      // RW DSP 58001828  SRAM Address CC
    uint8_t Line_side_LShift_01_CH2;      // RW DSP 58001828  SRAM Address CD
    uint8_t Line_side_LShift_10_CH2;      // RW DSP 58001828  SRAM Address CE
    uint8_t Line_side_LShift_11_CH2;      // RW DSP 58001828  SRAM Address CF
    
    uint8_t Line_side_LShift_00_CH3;      // RW DSP 58001828  SRAM Address D0
    uint8_t Line_side_LShift_01_CH3;      // RW DSP 58001828  SRAM Address D1
    uint8_t Line_side_LShift_10_CH3;      // RW DSP 58001828  SRAM Address D2
    uint8_t Line_side_LShift_11_CH3;      // RW DSP 58001828  SRAM Address D3

//  uint16_t Line_side_LShift_LSB_CH0 ;   // RW DSP 58001828  SRAM Address C4 - C5
//	uint16_t Line_side_LShift_MSB_CH0 ;   // RW DSP 5800182C  SRAM Address C6 - C7
//	uint16_t Line_side_LShift_LSB_CH1 ;   // RW DSP 58005828  SRAM Address C8 - C9
//	uint16_t Line_side_LShift_MSB_CH1 ;   // RW DSP 5800582C  SRAM Address CA - CB
//	uint16_t Line_side_LShift_LSB_CH2 ;   // RW DSP 58009828  SRAM Address CC - CD
//	uint16_t Line_side_LShift_MSB_CH2 ;   // RW DSP 5800982C  SRAM Address CE - CF
//	uint16_t Line_side_LShift_LSB_CH3 ;   // RW DSP 5800D828  SRAM Address D0 - D1
//	uint16_t Line_side_LShift_MSB_CH3 ;   // RW DSP 5800D82C  SRAM Address D2 - D3

	uint16_t Dynamic_Phase_THR_CH0 ;      // RW DSP 5800D82C  SRAM Address D4 - D5
	uint16_t Dynamic_Phase_THR_CH1 ;      // RW DSP 5800D82C  SRAM Address D6 - D7
	uint16_t Dynamic_Phase_THR_CH2 ;      // RW DSP 5800D82C  SRAM Address D8 - D9
	uint16_t Dynamic_Phase_THR_CH3 ;      // RW DSP 5800D82C  SRAM Address DA - DB

	uint8_t Line_Side_Buffer[36] ;      //  SRAM Address DC - FF
};

struct BRCM_87540_DSP_SS_PHY0_MEMORY
{
	uint16_t System_Side_PRE1_CH0 ;        // RW DSP 500344D0  SRAM Address 80 - 81
	uint16_t System_Side_PRE1_CH1 ;        // RW DSP 501344D0  SRAM Address 82 - 83
	uint16_t System_Side_PRE1_CH2 ;        // RW DSP 502344D0  SRAM Address 84 - 85
	uint16_t System_Side_PRE1_CH3 ;        // RW DSP 503344D0  SRAM Address 86 - 87

	uint16_t System_Side_Main_CH0 ;        // RW DSP 500344D4  SRAM Address 88 - 89
	uint16_t System_Side_Main_CH1 ;        // RW DSP 501344D4  SRAM Address 8A - 8B
	uint16_t System_Side_Main_CH2 ;        // RW DSP 502344D4  SRAM Address 8C - 8D
	uint16_t System_Side_Main_CH3 ;        // RW DSP 503344D4  SRAM Address 8E - 8F

	uint16_t System_Side_POST1_CH0 ;       // RW DSP 500344D8  SRAM Address 90 - 91
	uint16_t System_Side_POST1_CH1 ;       // RW DSP 501344D8  SRAM Address 92 - 93
	uint16_t System_Side_POST1_CH2 ;       // RW DSP 502344D8  SRAM Address 94 - 95
	uint16_t System_Side_POST1_CH3 ;       // RW DSP 503344D8  SRAM Address 96 - 97

	uint16_t System_Side_POST2_CH0 ;       // RW DSP 500344DC  SRAM Address 98 - 99
	uint16_t System_Side_POST2_CH1 ;       // RW DSP 501344DC  SRAM Address 9A - 9B
	uint16_t System_Side_POST2_CH2 ;       // RW DSP 502344DC  SRAM Address 9C - 9D
	uint16_t System_Side_POST2_CH3 ;       // RW DSP 503344DC  SRAM Address 9E - 9F

	uint16_t System_Side_POST3_CH0 ;       // RW DSP 500344E0  SRAM Address A0 - A1
	uint16_t System_Side_POST3_CH1 ;       // RW DSP 501344E0  SRAM Address A2 - A3
	uint16_t System_Side_POST3_CH2 ;       // RW DSP 502344E0  SRAM Address A4 - A5
	uint16_t System_Side_POST3_CH3 ;       // RW DSP 503344E0  SRAM Address A6 - A7

	uint16_t System_Side_PRE2_CH0 ;        // RW DSP 500344CC  SRAM Address A8 - A9
	uint16_t System_Side_PRE2_CH1 ;        // RW DSP 501344CC  SRAM Address AA - AB
	uint16_t System_Side_PRE2_CH2 ;        // RW DSP 502344CC  SRAM Address AC - AD
	uint16_t System_Side_PRE2_CH3 ;        // RW DSP 503344CC  SRAM Address AE - AF

	uint16_t System_Side_TX_Polarity_CH0 ; // RW DSP 500345CC  SRAM Address B0 - B1
	uint16_t System_Side_TX_Polarity_CH1 ; // RW DSP 501345CC  SRAM Address B2 - B3
	uint16_t System_Side_TX_Polarity_CH2 ; // RW DSP 502345CC  SRAM Address B4 - B5
	uint16_t System_Side_TX_Polarity_CH3 ; // RW DSP 503345CC  SRAM Address B6 - B7

	uint16_t System_Side_RX_Polarity_CH0 ; // RW DSP 5003458C  SRAM Address B8 - B9
	uint16_t System_Side_RX_Polarity_CH1 ; // RW DSP 5013458C  SRAM Address BA - BB
	uint16_t System_Side_RX_Polarity_CH2 ; // RW DSP 5023458C  SRAM Address BC - BD
	uint16_t System_Side_RX_Polarity_CH3 ; // RW DSP 5033458C  SRAM Address BE - BF

	uint8_t System_Side_Buffer[64];        // SRAM Address C0 - FF
};


extern struct BRCM_87540_DSP_LS_PHY0_MEMORY BRCM_87540_DSP_LS_PHY0_MEMORY_MAP;
extern struct BRCM_87540_DSP_SS_PHY0_MEMORY BRCM_87540_DSP_SS_PHY0_MEMORY_MAP;


