//----------------------------------------------------//
// ADC Analog Define (Color Pink)
//----------------------------------------------------//
//----------------------------------------------------//
//ADC 2.5/4096 = 0.00061  6.1 ( 100uV) 12bit
//----------------------------------------------------//
// Analog define 
#define ADC_CONV_Value_100uV   6.1      
#define Heater_CH4_Mon  ADC_CHANNEL_1       //PA1	Heater_CH4_MON
#define P3V3_RX_Mon     ADC_CHANNEL_8       //PB0	P3V3_RX_MON
#define Heater_CH3_Mon  ADC_CHANNEL_9       //PB1	Heater_CH3_MON
#define P3V3_TX_Mon     ADC_CHANNEL_4       //PB2	P3V3_TX_MON
#define RSSI_Mon        ADC_CHANNEL_7       //PB5	RSSI
#define EAM4_MON        ADC_CHANNEL_10      //PC0	EAM4_MON
#define EAM3_MON        ADC_CHANNEL_11      //PC1	EAM3_MON
#define EAM2_MON        ADC_CHANNEL_12      //PC2	EAM2_MON
#define EAM1_MON        ADC_CHANNEL_13      //PC3	EAM1_MON
#define Heater_CH1_Mon  ADC_CHANNEL_14      //PF4	Heater_CH1_Mon
#define Heater_CH2_Mon  ADC_CHANNEL_15      //PF5	Heater_CH2_Mon
#define GD_Temp_Sensor  ADC_CHANNEL_16    
//----------------------------------------------------//
// GPIO Digital Golden Finger Define (Color Yellow)
//----------------------------------------------------//
#define IntL_GPIOC          GPIO_PIN_9      // PC9  OUTPUT_OP
#define ModselL_G_GPIOC     GPIO_PIN_15     // PC15 Input OP
#define RESETL_G_GPIOF      GPIO_PIN_1      // PF1  Input OP
#define LPMODE_G_GPIOF      GPIO_PIN_0      // PF0  Input OP
//----------------------------------------------------//
// GPIO Digital Internal Control Define (Color Orange)
//----------------------------------------------------//
#define P3V3_DSP_EN_GPIOA      GPIO_PIN_11     // PA11 OUTPUT_PP
#define P3V3_RX_EN_GPIOA       GPIO_PIN_12     // PA12 OUTPUT_PP
#define P3V3_TX_EN_GPIOA       GPIO_PIN_15     // PA15 OUTPUT_PP
#define Max5825_CLR_GPIOB      GPIO_PIN_9      // PB9  OUTPUT_PP
#define MODSELB_DSP_GPIOB      GPIO_PIN_12     // PB12 OUTPUT_PP
#define RESET_L_DSP_GPIOB      GPIO_PIN_13     // PB13 OUTPUT_PP
#define LPMODE_DSP_GPIOB       GPIO_PIN_14     // PB14 OUTPUT_PP  
#define INTR_N_DSP_GPIOB       GPIO_PIN_15     // PB15 INPUT_PP 
#define OSC_EN_GPIOC           GPIO_PIN_6      // PC6  OUTPUT_PP
#define Max5825_IRQ_GPIOC      GPIO_PIN_13     // PC13 IN_OP
#define Max5825_LOAD_GPIOC     GPIO_PIN_14     // PC14 OUTPUT_PP
//----------------------------------------------------//
// GPIO Digital Golden Finger MSA Define
//----------------------------------------------------//
#define IntL_G_Low()      gpio_bit_reset(GPIOC, IntL_GPIOC)
#define IntL_G_High()     gpio_bit_set(GPIOC, IntL_GPIOC)
//------------------------------------------------------------------//
// GPIO OUTPUT Define
//------------------------------------------------------------------//
#define P3V3_DSP_PowerUp()     gpio_bit_set(GPIOA, P3V3_DSP_EN_GPIOA)    
#define P3V3_RX_PowerUp()      gpio_bit_set(GPIOA, P3V3_RX_EN_GPIOA) 
#define P3V3_TX_PowerUp()      gpio_bit_set(GPIOA, P3V3_TX_EN_GPIOA) 
#define MODSELB_DSP_High()     gpio_bit_set(GPIOB, MODSELB_DSP_GPIOB) 
#define RESET_L_DSP_High()     gpio_bit_set(GPIOB, RESET_L_DSP_GPIOB)
#define LPMODE_DSP_High()      gpio_bit_set(GPIOB, LPMODE_DSP_GPIOB)
#define OSCEN_Pin_High()       gpio_bit_set(GPIOC, OSC_EN_GPIOC)
#define IntL_High()            gpio_bit_set(GPIOC, IntL_GPIOC)   
#define Max5825_CLR_High()     gpio_bit_set(GPIOB, Max5825_CLR_GPIOB)
#define Max5825_LOAD_High()    gpio_bit_set(GPIOC, Max5825_LOAD_GPIOC) 
 
#define P3V3_DSP_PowerDown()   gpio_bit_reset(GPIOA, P3V3_DSP_EN_GPIOA)
#define P3V3_RX_PowerDown()    gpio_bit_reset(GPIOA, P3V3_RX_EN_GPIOA)
#define P3V3_TX_PowerDown()    gpio_bit_reset(GPIOA, P3V3_TX_EN_GPIOA)
#define MODSELB_DSP_Low()      gpio_bit_reset(GPIOB, MODSELB_DSP_GPIOB) 
#define RESET_L_DSP_Low()      gpio_bit_reset(GPIOB, RESET_L_DSP_GPIOB)
#define LPMODE_DSP_Low()       gpio_bit_reset(GPIOB, LPMODE_DSP_GPIOB)
#define OSCEN_Pin_Low()        gpio_bit_reset(GPIOC, OSC_EN_GPIOC)
#define IntL_Low()             gpio_bit_reset(GPIOC, IntL_GPIOC) 
#define Max5825_CLR_Low()      gpio_bit_reset(GPIOB, Max5825_CLR_GPIOB)
#define Max5825_LOAD_Low()     gpio_bit_reset(GPIOC, Max5825_LOAD_GPIOC) 


