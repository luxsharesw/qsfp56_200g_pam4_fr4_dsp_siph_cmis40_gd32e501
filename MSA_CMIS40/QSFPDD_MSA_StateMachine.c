#include "gd32e501.h"
#include "core_cm33.h"
#include "CMIS_MSA.h"
#include <stdint.h>
#include "Macom_TIA_03819.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "GD32E501_M_I2C1_PB3_PB4.h"
#include "BRCM_DSP_87540.h"
#include "GD_FlahMap.h"
#include "systick.h"
#include "GD32E501_GPIO_Customize_Define.h"
//--------------------------------------------------------//
//MODULE State Byte 3
//--------------------------------------------------------//
#define MODULE_MGMT_INIT    0x0
#define MODULE_LOW_PWR      0x2
#define MODULE_PWR_UP       0x4
#define MODULE_READY        0x6
#define MODULE_PWR_DN       0x8
#define MODULE_FAULT        0xA
#define MODULE_Standby      0x55
//--------------------------------------------------------//
//DATA PATH State  Page11h 128 - 131
//--------------------------------------------------------//
#define Deactivated_DataPath  0x1
#define Init_DataPath         0x2
#define Deinit_DataPath       0x3
#define Activated_DataPath    0x4
#define TxTurnOn_DataPath     0x5
#define TxTurnOFF_DataPath    0x6
#define Initialized_DataPath  0x7
//--------------------------------------------------------//
// MSA SRAM 
//--------------------------------------------------------//
// Module_Global_Control is 0x00
uint8_t Bef_Module_Global_Control = 0x00 ;
uint8_t SoftWare_Init_flag = 0 ;
uint8_t DSP_MODE_SET = 0x00;
uint8_t Module_Status = 0 ;

uint8_t HW_Deinitialization_flag = 0;
uint8_t SW_Deinitialization_flag = 0;
uint8_t App_SELCode = 0 ;
uint8_t DDMI_Enable = 0 ;
uint8_t Error_code_Pass = 0x00 ;
uint8_t DDMI_Update_C = 0 ;
uint8_t PowerON_AWEN_flag = 0 ;
uint8_t AW_DDMI_Count = 0 ;
uint16_t  DDMI_DataCount = 0 ;
// DataPath_Power up 0xFF
uint8_t Bef_DataPath_Power = 0xFF;
//Control Flag
uint8_t TxDIS_Power_flag=0;
uint8_t Tx_output_flag=0;
uint8_t Rx_output_flag=0;

uint8_t Bef_APP_Byte145_Lan0 = 0x10 ;
uint8_t Bef_APP_Byte146_Lan1 = 0x10 ;
uint8_t Bef_APP_Byte147_Lan2 = 0x10 ;
uint8_t Bef_APP_Byte148_Lan3 = 0x10 ;
uint8_t Bef_APP_Byte149_Lan4 = 0x10 ;
uint8_t Bef_APP_Byte150_Lan5 = 0x10 ;
uint8_t Bef_APP_Byte151_Lan6 = 0x10 ;
uint8_t Bef_APP_Byte152_Lan7 = 0x10 ;
// Rx Los
uint8_t Bef_Rx_Los = 0x00 ;

uint8_t Bel_Tx_LOS_Buffer=0x00;
uint8_t Tx_Los_Enable_Flag=0;
uint8_t Rx_Los_Enable_Flag=0;
uint8_t Tx_Disable_Flag=0;

uint8_t Bel_Tx_Squelch_Buffer=0x00;
uint8_t Bel_Rx_Squelch_Buffer=0x00;

//PAM4 or NRZ Mode Select
uint8_t Signal_Status=0;
uint8_t ModuleDeactivatedT_Flag=0;
//------------------------------------------------------//
// Page01h checksum byte255 caculate between 130-254
//------------------------------------------------------//
void Calculate_Page01h_Checksum()
{
	uint8_t Checksum;
	uint8_t Buffer;
	uint8_t Index;
	Checksum=0x00;
	for(Index=2;Index<127;Index++)
	{
		Buffer=QSFPDD_P1[Index];
		Checksum=Checksum+Buffer;
	}
	QSFPDD_P1[127]=Checksum;
}

void Get_Selected_Chip_Mode()
{
    uint8_t Data_Buffer;
    //Get DSP Chip Mode LSB Data
    Data_Buffer = BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.DSP_CHIP_MODE>>8;

    //Mode 1 CHIP_MODE_4X53G_PAM4_4X53G_PAM4 DSP Default Mode
   	if(Data_Buffer==0x00)
    {
        DSP_MODE_SET=Mode1_4x53G_PAM4_4x53G_PAM4_1PORT;
        //PAM4 to PAM4 Mode
        Signal_Status=PAM4_to_PAM4;
    }
    //Mode 2 CHIP_MODE_2X53G_PAM4_2X53G_PAM4
    else if(Data_Buffer==0x01)
    {
        DSP_MODE_SET=Mode2_2x53G_PAM4_2x53G_PAM4_2PORT;
        //PAM4 to PAM4 Mode
        Signal_Status=PAM4_to_PAM4;
    }
    //Mode 3 CHIP_MODE_4X25G_NRZ_4X25G_NRZ
    else if(Data_Buffer==0x02)
    {
        DSP_MODE_SET=Mode3_4x25G_NRZ_4x25G_NRZ_1PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 4 CHIP_MODE_1X53G_PAM4_1X53G_PAM4
    else if(Data_Buffer==0x03)
    {
        DSP_MODE_SET = Mode4_1x53G_PAM4_1x53G_PAM4_4PORT;
        //PAM4 to PAM4 Mode
        Signal_Status=PAM4_to_PAM4;
    }
    //Mode 5 CHIP_MODE_4X26G_NRZ_2X53G_PAM4
    else if(Data_Buffer==0x04)
    {
        DSP_MODE_SET=Mode5_4x26G_NRZ_2x53G_PAM4_1PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 6 CHIP_MODE_2X26G_NRZ_1X53G_PAM4
    else if(Data_Buffer==0x05)
    {   
        DSP_MODE_SET=Mode6_2x26G_NRZ_1x53G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 7 CHIP_MODE_2X25G_NRZ_1X50G_PAM4
    else if(Data_Buffer==0x06)
    {
        DSP_MODE_SET=Mode7_2x25G_NRZ_1x50G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 8 CHIP_MODE_4X25G_NRZ_2X50G_PAM4
    else if(Data_Buffer==0x07)
    {
        DSP_MODE_SET=Mode8_4X25G_NRZ_2X50G_PAM4_1PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 9 CHIP_MODE_1X10G_NRZ_1X10G_NRZ
    else if(Data_Buffer==0x08)
    {
        DSP_MODE_SET=Mode9_1x10G_NRZ_1x10G_NRZ_4PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 10 CHIP_MODE_2X26G_NRZ_1X53G_PAM4
    else if(Data_Buffer==0x09)
    {
        DSP_MODE_SET=Mode10_2x26G_NRZ_1x53G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 11 CHIP_MODE_2X26G_NRZ_1X53G_PAM4
    else if(Data_Buffer==0x0A)
    {
        DSP_MODE_SET=Mode11_2x25G_NRZ_1x50G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 12 CHIP_MODE_4X26G_NRZ_4X26G_NRZ
    else if(Data_Buffer==0x0B)
    {
        DSP_MODE_SET=Mode12_4x26G_NRZ_4x26G_NRZ_1PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 13 CHIP_MODE_1X25G_NRZ_1X25G_NRZ
    else if(Data_Buffer==0x0C)
    {
        DSP_MODE_SET=Mode13_1x25G_NRZ_1x25G_NRZ_4PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 14 CHIP_MODE_2X26G_NRZ_2X26G_NRZ
    else if(Data_Buffer==0x0D)
    {
        DSP_MODE_SET=Mode14_2x26G_NRZ_2x26G_NRZ_2PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 15 CHIP_MODE_2X25G_NRZ_2X25G_NRZ
    else if(Data_Buffer==0x0E)
    {
        DSP_MODE_SET=Mode15_2x25G_NRZ_2x25G_NRZ_2PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 16 CHIP_MODE_2X25G_NRZ_1X53G_PAM4
    else if(Data_Buffer==0x0F)
    {
        DSP_MODE_SET=Mode16_2x25G_NRZ_1x53G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 17 CHIP_MODE_4X25G_NRZ_2X53G_PAM4
    else if(Data_Buffer==0x10)
    {
        DSP_MODE_SET=Mode17_4x25G_NRZ_2x53G_PAM4_1PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 18 CHIP_MODE_4X50G_PAM4_4X50G_PAM4
    else if(Data_Buffer==0x11)
    {
        DSP_MODE_SET=Mode18_4x50G_PAM4_4x50G_PAM4_1PORT;
        //PAM4 to PAM4 Mode
        Signal_Status=PAM4_to_PAM4;
    }
    //Mode 19 CHIP_MODE_2X25G_NRZ_1X53G_PAM4
    else if(Data_Buffer==0x12)
    {
        DSP_MODE_SET=Mode19_2x50G_NRZ_1x53G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
}

uint8_t Get_AppSelCode_Result(uint8_t Mode_Value)
{
    uint8_t Mode_Verfiy_Result=0;
    uint8_t Data_Buffer=0x00;
    
    Data_Buffer=(Mode_Value&0xF0);
    
    if(Data_Buffer<0x80)
    {
        Mode_Verfiy_Result=1;
    }
    
    return Mode_Verfiy_Result;
}
void Get_module_Power_Monitor()
{
    uint16_t GetADC_Buffer ;
    //--------------------------------------------------------------//
    // P1V8 Monitor
    //--------------------------------------------------------------//
//    GetADC_Buffer = GET_ADC_Value_Data( P1V8_Mon );
//    CALIB_MEMORY_1_MAP.TX_CDR_P1V8_ADC_MSB = GetADC_Buffer >> 8 ;
 //   CALIB_MEMORY_1_MAP.TX_CDR_P1V8_ADC_LSB = GetADC_Buffer ;
    //--------------------------------------------------------------//
    // TX P3V3 Monitor
    //--------------------------------------------------------------//  
    GetADC_Buffer = GET_ADC_Value_Data( P3V3_TX_Mon )*2 ;
    CALIB_MEMORY_1_MAP.TX_P3V3_ADC_MSB = GetADC_Buffer >> 8 ;
    CALIB_MEMORY_1_MAP.TX_P3V3_ADC_LSB = GetADC_Buffer ;
    //--------------------------------------------------------------//
    // RX P3V3 Monitor
    //--------------------------------------------------------------//  
    GetADC_Buffer = GET_ADC_Value_Data( P3V3_RX_Mon )*2 ;
    CALIB_MEMORY_1_MAP.RX_P3V3_ADC_MSB = GetADC_Buffer >> 8 ;
    CALIB_MEMORY_1_MAP.RX_P3V3_ADC_LSB = GetADC_Buffer ;    
    //--------------------------------------------------------------//
    // Temperature Monitor
    //--------------------------------------------------------------//   
    GetADC_Buffer = GET_GD_Temperature();   
	CALIB_MEMORY_1_MAP.TEMP_ADC_MSB = GetADC_Buffer >> 8 ;
    CALIB_MEMORY_1_MAP.TEMP_ADC_LSB = GetADC_Buffer;
    //--------------------------------------------------------------//
    // PowerCntrol Enable status
    //--------------------------------------------------------------//         
    CALIB_MEMORY_1_MAP.Power_C_Status  = Get_Power_C_Status();
    //--------------------------------------------------------------//
    // RSSI Raw Data Get
    //--------------------------------------------------------------//      
    RSSI_Raw_DATA_GET();
}

//------------------------------------------------------//
// Tx Squelch Enable 0x83
//------------------------------------------------------//
void MSA_Tx_Squelch_Control_P10H_83()
{
    uint8_t Data_Buffer=0x00;
    Data_Buffer=QSFPDD_P10[3];
    if(Tx_output_flag==0)
    {
        if(Bef_Tx_output!=QSFPDD_P10[3]) //Tx Squelch Disable Control
		{
            Tx_output_flag=1;
			Bef_Tx_output=QSFPDD_P10[3];
		}
    }
        
    else
    {
        // Tx1
        if(Data_Buffer&0x01)
            DSP87540_SystemSide_TRX_Squelch_SET( 0 , RX_Side , Function_EN );
        else
            DSP87540_SystemSide_TRX_Squelch_SET( 0 , RX_Side , Function_DIS );
        // Tx2
        if(Data_Buffer&0x02)
            DSP87540_SystemSide_TRX_Squelch_SET( 1 , RX_Side , Function_EN );
        else
            DSP87540_SystemSide_TRX_Squelch_SET( 1 , RX_Side , Function_DIS );
        // Tx3
        if(Data_Buffer&0x04)
            DSP87540_SystemSide_TRX_Squelch_SET( 2 , RX_Side , Function_EN );
        else
            DSP87540_SystemSide_TRX_Squelch_SET( 2 , RX_Side , Function_DIS );
        // Tx4
        if(Data_Buffer&0x08)
            DSP87540_SystemSide_TRX_Squelch_SET( 3 , RX_Side , Function_EN );
        else
            DSP87540_SystemSide_TRX_Squelch_SET( 3 , RX_Side , Function_DIS );
        
        Tx_output_flag=0;
    }
//	DSP87540_SystemSide_TRX_Squelch_SET( ALL_CHANNEL , RX_Side , Function_EN );
}

//------------------------------------------------------//
// Rx Squelch Enable 0x8A
//------------------------------------------------------//
void MSA_Rx_OUTPUT_Control_P10H_8A(uint8_t Control_Data)
{
    if(Rx_output_flag==0) 
    {
        if(Bef_Rx_output!=Control_Data)
        {
            Rx_output_flag=1;
            Bef_Rx_output=Control_Data;
        }
    }
    else
    {
        // Rx1
        if(Control_Data&0x01)
            DSP87540_SystemSide_TRX_Squelch_SET( 0 , TX_Side , Function_EN );
        else
            DSP87540_SystemSide_TRX_Squelch_SET( 0 , TX_Side , Function_DIS );
        // Rx2
        if(Control_Data&0x02)
            DSP87540_SystemSide_TRX_Squelch_SET( 1 , TX_Side , Function_EN );
        else
            DSP87540_SystemSide_TRX_Squelch_SET( 1 , TX_Side , Function_DIS );
        // Rx3
        if(Control_Data&0x04)
            DSP87540_SystemSide_TRX_Squelch_SET( 2 , TX_Side , Function_EN );
        else
            DSP87540_SystemSide_TRX_Squelch_SET( 2 , TX_Side , Function_DIS );
        // Rx4
        if(Control_Data&0x08)
            DSP87540_SystemSide_TRX_Squelch_SET( 3 , TX_Side , Function_EN );
        else
            DSP87540_SystemSide_TRX_Squelch_SET( 3 , TX_Side , Function_DIS );
        
        Rx_output_flag=0;
    }
}

//------------------------------------------------------//
// Lower Page Control
//------------------------------------------------------//
void Module_Global_Control_P0H_26()
{
	if( Bef_Module_Global_Control!=QSFPDD_A0[26] )
	{
		if( QSFPDD_A0[26] & 0x08 )
            __NVIC_SystemReset();
	}

	Bef_Module_Global_Control = QSFPDD_A0[26];
}

//----------------------------------------------------------//
// Page11h Data Path State 0x80 and Lane State Changed 0x86 //
//----------------------State List--------------------------//
// Deactivated_DataPath                                     //
// Deactivated_DataPath                                     //
// Init_DataPath                                            //
// Deinit_DataPath                                          //
// Activated_DataPath                                       //
// TxTurnOn_DataPath                                        //
// TxTurnOFF_DataPath                                       //
// Initialized_DataPath                                     //
//----------------------------------------------------------//
void MSA_DataPathState_P11H_80(uint8_t DataPathState)
{
	DataPathState |= (DataPathState<<4);
	//Lane Data Path State
	QSFPDD_P11[0] = DataPathState;
	QSFPDD_P11[1] = DataPathState;
	//Lane State Changed
	QSFPDD_P11[6] = 0x0F;
}

//------------------------------------------------------//
// Data Path Error_Applcation
//------------------------------------------------------//
void MSA_Configure_Appsel_CODE_Check()
{
	uint8_t Check_Pass_Count = 0 ;
	uint8_t FOR_COUNT = 0;
	//-----------------------------------------------------//
	// Lan0 page10 145
	//-----------------------------------------------------//
    if(Get_AppSelCode_Result(QSFPDD_P10[17]))
	{
		if(QSFPDD_P11[78]!=QSFPDD_P10[17])
		{
			// page10 145 value write to page11 206
			QSFPDD_P11[78]  = QSFPDD_P10[17];
			// feedback error code 202 lan1 successfully
			Configuration_Error_code_Feedback(0,Accepted);
			Error_code_Pass=0;
		}
	}
	else
	{
		// feedback error code 202 lan1 Rejected Invalid Code
		Configuration_Error_code_Feedback(0,Rejected_Invalid_Code);
		QSFPDD_P11[78]  = 0x00;
	}

	//-----------------------------------------------------//
	// Lan1 page10 146
	//-----------------------------------------------------//
    if(Get_AppSelCode_Result(QSFPDD_P10[18]))
	{
		if(QSFPDD_P11[79]!=QSFPDD_P10[18])
		{
			// page10 146 value write to page11 207
			QSFPDD_P11[79]  = QSFPDD_P10[18];
			// feedback error code 202 lan1 successfully
			Configuration_Error_code_Feedback(1,Accepted);
			Error_code_Pass=0;
		}
	}
	else
	{
		// feedback error code 202 lan1 Rejected Invalid Code
		Configuration_Error_code_Feedback(1,Rejected_Invalid_Code);
		QSFPDD_P11[79]  = 0x00;
	}

	//-----------------------------------------------------//
	// Lan2 page10 147
	//-----------------------------------------------------//
    if(Get_AppSelCode_Result(QSFPDD_P10[19]))
	{
		if(QSFPDD_P11[80]!=QSFPDD_P10[19])
		{
			// page10 147 value write to page11 208
			QSFPDD_P11[80]  = QSFPDD_P10[19];
			// feedback error code 203 lan1 successfully
			Configuration_Error_code_Feedback(2,Accepted);
			Error_code_Pass=0;
		}
	}
	else
	{
		// feedback error code 203 lan1 Rejected Invalid Code
		Configuration_Error_code_Feedback(2,Rejected_Invalid_Code);
		QSFPDD_P11[80]  = 0x00;
	}

	//-----------------------------------------------------//
	// Lan3 page10 148
	//-----------------------------------------------------//
    if(Get_AppSelCode_Result(QSFPDD_P10[20]))
	{
		if(QSFPDD_P11[81]!=QSFPDD_P10[20])
		{
			// page10 148 value write to page11 209
			QSFPDD_P11[81]  = QSFPDD_P10[20];
			// feedback error code 203 lan1 successfully
			Configuration_Error_code_Feedback(3,Accepted);
			Error_code_Pass=0;
		}
	}
	else
	{
		// feedback error code 203 lan1 Rejected Invalid Code
		Configuration_Error_code_Feedback(3,Rejected_Invalid_Code);
		QSFPDD_P11[81]  = 0x00;
	}
    
	//  confirm 206 - 213 value is sams or not ?
	for(FOR_COUNT=0;FOR_COUNT<4;FOR_COUNT++)
	{
		if( QSFPDD_P11[78+FOR_COUNT] == QSFPDD_P10[17+FOR_COUNT])
			Check_Pass_Count++;
	}

	if(Check_Pass_Count==4)
	{
		if((QSFPDD_P11[78] & 0x70 ) == 0x70)
		{
			DSP_MODE_SET = Mode9_1x10G_NRZ_1x10G_NRZ_4PORT ;
            //NRZ Mode
            Signal_Status=NRZ_to_NRZ;
		}
		else if((QSFPDD_P11[78] & 0x60 ) == 0x60)
		{
			DSP_MODE_SET = Mode13_1x25G_NRZ_1x25G_NRZ_4PORT ;
            //NRZ Mode
            Signal_Status=NRZ_to_NRZ;
		}
		else if((QSFPDD_P11[78] & 0x50 ) == 0x50)
		{
			DSP_MODE_SET = Mode4_1x53G_PAM4_1x53G_PAM4_4PORT ;
            //PAM4 Mode
            Signal_Status=PAM4_to_PAM4;
		}
		else if((QSFPDD_P11[78] & 0x40 ) == 0x40)
		{
			DSP_MODE_SET = Mode2_2x53G_PAM4_2x53G_PAM4_2PORT ;
            //PAM4 Mode
            Signal_Status=PAM4_to_PAM4;
		}
		else if((QSFPDD_P11[78] & 0x30 ) == 0x30)
		{
			DSP_MODE_SET = Mode9_1x10G_NRZ_1x10G_NRZ_1PORT ;
            //NRZ Mode
            Signal_Status=NRZ_to_NRZ;
		}
		else if((QSFPDD_P11[78] & 0x20 ) == 0x20)
		{
			DSP_MODE_SET = Mode3_4x25G_NRZ_4x25G_NRZ_1PORT ;
            //NRZ Mode
            Signal_Status=NRZ_to_NRZ;
		}
		else if((QSFPDD_P11[78] & 0x10 ) == 0x10)
		{
			DSP_MODE_SET = Mode1_4x53G_PAM4_4x53G_PAM4_1PORT ;
            //PAM4 Mode
            Signal_Status=PAM4_to_PAM4;
		}
        GDMCU_FMC_READ_FUNCTION( FS_QSFPDD_P1 , &QSFPDD_P1[0] , 128 );      //Update default page01 page data
        if(Signal_Status)
        {
            //For Select NRZ Mode to auto set rx output limit values
            QSFPDD_P1[25]=0x70;		//Max Rx Output Amplitude Control Value
			QSFPDD_P1[26]=0x33;		//Max Rx Pre Post Control Value
        }
        Calculate_Page01h_Checksum();
        //Only set value one time,make sure SDD_MSA_CTLE_PRE_POST_AMP_CONTROL can work
        if(Error_code_Pass==0)
            Init_Rx_Output_Values();
        Error_code_Pass = 1 ;
	}
}

//------------------------------------------------------//
// TX_DIS_FUNCTION
//------------------------------------------------------//
void TX_DIS_FUNCTION(uint8_t TxDIS_Control )
{
    if(TxDIS_Power_flag==0)
    {
        if(Bef_TxDIS_Power!=TxDIS_Control) //Tx Disable Control
		{
            TxDIS_Power_flag=1;
			Bef_TxDIS_Power=TxDIS_Control;
		}
    }
	// Macom 38435 IC1
	// HW Tx8 - Tx3
	// HW Tx7 - Tx2
	// HW Tx6 - Tx1
	// HW Tx5 - Tx0
	//--------------------------------------------------//
	// Tx1 MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE
	//--------------------------------------------------//
    if(TxDIS_Power_flag==1)
    {
/*if( TxDIS_Control & 0x01 )
            MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE |= 0x01 ;
        else
            MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE &= ~0x01 ;
        //--------------------------------------------------//
        // Tx2
        //--------------------------------------------------//
        if( TxDIS_Control & 0x02 )
            MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE |= 0x02 ;
        else
            MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE &= ~0x02 ;
        //--------------------------------------------------//
        // Tx3
        //--------------------------------------------------//
        if( TxDIS_Control & 0x04 )
            MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE |= 0x04 ;
        else
            MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE &= ~0x04 ;
        //--------------------------------------------------//
        // Tx4
        //--------------------------------------------------//
        if( TxDIS_Control & 0x08 )
            MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE |= 0x08 ;
        else
            MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE &= ~0x08 ;

        // Tx1-Tx4 I2C is Master IC1
        Master_I2C1_ByteWrite_PB34( MALD38435_SADR , 0x1D , 0x7F );
        Master_I2C1_ByteWrite_PB34( MALD38435_SADR , 0x41 , MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE );
     */   
        TxDIS_Power_flag=0;
    }
}

//------------------------------------------------------//
// Lower Page Control                                   //
// Module State 0x03 and State Changed Flag 0x08        //
//------------------------------------------------------//
void Module_State_P0H_03(ModuleState)
{
	//Module State Change
	QSFPDD_A0[3] = ModuleState ;
	//Module State Changed Flag
	QSFPDD_A0[8] |= 0x01 ;
}

void QSFPDD_DDMI_ONLY_ADC_GET()
{
    Temperature_Monitor(GET_GD_Temperature());
    VCC_Monitor( GET_ADC_Value_Data( P3V3_TX_Mon )*2 );
    //Get_module_Power_Monitor();
}

void MSA_DDMI_Function()
{
	// DDMI Function update
	if( DDMI_DataCount > 200 )
	{
		if( CALIB_MEMORY_MAP.DDMI_DISABLE == 1)
            QSFPDD_DDMI_ONLY_ADC_GET();
		else
		{
			// Status Machine Loop
			QSFPDD_DDMI_StateMachine( DDMI_Update_C );
			DDMI_Update_C++;
			//Status Machine flag
			if( DDMI_Update_C >= 10 )
			{
				DDMI_Update_C = 0;
				if(PowerON_AWEN_flag==0)
				{
					AW_DDMI_Count ++ ;
					if(AW_DDMI_Count>2)
						PowerON_AWEN_flag = 1;
				}
			}
		}
		DDMI_DataCount = 0;
	}
	DDMI_DataCount++;
}

void DSP_Tx_Los_Check()
{
	uint8_t Data_buffer=0x00;
    
    if( CALIB_MEMORY_MAP.SKIP_DSP_Tx_Los_Check == 0)
    {
        if(Tx_Los_Enable_Flag==0)
        {
            if(Bel_Tx_LOS_Buffer!=Tx_LOS_Buffer)
            {
                Tx_Los_Enable_Flag=1;
                Bel_Tx_LOS_Buffer=Tx_LOS_Buffer;
            }
            //For Tx Squelch Disable
			if(Bel_Tx_Squelch_Buffer!=QSFPDD_P10[3])
			{
				Tx_Los_Enable_Flag = 1 ;
				Bel_Tx_Squelch_Buffer=QSFPDD_P10[3];
			}
        }
        else
        {
            //4 Port Mode
            if((DSP_MODE_SET==Mode4_1x53G_PAM4_1x53G_PAM4_4PORT)||
               (DSP_MODE_SET==Mode13_1x25G_NRZ_1x25G_NRZ_4PORT)||
               (DSP_MODE_SET==Mode9_1x10G_NRZ_1x10G_NRZ_4PORT))
            {
                Data_buffer=Tx_LOS_Buffer;
                //Data Path State Lane0
                if(Data_buffer&0x01)
                    QSFPDD_P11[0]=((QSFPDD_P11[0]&= ~0x0F)|Initialized_DataPath);
                else
                    QSFPDD_P11[0]=((QSFPDD_P11[0]&= ~0x0F)|Activated_DataPath);
                //Data Path State Lane1
                if(Data_buffer&0x02)
                    QSFPDD_P11[0]=((QSFPDD_P11[0]&= ~0xF0)|(Initialized_DataPath<<4));
                else
                    QSFPDD_P11[0]=((QSFPDD_P11[0]&= ~0xF0)|(Activated_DataPath<<4));
                //Data Path State Lane2
                if(Data_buffer&0x04)
                    QSFPDD_P11[1]=((QSFPDD_P11[1]&= ~0x0F)|Initialized_DataPath);
                else
                    QSFPDD_P11[1]=((QSFPDD_P11[1]&= ~0x0F)|Activated_DataPath);
                //Data Path State Lane3
                if(Data_buffer&0x08)
                    QSFPDD_P11[1]=((QSFPDD_P11[1]&= ~0xF0)|(Initialized_DataPath<<4));
                else
                    QSFPDD_P11[1]=((QSFPDD_P11[1]&= ~0xF0)|(Activated_DataPath<<4));
                
            }
            //2 port Mode
            else if(DSP_MODE_SET==Mode2_2x53G_PAM4_2x53G_PAM4_2PORT)
            {
                if( (Tx_LOS_Buffer&0x03)>0 )
                {
                    Data_buffer |= 0x03 ;
                    QSFPDD_P11[0]=(Initialized_DataPath|Initialized_DataPath<<4);
                }
                else
                {
                    Data_buffer &= ~0x03 ;
                    QSFPDD_P11[0]=(Activated_DataPath|Activated_DataPath<<4);
                }

                if( (Tx_LOS_Buffer&0x0C)>0 )
                {
                    Data_buffer |= 0x0C ;
                    QSFPDD_P11[1]=(Initialized_DataPath|Initialized_DataPath<<4);
                }
                else
                {
                    Data_buffer &= ~0x0C ;
                    QSFPDD_P11[1]=(Activated_DataPath|Activated_DataPath<<4);
                }
            }
            //1 Port Mode
            else if(Tx_LOS_Buffer!=0x00)
            {
                if(Tx_LOS_Buffer>0)
                {
                    Data_buffer |= 0x0F ;
                    MSA_DataPathState_P11H_80(Initialized_DataPath);
                }
                else
                    Data_buffer = 0x00 ;
            }
            else
                MSA_DataPathState_P11H_80(Activated_DataPath);
            
            //For LowPwrS is TRUE Condition
            if(Data_buffer==0x0F)
            {
                ModuleDeactivatedT_Flag=1;
            }
            else
            {
                ModuleDeactivatedT_Flag=0;
            }
			//For Tx Squelch Disable
            if(QSFPDD_P10[3]!=0x00)
            {
				//Lane0-Lane3
				if(QSFPDD_P10[3]&0x01)
					Data_buffer &= ~0x01;
				if(QSFPDD_P10[3]&0x02)
					Data_buffer &= ~0x02;
				if(QSFPDD_P10[3]&0x04)
					Data_buffer &= ~0x04;
				if(QSFPDD_P10[3]&0x08)
					Data_buffer &= ~0x08;
            }
            Tx_Disable_Flag=Data_buffer;
            TxDIS_Power_flag=1;
            TX_DIS_FUNCTION( Data_buffer );
            Tx_Los_Enable_Flag=0;
        }
    }
    //for calibration station
    else
    {
        Tx_Disable_Flag=0;
    }
}

//------------------------------------------------------//
// Modeule_MGMT_INIT
//------------------------------------------------------//
void Modeule_MGMT_INIT()
{
	// LDD Disable
	//TX_DIS_FUNCTION(0xFF);
	//DSP to low Power Mode
    LPMODE_DSP_High();

	if( CALIB_MEMORY_MAP.QDD_SoftInitM_EN == 0x01 )
	{
		if(gpio_input_bit_get(GPIOF, LPMODE_G_GPIOF)==1)
		{
			// Software Initialization
			SoftWare_Init_flag = 1 ;
			QSFPDD_P11[6] = 0x0F ;
		}
		else
		{
			// Hardware Initialization
			SoftWare_Init_flag = 0 ;
		}
	}
	else
	{	// Hardware Initialization
		SoftWare_Init_flag = 2 ;
	}

    // bit4 Force LowPwr = 0
	// Bit6 LowPwr = 1
	QSFPDD_A0[26] = 0x40 ;
	// DataPathDeinit=00h
	QSFPDD_P10[0] = 0x00;
    //Arista Default Mode is 1 Port 4x53 PAM4 to 4x53 PAM4
    DSP_MODE_SET=Mode1_4x53G_PAM4_4x53G_PAM4_1PORT;
    //PAM4 Mode
    Signal_Status=PAM4_to_PAM4;
    //Get_Selected_Chip_Mode();
    //Init Rx Pre Post Amp Values
	Init_Rx_Output_Values();
	MSA_DataPathState_P11H_80 (Deactivated_DataPath);
	Module_Status = MODULE_LOW_PWR;
    Module_State_P0H_03(Module_Status);
    IntL_G_Low();
}

//------------------------------------------------------//
// QSFPDD_MSA_StateMachine
//------------------------------------------------------//
void QSFPDD_MSA_StateMachine()
{
	switch( Module_Status )
	{
		case MODULE_MGMT_INIT:
			 Modeule_MGMT_INIT();
			 break;

		case MODULE_LOW_PWR:
			 // Software Init Mode
			 if(SoftWare_Init_flag==1)
			 {
				 if((QSFPDD_A0[26]&0x50)==0x00)
				 {
                    Module_Status = MODULE_PWR_UP ;
                    Module_State_P0H_03(Module_Status);
                    IntL_G_Low();
                    //For DSP_Tx_Los_Check check again to change data path state by tx lol
                    Tx_Los_Enable_Flag=1;
				 }
			 }
			 // HW Init Mode
			 else
			 {
	 			 if(HW_Deinitialization_flag==0)
				 {
                    Module_Status = MODULE_PWR_UP ;
                    Module_State_P0H_03(Module_Status);
                    IntL_G_Low();
				 }
				 else
				 {
					 if(gpio_input_bit_get(GPIOF, LPMODE_G_GPIOF)==0)
					 {
                        Module_Status = MODULE_PWR_UP ;
                        Module_State_P0H_03(Module_Status);
                        IntL_G_Low();
					 }
				 }
			 }

			 break;

		case MODULE_PWR_UP:

			 if( ( HW_Deinitialization_flag==0 ) && ( SW_Deinitialization_flag == 0 ) )
			 {
				 // DSP to High Power Mode
                 LPMODE_DSP_Low(); 
				 // Disable I2C
				 //i2c_disable(I2C0);
                 delay_1ms(75);
				 DSP87540_Init(DSP_MODE_SET);
				 // Enable I2C
				 //i2c_enable(I2C0); 
			 }

			 Module_Status = MODULE_READY ;
		     Module_State_P0H_03(Module_Status);
             IntL_G_Low();

			 HW_Deinitialization_flag = 0;
			 SW_Deinitialization_flag = 0;

			 if(QSFPDD_P10[0]&0x0F)
				 App_SELCode = 1 ;		  // Software Configuration and Initialization App sel code write to error code data
			 else
				 App_SELCode = 0 ;        // Quick Software Initialization

			 break;

		case MODULE_READY:
            //Quick Init Process
            if( App_SELCode == 0 )
            {
                Module_Status = MODULE_Standby ;
                DDMI_Enable = 1;			// DDMI Function is show eeprom
                // All Tx output enable
                TX_DIS_FUNCTION(0x00);
                // DataPath Activated
                MSA_DataPathState_P11H_80 (Activated_DataPath);
            }
            //Config Mode Process
            else
            {	  
                //Config Mode By AppSelCode
                if(QSFPDD_P10[15]&0x0F)
                {
                    //Config Mode By Page10h 0x8F Apply_DataPathInit
                    MSA_Configure_Appsel_CODE_Check();
                }
                else
                {
                    //Page11h init Appselcode values
                    QSFPDD_P11[78]=0x00;
                    QSFPDD_P11[79]=0x00;
                    QSFPDD_P11[80]=0x00;
                    QSFPDD_P11[81]=0x00;
                }
                if(Error_code_Pass==1)
                {
                    //Config SI Controls By Page10h 0x90 Apply_Immediate
                    QDD_MSA_CTLE_PRE_POST_AMP_CONTROL();
                }
                //SW Deinit to MODULE_LOW_PWR Status
                if( QSFPDD_A0[26] & 0x40 )
                {
                    Module_Status = MODULE_LOW_PWR ;
                    Module_State_P0H_03(Module_Status);
                    SW_Deinitialization_flag = 1;
                    IntL_G_Low();
                }
                //Exit Config Mode
                if( QSFPDD_P10[0] == 0x00 )
                {
                    QSFPDD_P10[15] = 0x00;
                    //Config Pass to change to select mode
                    if(Error_code_Pass==1)
                    {
                         // Disable I2C
                         //i2c_disable(I2C0);
                         DSP87540_Init(DSP_MODE_SET); 
                         //Enable I2C
                         //i2c_enable(I2C0); 
                         IntL_G_Low();
                         Module_Status = MODULE_Standby ;
                         DDMI_Enable = 1;
                         // All Tx output enable
                         TX_DIS_FUNCTION(0x00);
                         // DataPath  Activated
                         MSA_DataPathState_P11H_80(Activated_DataPath);
                         Error_code_Pass = 0;
                    }
                    //Config Fail or not select any Mode, use default mode
                    else
                    {
                        // Disable I2C
                        //i2c_disable(I2C0);
                        DSP87540_Init(DSP_MODE_SET); 
                        //Enable I2C
                        //i2c_enable(I2C0); 
                        IntL_G_Low();
                        Module_Status = MODULE_Standby ;
                        DDMI_Enable = 1;
                        // All Tx output enable
                        TX_DIS_FUNCTION(0x00);
                        // DataPath  Activated
                        MSA_DataPathState_P11H_80(Activated_DataPath);
                        IntL_G_Low();
                        //Page11h Appselcode to PAM4 mode
                        QSFPDD_P11[78]=0x11;
                        QSFPDD_P11[79]=0x11;
                        QSFPDD_P11[80]=0x11;
                        QSFPDD_P11[81]=0x11;
                    }
                }
            }
            break;

	    // Power On Init ready go to MODULE_Standby
		case MODULE_Standby:
             // HW InitMode
			 if(SoftWare_Init_flag == 0)
			 {
				 //HW Deinit Process
				 if(gpio_input_bit_get(GPIOF, LPMODE_G_GPIOF)==1)
				 {
			         Module_Status = MODULE_LOW_PWR ;
				     Module_State_P0H_03(Module_Status);
                     IntL_G_Low();
				     // DataPath  DeActivated and Module status Changed
					 MSA_DataPathState_P11H_80 (Deactivated_DataPath);
					 //config error code no status
					 Configuration_Error_code_Feedback( All_CH , No_Status);
                     // LDD output Disable
				     TX_DIS_FUNCTION(0xFF);
				     HW_Deinitialization_flag = 1;
				 }
			 }
             // Software InitMode
			 else
			 {
				 //SW Deinit Process
				 if ( QSFPDD_P10[0] & 0x0F )
				 {
                     // LDD output Disable
                     TX_DIS_FUNCTION(0xFF);
                     // DataPath  DeActivated and Lane State Changed
                     MSA_DataPathState_P11H_80(Deactivated_DataPath);
                     //Config error code no status
                     Configuration_Error_code_Feedback( All_CH , No_Status);
                     Module_Status = MODULE_READY;
                     //Use Config Mode Process
                     App_SELCode = 1;
				 }

                 //LowPwrExS is TRUE Function
				 if(ModuleDeactivatedT_Flag)
				 {
					 if((QSFPDD_A0[26]&0x40)||(QSFPDD_A0[26]&0x10))
					 {
						 Module_Status = MODULE_LOW_PWR ;
						 Module_State_P0H_03(Module_Status);
						 SW_Deinitialization_flag = 1;
						 IntL_G_Low();
					 }
				 }
                 
			 }
			 break;
             
		case MODULE_PWR_DN:
			 break;

		case MODULE_FAULT:
			 break;
	}

	if( Module_Status != MODULE_READY )
        QSFPDD_DDMI_ONLY_ADC_GET();

	if(DDMI_Enable)
		MSA_DDMI_Function();
	
	//After hw or sw deinit don't need to do those function
	if(( Module_Status == MODULE_Standby )&&( HW_Deinitialization_flag==0 )&&( SW_Deinitialization_flag==0 ))
	{

        //MSA_Tx_Squelch_Control_P10H_83();  //Tx Squelch Disable Control
        MSA_Rx_OUTPUT_Control_P10H_8A(QSFPDD_P10[10]);   //Rx Output Disable Control
        TX_DIS_FUNCTION(QSFPDD_P10[2]);    //Tx Disable Control

        // DSP output level settting
		QDD_MSA_CTLE_PRE_POST_AMP_CONTROL();

        DSP_Tx_Los_Check();					//Auto Enable or Disable Tx Output By System Side Input Los Status
		P13_diagnostic_features();			// DSP Loopback/PRBS GEN function
	}
    // software reset
	Module_Global_Control_P0H_26();
}


