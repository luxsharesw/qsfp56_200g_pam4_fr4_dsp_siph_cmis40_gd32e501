#include "gd32e501.h"
#include "CMIS_MSA.h"
#include <stdint.h>
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "BRCM_DSP5_87540_Optional.h"
#include "GD32E501_M_I2C1_PB3_PB4.h"
#include "LDD_Control.h"
#include "Macom_TIA_03819.h"
#include "GD32E501_GPIO_Customize_Define.h"

uint8_t Temperature_LATCH = 0;
uint8_t VCC_Temperature_LATCH = 0;
uint8_t TxP_HA_LATCH = 0;
uint8_t TxP_LA_LATCH = 0;
uint8_t TxP_HW_LATCH = 0;
uint8_t TxP_LW_LATCH = 0;

uint8_t BIAS_HA_LATCH = 0;
uint8_t BIAS_LA_LATCH = 0;
uint8_t BIAS_HW_LATCH = 0;
uint8_t BIAS_LW_LATCH = 0;

uint8_t RxP_HA_LATCH = 0;
uint8_t RxP_LA_LATCH = 0;
uint8_t RxP_HW_LATCH = 0;
uint8_t RxP_LW_LATCH = 0;

uint8_t Rx_LOS_Buffer = 0x00;
uint8_t Tx_LOS_Buffer = 0x00;

uint16_t IntL_Status_TX = 0;
uint16_t IntL_Status_RX = 0;
extern uint8_t Tx_Los_Enable_Flag;

void QSFPDD_Temperature_VCC_AW()
{
	int16_t T_Temp;
	int16_t High_Alarm_Value, Low_Alarm_Value, High_Warning, Low_Warning;
	uint16_t VCC_Temp;
	uint16_t VCC_High_Alarm_Value, VCC_Low_Alarm_Value, VCC_High_Warning, VCC_Low_Warning;
	uint8_t LATCH = 0;
	uint8_t MASK_FLAG=0;

	T_Temp           = ((int16_t)( QSFPDD_A0[14]<< 8 ) | QSFPDD_A0[15] );
	High_Alarm_Value = ((int16_t)( QSFPDD_P2[0] << 8 ) | QSFPDD_P2[1] );
	Low_Alarm_Value  = ((int16_t)( QSFPDD_P2[2] << 8 ) | QSFPDD_P2[3] );
	High_Warning     = ((int16_t)( QSFPDD_P2[4] << 8 ) | QSFPDD_P2[5] );
	Low_Warning      = ((int16_t)( QSFPDD_P2[6] << 8 ) | QSFPDD_P2[7] );

	VCC_Temp             = ((uint16_t)( QSFPDD_A0[16] << 8 ) | QSFPDD_A0[17] );
	VCC_High_Alarm_Value = ((uint16_t)( QSFPDD_P2[8]  << 8 ) | QSFPDD_P2[9] );
	VCC_Low_Alarm_Value  = ((uint16_t)( QSFPDD_P2[10] << 8 ) | QSFPDD_P2[11] );
	VCC_High_Warning     = ((uint16_t)( QSFPDD_P2[12] << 8 ) | QSFPDD_P2[13] );
	VCC_Low_Warning      = ((uint16_t)( QSFPDD_P2[14] << 8 ) | QSFPDD_P2[15] );

	// Temp. VCC Mask flag Address byte32
	MASK_FLAG = QSFPDD_A0[32];

	if( T_Temp < Low_Warning )               // set Low  warning flag bit4
		LATCH |= 0x08;
	if( T_Temp > High_Warning )              // set high warning flag bit3
		LATCH |= 0x04;
	if( T_Temp < Low_Alarm_Value )           // set Low  alarm   flag bit2
		LATCH |= 0x02;
	if( T_Temp > High_Alarm_Value )          // set high alarm   flag bit1
		LATCH |= 0x01;

	if ( VCC_Temp < VCC_Low_Warning )        // set Low  warning flag bit7
		LATCH |= 0x80;
	if ( VCC_Temp > VCC_High_Warning )       // set High warning flag bit6
		LATCH |= 0x40;
	if ( VCC_Temp < VCC_Low_Alarm_Value )    // set Low  alarm   flag bit5
		LATCH |= 0x20;
	if ( VCC_Temp > VCC_High_Alarm_Value )   // set High alarm   flag bit4
		LATCH |= 0x10;

	if ((QSFPDD_A0[9] > 0) && (LATCH == 0))
		LATCH = QSFPDD_A0[9];

	VCC_Temperature_LATCH = LATCH;

	QSFPDD_A0[9] = LATCH;

	if( LATCH & (~MASK_FLAG) )
	{
		IntL_G_Low();
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= VCC_TEMP_INT;
	}
}

void QSFP28_TxPower_AW()
{
	uint16_t TXPOWER_Temp[8];
	uint8_t Channel;
	uint16_t High_Alarm_Value, Low_Alarm_Value, High_Warning_Value, Low_Warning_Value;
	uint8_t LOW_A_LATCH  = 0;
	uint8_t HIGH_A_LATCH = 0;
	uint8_t LOW_W_LATCH  = 0;
	uint8_t HIGH_W_LATCH = 0;

	TXPOWER_Temp[0] = ( (uint16_t)( QSFPDD_P11[26] << 8) | QSFPDD_P11[27] );
	TXPOWER_Temp[1] = ( (uint16_t)( QSFPDD_P11[28] << 8) | QSFPDD_P11[29] );
	TXPOWER_Temp[2] = ( (uint16_t)( QSFPDD_P11[30] << 8) | QSFPDD_P11[31] );
	TXPOWER_Temp[3] = ( (uint16_t)( QSFPDD_P11[32] << 8) | QSFPDD_P11[33] );
	TXPOWER_Temp[4] = ( (uint16_t)( QSFPDD_P11[34] << 8) | QSFPDD_P11[35] );
	TXPOWER_Temp[5] = ( (uint16_t)( QSFPDD_P11[36] << 8) | QSFPDD_P11[37] );
	TXPOWER_Temp[6] = ( (uint16_t)( QSFPDD_P11[38] << 8) | QSFPDD_P11[39] );
	TXPOWER_Temp[7] = ( (uint16_t)( QSFPDD_P11[40] << 8) | QSFPDD_P11[41] );

	High_Alarm_Value   = ( (uint16_t)( QSFPDD_P2[48] << 8)| QSFPDD_P2[49] );
	Low_Alarm_Value    = ( (uint16_t)( QSFPDD_P2[50] << 8)| QSFPDD_P2[51] );
	High_Warning_Value = ( (uint16_t)( QSFPDD_P2[52] << 8)| QSFPDD_P2[53] );
	Low_Warning_Value  = ( (uint16_t)( QSFPDD_P2[54] << 8)| QSFPDD_P2[55] );
    //Only use four channel
	for(Channel = 0; Channel <= 3; Channel++)
	{
		// CH0 - CH 7 High Alarm Flag setting
		if( TXPOWER_Temp[Channel] > High_Alarm_Value )
			HIGH_A_LATCH |= ( 1 << Channel );
		// CH0 - CH 7 Low Alarm Flag setting
		if( TXPOWER_Temp[Channel] < Low_Alarm_Value )
			LOW_A_LATCH  |= ( 1 << Channel );
		// CH0 - CH 7 High Warning Flag setting
		if( TXPOWER_Temp[Channel] > High_Warning_Value )
			HIGH_W_LATCH |= ( 1 << Channel );
		// CH0 - CH 7 Low Warning Flag setting
		if( TXPOWER_Temp[Channel] < Low_Warning_Value )
			LOW_W_LATCH  |= ( 1 << Channel );
	}

	//-------------------------------------------------------------//
	// High / Low Alarm & Warning Flag write Value
	//-------------------------------------------------------------//
	if ( ( QSFPDD_P11[11] > 0 ) && ( HIGH_A_LATCH == 0 ))
		HIGH_A_LATCH = QSFPDD_P11[11];
	if ( ( QSFPDD_P11[12] > 0 ) && ( LOW_A_LATCH  == 0 ))
		LOW_A_LATCH = QSFPDD_P11[12];
	if ( ( QSFPDD_P11[13] > 0 ) && ( HIGH_W_LATCH == 0 ))
		HIGH_W_LATCH = QSFPDD_P11[13];
	if ( ( QSFPDD_P11[14] > 0 ) && ( LOW_W_LATCH  == 0 ))
		LOW_W_LATCH = QSFPDD_P11[14];

	TxP_HA_LATCH = HIGH_A_LATCH ;
	TxP_LA_LATCH = LOW_A_LATCH ;
	TxP_HW_LATCH = HIGH_W_LATCH ;
	TxP_LW_LATCH = LOW_W_LATCH ;

	QSFPDD_P11[11] = HIGH_A_LATCH ;
	QSFPDD_P11[12] = LOW_A_LATCH ;
	QSFPDD_P11[13] = HIGH_W_LATCH ;
	QSFPDD_P11[14] = LOW_W_LATCH ;

	//Page11 Byte 0x85 Flag Summary
	//QSFPDD_P11[5] = HIGH_A_LATCH | LOW_A_LATCH | HIGH_W_LATCH | LOW_W_LATCH ;

	if( TxP_HA_LATCH & (~QSFPDD_P10[90]))
	{
		IntL_G_Low();
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_TX |= TXPHA_INT ;
	}
	if( TxP_LA_LATCH & (~QSFPDD_P10[91]))
	{
		IntL_G_Low();
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_TX |= TXPLA_INT ;
	}
	if( TxP_HW_LATCH & (~QSFPDD_P10[92]))
	{
		IntL_G_Low();
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_TX |= TXPHW_INT ;
	}
	if( TxP_LW_LATCH & (~QSFPDD_P10[93]))
	{
		IntL_G_Low();
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_TX |= TXPLW_INT ;
	}
}

void QSFP28_Bias_AW()
{
	uint16_t Bias_Temp[8];
	uint8_t Channel;
	uint16_t High_Alarm_Value, Low_Alarm_Value, High_Warning_Value, Low_Warning_Value;
	uint8_t LOW_A_LATCH  = 0;
	uint8_t HIGH_A_LATCH = 0;
	uint8_t LOW_W_LATCH  = 0;
	uint8_t HIGH_W_LATCH = 0;

	Bias_Temp[0] = ((uint16_t)( QSFPDD_P11[42] << 8 ) | QSFPDD_P11[43] );
	Bias_Temp[1] = ((uint16_t)( QSFPDD_P11[44] << 8 ) | QSFPDD_P11[45] );
	Bias_Temp[2] = ((uint16_t)( QSFPDD_P11[46] << 8 ) | QSFPDD_P11[47] );
	Bias_Temp[3] = ((uint16_t)( QSFPDD_P11[48] << 8 ) | QSFPDD_P11[49] );
	Bias_Temp[4] = ((uint16_t)( QSFPDD_P11[50] << 8 ) | QSFPDD_P11[51] );
	Bias_Temp[5] = ((uint16_t)( QSFPDD_P11[52] << 8 ) | QSFPDD_P11[53] );
	Bias_Temp[6] = ((uint16_t)( QSFPDD_P11[54] << 8 ) | QSFPDD_P11[55] );
	Bias_Temp[7] = ((uint16_t)( QSFPDD_P11[56] << 8 ) | QSFPDD_P11[57] );

	High_Alarm_Value   = ( (uint16_t)( QSFPDD_P2[56] << 8)| QSFPDD_P2[57] );
	Low_Alarm_Value    = ( (uint16_t)( QSFPDD_P2[58] << 8)| QSFPDD_P2[59] );
	High_Warning_Value = ( (uint16_t)( QSFPDD_P2[60] << 8)| QSFPDD_P2[61] );
	Low_Warning_Value  = ( (uint16_t)( QSFPDD_P2[62] << 8)| QSFPDD_P2[63] );
    //Only use four channel
	for( Channel = 0; Channel <= 3; Channel++ )
	{
		// CH0 - CH 7 High Alarm Flag setting
		if( Bias_Temp[Channel] > High_Alarm_Value )
			HIGH_A_LATCH |= ( 1 << Channel );
		// CH0 - CH 7 Low Alarm Flag setting
		if( Bias_Temp[Channel] < Low_Alarm_Value )
			LOW_A_LATCH  |= ( 1 << Channel );
		// CH0 - CH 7 High Warning Flag setting
		if( Bias_Temp[Channel] > High_Warning_Value )
			HIGH_W_LATCH |= ( 1 << Channel );
		// CH0 - CH 7 Low Warning Flag setting
		if( Bias_Temp[Channel] < Low_Warning_Value )
			LOW_W_LATCH  |= ( 1 << Channel );
	}

	//-------------------------------------------------------------//
	// High / Low Alarm & Warning Flag write Value
	//-------------------------------------------------------------//
	if ( ( QSFPDD_P11[15] > 0 ) && ( HIGH_A_LATCH == 0 ))
		HIGH_A_LATCH = QSFPDD_P11[15];

	if ( ( QSFPDD_P11[16] > 0 ) && ( LOW_A_LATCH  == 0 ))
		LOW_A_LATCH = QSFPDD_P11[16];

	if ( ( QSFPDD_P11[17] > 0 ) && ( HIGH_W_LATCH == 0 ))
		HIGH_W_LATCH = QSFPDD_P11[17];

	if ( ( QSFPDD_P11[18] > 0 ) && ( LOW_W_LATCH  == 0 ))
		LOW_W_LATCH = QSFPDD_P11[18];

	BIAS_HA_LATCH = HIGH_A_LATCH ;
	BIAS_LA_LATCH = LOW_A_LATCH ;
	BIAS_HW_LATCH = HIGH_W_LATCH ;
	BIAS_LW_LATCH = LOW_W_LATCH ;

	QSFPDD_P11[15] = HIGH_A_LATCH ;
	QSFPDD_P11[16] = LOW_A_LATCH ;
	QSFPDD_P11[17] = HIGH_W_LATCH ;
	QSFPDD_P11[18] = LOW_W_LATCH ;
	//Page11 Byte 0x85 Flag Summary
	//QSFPDD_P11[5] = HIGH_A_LATCH | LOW_A_LATCH | HIGH_W_LATCH | LOW_W_LATCH ;
	if( BIAS_HA_LATCH & (~QSFPDD_P10[94]))
	{
		IntL_G_Low();
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_TX |= TXBHA_INT ;
	}
	if( BIAS_LA_LATCH & (~QSFPDD_P10[95]))
	{
		IntL_G_Low();
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_TX |= TXBLA_INT ;
	}
	if( BIAS_HW_LATCH & (~QSFPDD_P10[96]))
	{
		IntL_G_Low();
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_TX |= TXBHW_INT ;
	}
	if( BIAS_LW_LATCH & (~QSFPDD_P10[97]))
	{
		IntL_G_Low();
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_TX |= TXBLW_INT ;
	}
}

void QSFPDD_RxPower_AW()
{
	uint16_t RXPOWER_Temp[8];
	uint8_t Channel;
	uint16_t High_Alarm_Value, Low_Alarm_Value, High_Warning_Value, Low_Warning_Value ;
	uint8_t HIGH_A_LATCH = 0;
	uint8_t LOW_A_LATCH = 0;
	uint8_t HIGH_W_LATCH = 0;
	uint8_t LOW_W_LATCH = 0;
    
    uint8_t RxP_HA_MASK = ~QSFPDD_P10[100];
    uint8_t RxP_LA_MASK = ~QSFPDD_P10[101];
    uint8_t RxP_HW_MASK = ~QSFPDD_P10[102];
    uint8_t RxP_LW_MASK = ~QSFPDD_P10[103];

	RXPOWER_Temp[0] = ((uint16_t)( QSFPDD_P11[58] << 8 ) | QSFPDD_P11[59] );
	RXPOWER_Temp[1] = ((uint16_t)( QSFPDD_P11[60] << 8 ) | QSFPDD_P11[61] );
	RXPOWER_Temp[2] = ((uint16_t)( QSFPDD_P11[62] << 8 ) | QSFPDD_P11[63] );
	RXPOWER_Temp[3] = ((uint16_t)( QSFPDD_P11[64] << 8 ) | QSFPDD_P11[65] );
	RXPOWER_Temp[4] = ((uint16_t)( QSFPDD_P11[66] << 8 ) | QSFPDD_P11[67] );
	RXPOWER_Temp[5] = ((uint16_t)( QSFPDD_P11[68] << 8 ) | QSFPDD_P11[69] );
	RXPOWER_Temp[6] = ((uint16_t)( QSFPDD_P11[70] << 8 ) | QSFPDD_P11[71] );
	RXPOWER_Temp[7] = ((uint16_t)( QSFPDD_P11[72] << 8 ) | QSFPDD_P11[73] );

	High_Alarm_Value   = ( (uint16_t)( QSFPDD_P2[64] << 8)| QSFPDD_P2[65] );
	Low_Alarm_Value    = ( (uint16_t)( QSFPDD_P2[66] << 8)| QSFPDD_P2[67] );
	High_Warning_Value = ( (uint16_t)( QSFPDD_P2[68] << 8)| QSFPDD_P2[69] );
	Low_Warning_Value  = ( (uint16_t)( QSFPDD_P2[70] << 8)| QSFPDD_P2[71] );
    //Only use four channel
	for( Channel = 0; Channel <= 3; Channel++ )
	{
		// CH0 - CH 7 High Alarm Flag setting
		if( RXPOWER_Temp[Channel] > High_Alarm_Value )
			HIGH_A_LATCH |= ( 1 << Channel );
		// CH0 - CH 7 Low Alarm Flag setting
		if( RXPOWER_Temp[Channel] < Low_Alarm_Value )
			LOW_A_LATCH  |= ( 1 << Channel );
		// CH0 - CH 7 High Warning Flag setting
		if( RXPOWER_Temp[Channel] > High_Warning_Value )
			HIGH_W_LATCH |= ( 1 << Channel );
		// CH0 - CH 7 Low Warning Flag setting
		if( RXPOWER_Temp[Channel] < Low_Warning_Value )
			LOW_W_LATCH  |= ( 1 << Channel );
	}
	//-------------------------------------------------------------//
	// High / Low Alarm & Warning Flag write Value
	//-------------------------------------------------------------//
	if ( ( QSFPDD_P11[21] > 0 ) && ( HIGH_A_LATCH == 0 ))
		HIGH_A_LATCH = QSFPDD_P11[21];
	if ( ( QSFPDD_P11[22] > 0 ) && ( LOW_A_LATCH  == 0 ))
		LOW_A_LATCH = QSFPDD_P11[22];
	if ( ( QSFPDD_P11[23] > 0 ) && ( HIGH_W_LATCH == 0 ))
		HIGH_W_LATCH = QSFPDD_P11[23];
	if ( ( QSFPDD_P11[24] > 0 ) && ( LOW_W_LATCH  == 0 ))
		LOW_W_LATCH = QSFPDD_P11[24];

	RxP_HA_LATCH  = HIGH_A_LATCH ;
	RxP_LA_LATCH  = LOW_A_LATCH ;
	RxP_HW_LATCH  = HIGH_W_LATCH ;
	RxP_LW_LATCH  = LOW_W_LATCH ;

	QSFPDD_P11[21] = HIGH_A_LATCH ;
	QSFPDD_P11[22] = LOW_A_LATCH ;
	QSFPDD_P11[23] = HIGH_W_LATCH ;
	QSFPDD_P11[24] = LOW_W_LATCH ;

	//Page11 Byte 0x85 Flag Summary
	//QSFPDD_P11[5] = HIGH_A_LATCH | LOW_A_LATCH | HIGH_W_LATCH | LOW_W_LATCH ;

	if( RxP_HA_LATCH & RxP_HA_MASK )
	{
		IntL_G_Low();
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= RXHA_INT;
	}
	if( RxP_LA_LATCH & RxP_LA_MASK )
	{
		IntL_G_Low();
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= RXLA_INT;
	}
	if( RxP_HW_LATCH & RxP_HW_MASK )
	{
		IntL_G_Low();
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= RXHW_INT;
	}
	if( RxP_LW_LATCH & RxP_LW_MASK )
	{
		IntL_G_Low();
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= RXLW_INT;
	}
}

void CMIS40_TXLOS_LOL_AW()
{
    uint8_t Tx_LOS_LATCH_R = 0,Tx_LOL_LATCH_R = 0;
    //Get LOL From System Side Input
    Tx_LOL_LATCH_R = (DSP87540_System_Side_LOL()&0x000F);
    
    Tx_LOS_LATCH_R = Tx_LOL_LATCH_R;
    
    //Use for DSP_Tx_Los_Check to get real status to enable or disable tx output
    Tx_LOS_Buffer = Tx_LOL_LATCH_R ;
        
	if( ( QSFPDD_P11[8] > 0 ) && ( Tx_LOS_LATCH_R == 0x00 ) )
		Tx_LOS_LATCH_R = QSFPDD_P11[8];

	if( ( QSFPDD_P11[9] > 0 ) && ( Tx_LOL_LATCH_R == 0x00 ) )
		Tx_LOL_LATCH_R = QSFPDD_P11[9];
    
	QSFPDD_P11[8] |= Tx_LOS_LATCH_R ;
	QSFPDD_P11[9] |= Tx_LOL_LATCH_R ;

	if( Tx_LOS_LATCH_R & (~QSFPDD_P10[87]))
	{
		IntL_G_Low();
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= TXLOS_INT;
	}

	if( Tx_LOL_LATCH_R & (~QSFPDD_P10[88]))
	{
		IntL_G_Low();
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= TXCDR_LOL_INT;
	} 
}

void CMIS40_RXLOS_LOL_AW()
{
	uint8_t Rx_LOL_LATCH_R,Rx_LOS_LATCH_R;
    //Get LOL From Line Side Input
	Rx_LOL_LATCH_R = (DSP87540_Line_Side_LOL()&0x000F);
    //Get LOS From TIA Inout
    Rx_LOS_LATCH_R = GET_TIA_LOS();
    
    //Use for MSA_Rx_Los_Check to get real status
    Rx_LOS_Buffer=Rx_LOS_LATCH_R;
    
    if( ( QSFPDD_P11[19]>0 ) && ( Rx_LOS_LATCH_R==0x00 ))
		Rx_LOS_LATCH_R = QSFPDD_P11[19] ;
    
	if( ( QSFPDD_P11[20]>0 ) && ( Rx_LOL_LATCH_R==0x00 ))
		Rx_LOL_LATCH_R = QSFPDD_P11[20];
    
	QSFPDD_P11[20] |= Rx_LOL_LATCH_R;
    QSFPDD_P11[19] |= Rx_LOS_LATCH_R;

	if( Rx_LOS_LATCH_R & (~QSFPDD_P10[98]))
	{
		IntL_G_Low();
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= RX_ROS_INT;
	}
    
	if( Rx_LOL_LATCH_R & (~QSFPDD_P10[99]))
	{
		IntL_G_Low();
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= RXCDR_LOL_INT;
	}
}

void Clear_Flag(uint8_t Current_AD)
{
	switch(Current_AD)
	{
		case ADDR_DPCF :
						QSFPDD_P11[ADDR_DPCF] = 0 ;
					    break;
		case ADDR_TXFAULT:
						QSFPDD_P11[ADDR_TXFAULT] = 0 ;
						break;
		case ADDR_TXLOS:
						QSFPDD_P11[ADDR_TXLOS] = 0 ;
						IntL_Status_TX &= ~TXLOS_INT ;
						break;
		case ADDR_TXCDR_LOL:
						QSFPDD_P11[ADDR_TXCDR_LOL] = 0 ;
						IntL_Status_TX &= ~TXCDR_LOL_INT ;
						break;
		case ADDR_TX_EQ_FAULT:
						QSFPDD_P11[ADDR_TX_EQ_FAULT] = 0 ;
						break;
		case ADDR_TXP_HA:
						QSFPDD_P11[ADDR_TXP_HA] &= ~TxP_HA_LATCH ;
						IntL_Status_TX &= ~TXPHA_INT ;
						break;
		case ADDR_TXP_LA:
						QSFPDD_P11[ADDR_TXP_LA] = 0x00 ;
						//QSFPDD_P11[ADDR_TXP_LA] &= ~TxP_LA_LATCH ;
						//IntL_Status_TX &= ~TXPLA_INT ;
						break;
		case ADDR_TXP_HW:
						QSFPDD_P11[ADDR_TXP_HW] &= ~TxP_HW_LATCH ;
						IntL_Status_TX &= ~TXPHW_INT ;
						break;
		case ADDR_TXP_LW:
						QSFPDD_P11[ADDR_TXP_LW] = 0x00;
						//QSFPDD_P11[ADDR_TXP_LW] &= ~TxP_LW_LATCH ;
						//IntL_Status_TX &= ~TXPLW_INT ;
						break;
		case ADDR_TXB_HA:
						QSFPDD_P11[ADDR_TXB_HA] &= ~BIAS_HA_LATCH ;
						IntL_Status_TX &= ~TXBHA_INT ;
						break;
		case ADDR_TXB_LA:
						QSFPDD_P11[ADDR_TXB_LA] = 0x00;
						//QSFPDD_P11[ADDR_TXB_LA] &= ~BIAS_LA_LATCH ;
						//IntL_Status_TX &= ~TXBLA_INT ;
						break;
		case ADDR_TXB_HW:
						QSFPDD_P11[ADDR_TXB_HW] &= ~BIAS_HW_LATCH ;
						IntL_Status_TX &= ~TXBHW_INT ;
						break;
		case ADDR_TXB_LW:
						QSFPDD_P11[ADDR_TXB_LW] = 0x00;
						//QSFPDD_P11[ADDR_TXB_LW] &= ~BIAS_LW_LATCH ;
						//IntL_Status_TX &= ~TXBLW_INT ;
						break;
		case ADDR_RXLOS:
						QSFPDD_P11[ADDR_RXLOS] = 0 ;
						IntL_Status_RX &= ~RX_ROS_INT ;
						break;
		case ADDR_RXCDR_LOL:
						QSFPDD_P11[ADDR_RXCDR_LOL] = 0 ;
						IntL_Status_RX &= ~RXCDR_LOL_INT ;
						break;
		case ADDR_RXP_HA:
						QSFPDD_P11[ADDR_RXP_HA] &= ~RxP_HA_LATCH ;
						IntL_Status_RX &= ~RXHA_INT ;
						break;
		case ADDR_RXP_LA:
						QSFPDD_P11[ADDR_RXP_LA] &= ~RxP_LA_LATCH ;
						IntL_Status_RX &= ~RXLA_INT ;
						break;
		case ADDR_RXP_HW:
						QSFPDD_P11[ADDR_RXP_HW] &= ~RxP_HW_LATCH ;
						IntL_Status_RX &= ~RXHW_INT ;
						break;
		case ADDR_RXP_LW:
						QSFPDD_P11[ADDR_RXP_LW] &= ~RxP_LW_LATCH ;
						IntL_Status_RX &= ~RXLW_INT ;
						break;
	}

	if( ( IntL_Status_TX == 0x00 ) && ( IntL_Status_RX == 0x00 ) )
	{
		QSFPDD_A0[3] |= 0x01;
		IntL_G_High();
	}

}

void Clear_VCC_TEMP_Flag()
{
	QSFPDD_A0[9]    &= ~VCC_Temperature_LATCH ;
	IntL_Status_RX  &= ~VCC_TEMP_INT;

	if( ( IntL_Status_TX == 0x00 ) && ( IntL_Status_RX == 0x00 ) )
	{
		QSFPDD_A0[3] |= 0x01;
		IntL_G_High();
	}
}

void Clear_Module_state_Byte8()
{
	QSFPDD_A0[8] = 0 ;
	if( ( IntL_Status_TX == 0x00 ) && ( IntL_Status_RX == 0x00 ) )
	{
		QSFPDD_A0[3] |= 0x01;
		IntL_G_High();
	}
}

void ALL_Clear_Funciton()
{
    if(Clear_flag & 0x01)
    {
        Clear_Module_state_Byte8();
        Clear_flag &= ~0x01;
    }
    
    if(Clear_flag & 0x02)
    {
        Clear_VCC_TEMP_Flag();
        Clear_flag &= ~0x02;
    }
    
    if(Clear_flag & 0x04)
    {
        Clear_Flag(Clear_ADR);
        Clear_flag &= ~0x04;
    }
}

void DDMI_AW_Monitor()
{
	QSFPDD_Temperature_VCC_AW();
	QSFPDD_RxPower_AW();
//	QSFP28_Bias_AW();
//	QSFP28_TxPower_AW();  
 //   ALL_Clear_Funciton();
}



