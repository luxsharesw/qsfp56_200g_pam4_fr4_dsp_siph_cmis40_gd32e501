#include "gd32e501.h"
#include "core_cm33.h"
#include "CMIS_MSA.h"
#include "BRCM_DSP_87540.h"
#include "Calibration_Struct.h"

uint8_t P13_B144_Bef = 0;
uint8_t P13_B152_Bef = 0;
uint8_t P13_B180_Bef = 0;
uint8_t P13_B181_Bef = 0;
uint8_t P13_B182_Bef = 0;
uint8_t P13_B183_Bef = 0;
uint8_t P13_B155_Bef = 0;
uint8_t P13_B147_Bef = 0;

uint8_t Config_Error_flag = 0x00 ;
uint8_t	System_Side_Tab_Load = 0x00;

uint8_t Get_SystemSide_PRBS_Pattern(uint8_t Data)
{
    uint8_t PRBS_Pattern = 0x00;
    
    if(Signal_Status==PAM4_to_PAM4)
    {
        if(Data==0x00)
            PRBS_Pattern=PRBS31Q_SystemSide;
        else if(Data==0x02)
            PRBS_Pattern=PRBS23Q_SystemSide;
        else if(Data==0x04)
            PRBS_Pattern=PRBS15Q_SystemSide;
        else if(Data==0x06)
            PRBS_Pattern=PRBS13Q_SystemSide;
        else if(Data==0x08)
            PRBS_Pattern=PRBS9Q_SystemSide;
        else if(Data==0x0A)
            PRBS_Pattern=PRBS7Q_SystemSide;
        else if(Data==0x0C)
            PRBS_Pattern=SSPRQ_SystemSide;
    }
    else
    {
        if(Data==0x01)
            PRBS_Pattern=PRBS31Q_SystemSide;
        else if(Data==0x03)
            PRBS_Pattern=PRBS23Q_SystemSide;
        else if(Data==0x05)
            PRBS_Pattern=PRBS15Q_SystemSide;
        else if(Data==0x07)
            PRBS_Pattern=PRBS13Q_SystemSide;
        else if(Data==0x09)
            PRBS_Pattern=PRBS9Q_SystemSide;
        else if(Data==0x0B)
            PRBS_Pattern=PRBS7Q_SystemSide;
        else if(Data==0x0C)
            PRBS_Pattern=SSPRQ_SystemSide;
    }
    
    return PRBS_Pattern;
}

uint8_t Get_LineSide_PRBS_Pattern(uint8_t Data)
{
    uint8_t PRBS_Pattern = 0x00;
    
    if(Signal_Status==PAM4_to_PAM4)
    {
        if(Data==0x00)
            PRBS_Pattern=PRBS31Q_LineSide;
        else if(Data==0x02)
            PRBS_Pattern=PRBS23Q_LineSide;
        else if(Data==0x04)
            PRBS_Pattern=PRBS15Q_LineSide;
        else if(Data==0x06)
            PRBS_Pattern=PRBS13Q_LineSide;
        else if(Data==0x08)
            PRBS_Pattern=PRBS9Q_LineSide;
        else if(Data==0x0A)
            PRBS_Pattern=PRBS7Q_LineSide;
        else if(Data==0x0C)
            PRBS_Pattern=SSPRQ_LineSide;
    }
    else
    {
        if(Data==0x01)
            PRBS_Pattern=PRBS31Q_LineSide;
        else if(Data==0x03)
            PRBS_Pattern=PRBS23Q_LineSide;
        else if(Data==0x05)
            PRBS_Pattern=PRBS15Q_LineSide;
        else if(Data==0x07)
            PRBS_Pattern=PRBS13Q_LineSide;
        else if(Data==0x09)
            PRBS_Pattern=PRBS9Q_LineSide;
        else if(Data==0x0B)
            PRBS_Pattern=PRBS7Q_LineSide;
        else if(Data==0x0C)
            PRBS_Pattern=SSPRQ_LineSide;
    }
    
    return PRBS_Pattern;
}
//For Get Rx Output Max Value By Selected Mode
uint8_t Get_Rx_Output_Max_Value (uint8_t PAM4_Max ,uint8_t NRZ_Max)
{
	uint8_t Max_Value = 0x00;
	//NRZ Mode
	if (Signal_Status!=PAM4_to_PAM4)
		Max_Value = NRZ_Max;
	//PAM4 Mode
	else
		Max_Value = PAM4_Max;
	return Max_Value;
}

void Configuration_Error_code_Feedback(uint8_t Lane_CH,uint8_t Write_Error_code)
{
	uint8_t Data_Buffer = 0;
	uint8_t Current_Address = 0 ;
    // Page11 202 QSFPDD_P11[74] Lan0/Lan1
	// Page11 203 QSFPDD_P11[75] Lan2/Lan3
	// Page11 204 QSFPDD_P11[76] Lan4/Lan5
	// Page11 205 QSFPDD_P11[77] Lan6/Lan7
	// Determine odd and even
	if (Lane_CH==4)
	{
		QSFPDD_P11[74] = ((Write_Error_code<<4)|Write_Error_code);
		QSFPDD_P11[75] = ((Write_Error_code<<4)|Write_Error_code);
		//QSFPDD_P11[76] = ((Write_Error_code<<4)|Write_Error_code);
		//QSFPDD_P11[77] = ((Write_Error_code<<4)|Write_Error_code);
	}
	else
	{
		if( Lane_CH%2 == 0 )
		{
			Current_Address = ( 74 + Lane_CH/2 ) ;
			Data_Buffer = QSFPDD_P11[Current_Address] & 0xF0 ;
			Data_Buffer = (Data_Buffer | Write_Error_code);
			QSFPDD_P11[Current_Address] = Data_Buffer ;
		}
		else
		{
			Current_Address = ( 74 + Lane_CH/2 ) ;
			Data_Buffer = QSFPDD_P11[Current_Address] & 0x0F ;
			Data_Buffer = ( Data_Buffer | ( Write_Error_code << 4 ));
			QSFPDD_P11[Current_Address] = Data_Buffer ;
		}
	}
}

void HOST_Side_Generator_Pattern_Control_P13()
{
    //Host Side Generator Enable
    if(P13_B144_Bef!=QSFPDD_P13[16])
    {
        if(QSFPDD_P13[16] & 0x01)
            DSP87540_SystemSide_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[20],0)), 0 , 1);
        else
            DSP87540_SystemSide_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[20],0)), 0 , 0);
        
        if(QSFPDD_P13[16] & 0x02)
            DSP87540_SystemSide_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[20],1)), 1 , 1);
        else
            DSP87540_SystemSide_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[20],1)), 1 , 0);
        
        if(QSFPDD_P13[16] & 0x04)
            DSP87540_SystemSide_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[21],2)), 2 , 1);
        else
            DSP87540_SystemSide_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[21],2)), 2 , 0);
        
        if(QSFPDD_P13[16] & 0x08)
            DSP87540_SystemSide_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[21],3)), 3 , 1);
        else
            DSP87540_SystemSide_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[21],3)), 3 , 0);
    }

    P13_B144_Bef = QSFPDD_P13[16];
}
void Media_Side_Generator_Pattern_Control_P13()
{
    //Media Side Generator Enable
    if(P13_B152_Bef!=QSFPDD_P13[24])
    {
        if(QSFPDD_P13[24] & 0x01)
            DSP87540_LineSide_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[28],0)), 0 , 1);
        else
            DSP87540_LineSide_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[28],0)), 0 , 0);
        
        if(QSFPDD_P13[24] & 0x02)
            DSP87540_LineSide_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[28],1)), 1 , 1);
        else
            DSP87540_LineSide_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[28],1)), 1 , 0);
        
        if(QSFPDD_P13[24] & 0x04)
            DSP87540_LineSide_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[29],2)), 2 , 1);
        else
            DSP87540_LineSide_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[29],2)), 2 , 0);
        
        if(QSFPDD_P13[24] & 0x08)
            DSP87540_LineSide_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[29],3)), 3 , 1);
        else
            DSP87540_LineSide_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[29],3)), 3 , 0);
    }
    P13_B152_Bef = QSFPDD_P13[24];
}
void Media_Side_LoopBack_P13()
{
	// Page13h 180 / 52 Media output ( line side diag. loopback )
	// Page13h 181 / 53 Media input  ( Line side rem.  loopback )
	//---------------------------------------------------------------------------//
	// Media output  ( line side diag. loopback )
	//---------------------------------------------------------------------------//
	if(P13_B180_Bef!=QSFPDD_P13[52])
	{
		if(QSFPDD_P13[52] & 0x01)
			DSP87540_LineSide_Loopback_SET( Digital_LoopBack_mode , 0 , 1 );
		else
			DSP87540_LineSide_Loopback_SET( Digital_LoopBack_mode , 0 , 0 );

		if(QSFPDD_P13[52] & 0x02)
			DSP87540_LineSide_Loopback_SET( Digital_LoopBack_mode , 1 , 1 );
		else
			DSP87540_LineSide_Loopback_SET( Digital_LoopBack_mode , 1 , 0 );

		if(QSFPDD_P13[52] & 0x04)
			DSP87540_LineSide_Loopback_SET( Digital_LoopBack_mode , 2 , 1 );
		else
			DSP87540_LineSide_Loopback_SET( Digital_LoopBack_mode , 2 , 0 );

		if(QSFPDD_P13[52] & 0x08)
			DSP87540_LineSide_Loopback_SET( Digital_LoopBack_mode , 3 , 1 );
		else
			DSP87540_LineSide_Loopback_SET( Digital_LoopBack_mode , 3 , 0 );
	}

	//---------------------------------------------------------------------------//
	// Media input  ( Line side rem.  loopback )
	//---------------------------------------------------------------------------//
	if(P13_B181_Bef!=QSFPDD_P13[53])
	{
		if(QSFPDD_P13[53] & 0x01)
			DSP87540_LineSide_Loopback_SET( Remote_Loopback_mode , 0 , 1 );
		else
			DSP87540_LineSide_Loopback_SET( Remote_Loopback_mode , 0 , 0 );

		if(QSFPDD_P13[53] & 0x02)
			DSP87540_LineSide_Loopback_SET( Remote_Loopback_mode , 1 , 1 );
		else
			DSP87540_LineSide_Loopback_SET( Remote_Loopback_mode , 1 , 0 );

		if(QSFPDD_P13[53] & 0x04)
			DSP87540_LineSide_Loopback_SET( Remote_Loopback_mode , 2 , 1 );
		else
			DSP87540_LineSide_Loopback_SET( Remote_Loopback_mode , 2 , 0 );

		if(QSFPDD_P13[53] & 0x08)
			DSP87540_LineSide_Loopback_SET( Remote_Loopback_mode , 3 , 1 );
		else
			DSP87540_LineSide_Loopback_SET( Remote_Loopback_mode , 3 , 0 );
	}
	P13_B180_Bef = QSFPDD_P13[52];
	P13_B181_Bef = QSFPDD_P13[53];
}

void Host_Side_LoopBack_P13()
{
	// Page13h 182 / 54 Host  output ( Sys  side diag. loopback )
	// Page13h 183 / 55 Host  Input  ( Sys  Side Rem.  loopback )
	//---------------------------------------------------------------------------//
	// Host  output ( Sys  side diag. loopback )
	//---------------------------------------------------------------------------//
	if(P13_B182_Bef!=QSFPDD_P13[54])
	{
		if(QSFPDD_P13[54] & 0x01)
			DSP87540_SystemSide_Loopback_SET( Digital_LoopBack_mode , 0 , 1 );
		else
			DSP87540_SystemSide_Loopback_SET( Digital_LoopBack_mode , 0 , 0 );

		if(QSFPDD_P13[54] & 0x02)
			DSP87540_SystemSide_Loopback_SET( Digital_LoopBack_mode , 1 , 1 );
		else
			DSP87540_SystemSide_Loopback_SET( Digital_LoopBack_mode , 1 , 0 );

		if(QSFPDD_P13[54] & 0x04)
			DSP87540_SystemSide_Loopback_SET( Digital_LoopBack_mode , 2 , 1 );
		else
			DSP87540_SystemSide_Loopback_SET( Digital_LoopBack_mode , 2 , 0 );

		if(QSFPDD_P13[54] & 0x08)
			DSP87540_SystemSide_Loopback_SET( Digital_LoopBack_mode , 3 , 1 );
		else
			DSP87540_SystemSide_Loopback_SET( Digital_LoopBack_mode , 3 , 0 );
	}

	//---------------------------------------------------------------------------//
	// Host  Input ( Sys  Side Rem.  loopback )
	//---------------------------------------------------------------------------//
	if(P13_B183_Bef!=QSFPDD_P13[55])
	{
		if(QSFPDD_P13[55] & 0x01)
			DSP87540_SystemSide_Loopback_SET( Remote_Loopback_mode , 0 , 1 );
		else
			DSP87540_SystemSide_Loopback_SET( Remote_Loopback_mode , 0 , 0 );

		if(QSFPDD_P13[55] & 0x02)
			DSP87540_SystemSide_Loopback_SET( Remote_Loopback_mode , 1 , 1 );
		else
			DSP87540_SystemSide_Loopback_SET( Remote_Loopback_mode , 1 , 0 );

		if(QSFPDD_P13[55] & 0x04)
			DSP87540_SystemSide_Loopback_SET( Remote_Loopback_mode , 2 , 1 );
		else
			DSP87540_SystemSide_Loopback_SET( Remote_Loopback_mode , 2 , 0 );

		if(QSFPDD_P13[55] & 0x08)
			DSP87540_SystemSide_Loopback_SET( Remote_Loopback_mode , 3 , 1 );
		else
			DSP87540_SystemSide_Loopback_SET( Remote_Loopback_mode , 3 , 0 );
	}

	P13_B182_Bef = QSFPDD_P13[54];
	P13_B183_Bef = QSFPDD_P13[55];
}

void Rx_Output_Limit_Check()
{
	uint8_t Pre_Cursor_RX1,Pre_Cursor_RX2,Pre_Cursor_RX3,Pre_Cursor_RX4;
	uint8_t Post_Cursor_RX1,Post_Cursor_RX2,Post_Cursor_RX3,Post_Cursor_RX4;
	uint8_t AMP_C_RX1,AMP_C_RX2,AMP_C_RX3,AMP_C_RX4 ;
	uint8_t Max_Value = 0x00;

	Config_Error_flag=0x00;

    Max_Value = Get_Rx_Output_Max_Value(PAM4_Pre_Cursor_Max_Vaule , NRZ_Pre_Cursor_Max_Vaule);

	//Pre Value Range Check
	Pre_Cursor_RX1 = Get_CH_Data(QSFPDD_P10[34],0);
	Pre_Cursor_RX2 = Get_CH_Data(QSFPDD_P10[34],1);
	Pre_Cursor_RX3 = Get_CH_Data(QSFPDD_P10[35],2);
	Pre_Cursor_RX4 = Get_CH_Data(QSFPDD_P10[35],3);

	if(Pre_Cursor_RX1>Max_Value)
		Config_Error_flag |= 0x01;
	if(Pre_Cursor_RX2>Max_Value)
		Config_Error_flag |= 0x02;
	if(Pre_Cursor_RX3>Max_Value)
		Config_Error_flag |= 0x04;
	if(Pre_Cursor_RX4>Max_Value)
		Config_Error_flag |= 0x08;

	//Post Value Range Check
	Post_Cursor_RX1 = Get_CH_Data(QSFPDD_P10[38],0);
	Post_Cursor_RX2 = Get_CH_Data(QSFPDD_P10[38],1);
	Post_Cursor_RX3 = Get_CH_Data(QSFPDD_P10[39],2);
	Post_Cursor_RX4 = Get_CH_Data(QSFPDD_P10[39],3);

    Max_Value = Get_Rx_Output_Max_Value(PAM4_Post_Cursor_Max_Vaule , NRZ_Post_Cursor_Max_Vaule);

	if(Post_Cursor_RX1>Max_Value)
		Config_Error_flag |= 0x01;
	if(Post_Cursor_RX2>Max_Value)
		Config_Error_flag |= 0x02;
	if(Post_Cursor_RX3>Max_Value)
		Config_Error_flag |= 0x04;
	if(Post_Cursor_RX4>Max_Value)
		Config_Error_flag |= 0x08;

	//Amp Value Range Check
	AMP_C_RX1 = Get_CH_Data(QSFPDD_P10[42],0);
	AMP_C_RX2 = Get_CH_Data(QSFPDD_P10[42],1);
	AMP_C_RX3 = Get_CH_Data(QSFPDD_P10[43],2);
	AMP_C_RX4 = Get_CH_Data(QSFPDD_P10[43],3);

    Max_Value = Get_Rx_Output_Max_Value(PAM4_AMP_Max_Vaule , NRZ_AMP_Max_Vaule );

	if(AMP_C_RX1>Max_Value)
		Config_Error_flag |= 0x01;
	if(AMP_C_RX2>Max_Value)
		Config_Error_flag |= 0x02;
	if(AMP_C_RX3>Max_Value)
		Config_Error_flag |= 0x04;
	if(AMP_C_RX4>Max_Value)
		Config_Error_flag |= 0x08;

	//Config Error Code Feedback when value out of range
	//CH1-4
	if(Config_Error_flag&0x01)
		Configuration_Error_code_Feedback( 0 , Rejected_Invalid_SI );
	if(Config_Error_flag&0x02)
		Configuration_Error_code_Feedback( 1 , Rejected_Invalid_SI );
	if(Config_Error_flag&0x04)
		Configuration_Error_code_Feedback( 2 , Rejected_Invalid_SI );
	if(Config_Error_flag&0x08)
		Configuration_Error_code_Feedback( 3 , Rejected_Invalid_SI );
}


//------------------------------------------------------------------------------------------//
// MSA Page10h Rx Pre Control                                                             //
//----------------------------------------------------------------------------------------//
void RX_Pre_Control( )
{
	uint8_t Pre_Cursor_RX1,Pre_Cursor_RX2,Pre_Cursor_RX3,Pre_Cursor_RX4;

	// MSA PAGE10 Byte 162 RX12 Pre-cursor CONTROL
	// MSA PAGE10 Byte 163 RX34 Pre-cursor CONTROL

	// Device 3 level Control 09 / 0A / 0B
	//Get Pre Channels Data//
	Pre_Cursor_RX1 = Get_CH_Data(QSFPDD_P10[34],0);
	Pre_Cursor_RX2 = Get_CH_Data(QSFPDD_P10[34],1);
	Pre_Cursor_RX3 = Get_CH_Data(QSFPDD_P10[35],2);
	Pre_Cursor_RX4 = Get_CH_Data(QSFPDD_P10[35],3);

	//-----------------------------------------------------------------------------------------//
	// MSA Pre-Cursor RX1 - RX4
	//-----------------------------------------------------------------------------------------//
	// RX1 Pre_Cursor
	if((Config_Error_flag&0x01)==0)
	{
		if(Pre_Cursor_RX1==0)
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH0 = 0x0000;
		}
		else
		{
            BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH0 = (((uint16_t)Pre_Table[Pre_Cursor_RX1]<<8 | 0x00FF));
		}
		Configuration_Error_code_Feedback(0,Config_In_Process);
		QSFPDD_P11[95]=((QSFPDD_P11[95]&= ~0x0F)|Pre_Cursor_RX1);
		System_Side_Tab_Load |= 0x01;
	}

	// RX2 Pre_Cursor
	if((Config_Error_flag&0x02)==0)
	{
		if(Pre_Cursor_RX2==0)
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH1 = 0x0000;
		}
		else
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH1 = ( (uint16_t)Pre_Table[Pre_Cursor_RX2]<<8 | 0x00FF );
		}
		Configuration_Error_code_Feedback(1,Config_In_Process);
		QSFPDD_P11[95]=((QSFPDD_P11[95]&= ~0xF0)|(Pre_Cursor_RX2<<4));
		System_Side_Tab_Load |= 0x02;
	}

	// RX3 Pre_Cursor
	if((Config_Error_flag&0x04)==0)
	{
		if(Pre_Cursor_RX3==0)
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH2 = 0x0000;
		}
		else
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH2 = ( (uint16_t)Pre_Table[Pre_Cursor_RX3]<<8 | 0x00FF );
		}
		Configuration_Error_code_Feedback(2,Config_In_Process);
		QSFPDD_P11[96]=((QSFPDD_P11[96]&= ~0x0F)|Pre_Cursor_RX3);
    	System_Side_Tab_Load |= 0x04;
	}

	// RX4 Pre_Cursor
	if((Config_Error_flag&0x08)==0)
	{
		if(Pre_Cursor_RX4==0)
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH3 = 0x0000;
		}
		else
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH3 = ( (uint16_t)Pre_Table[Pre_Cursor_RX4]<<8 | 0x00FF );
		}
		Configuration_Error_code_Feedback(3,Config_In_Process);
		QSFPDD_P11[96]=((QSFPDD_P11[96]&= ~0xF0)|(Pre_Cursor_RX4<<4));
		System_Side_Tab_Load |= 0x08;
	}

}

//------------------------------------------------------------------------------------------//
// MSA Page10h Rx Post Control                                                             //
//----------------------------------------------------------------------------------------//

void RX_Post_Control( )
{
	uint8_t Post_Cursor_RX1,Post_Cursor_RX2,Post_Cursor_RX3,Post_Cursor_RX4;

	// MSA PAGE10 Byte 166 RX12 Post-cursor CONTROL
	// MSA PAGE10 Byte 167 RX34 Post-cursor CONTROL
	// MSA PAGE10 Byte 168 RX56 Post-cursor CONTROL
	// MSA PAGE10 Byte 169 RX78 Post-cursor CONTROL

	// Device 3 level Control 0C / 0D / 0E
	//Get Rx Post Channel Data
	Post_Cursor_RX1 = Get_CH_Data(QSFPDD_P10[38],0);
	Post_Cursor_RX2 = Get_CH_Data(QSFPDD_P10[38],1);
	Post_Cursor_RX3 = Get_CH_Data(QSFPDD_P10[39],2);
	Post_Cursor_RX4 = Get_CH_Data(QSFPDD_P10[39],3);

	//-----------------------------------------------------------------------------------------//
	// MSA Post-Cursor RX1 - RX4
	//-----------------------------------------------------------------------------------------//
	// RX1 Post_Cursor
	if((Config_Error_flag&0x01)==0)
	{
		if(Post_Cursor_RX1 == 0)
        {
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH0 = 0x0000;
        }
		else
        {
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH0 = ( (uint16_t)Post_Table[Post_Cursor_RX1]<<8 | 0x00FF );
        }
		
		Configuration_Error_code_Feedback(0,Config_In_Process);
		QSFPDD_P11[99]=((QSFPDD_P11[99]&= ~0x0F)|Post_Cursor_RX1);
		System_Side_Tab_Load |= 0x01;
	}

	// RX2 Post_Cursor
	if((Config_Error_flag&0x02)==0)
	{
		if(Post_Cursor_RX2 == 0)
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH1 = 0x0000;
		}
		else
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH1 = ( (uint16_t)Post_Table[Post_Cursor_RX2]<<8 | 0x00FF );
		}
		Configuration_Error_code_Feedback(1,Config_In_Process);
		QSFPDD_P11[99]=((QSFPDD_P11[99]&= ~0xF0)|(Post_Cursor_RX2<<4));
		System_Side_Tab_Load |= 0x02;
	}

	// RX3 Post_Cursor
	if((Config_Error_flag&0x04)==0)
	{
		if(Post_Cursor_RX3 == 0)
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH2 = 0x0000;
		}
		else
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH2 = ( (uint16_t)Post_Table[Post_Cursor_RX3]<<8 | 0x00FF );
		}
		Configuration_Error_code_Feedback(2,Config_In_Process);
		QSFPDD_P11[100]=((QSFPDD_P11[100]&= ~0x0F)|Post_Cursor_RX3);
		System_Side_Tab_Load |= 0x04;
	}

	// RX4 Post_Cursor
	if((Config_Error_flag&0x08)==0)
	{
		if(Post_Cursor_RX4 == 0)
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH3 = 0x0000;
		}
		else
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH3 = ( (uint16_t)Post_Table[Post_Cursor_RX4]<<8 | 0x00FF );
		}
		Configuration_Error_code_Feedback(3,Config_In_Process);
		QSFPDD_P11[100]=((QSFPDD_P11[100]&= ~0xF0)|(Post_Cursor_RX4<<4));
		System_Side_Tab_Load |= 0x08;
	}
}

//------------------------------------------------------------------------------------------//
// MSA Page10h Rx Output AMP Control                                                       //
//----------------------------------------------------------------------------------------//

void RX_AMP_OutSwing_Control( )
{
	uint8_t AMP_C_RX1,AMP_C_RX2,AMP_C_RX3,AMP_C_RX4 ;

	// Device 3 level Control 00 / 01 / 02
	// MSA PAGE10 Byte 170 RX12 AMP CONTROL
	// MSA PAGE10 Byte 171 RX34 AMP CONTROL
	// MSA PAGE10 Byte 172 RX56 AMP CONTROL
	// MSA PAGE10 Byte 173 RX78 AMP CONTROL
	//Get Rx Out AMP Channel Data
	AMP_C_RX1 = Get_CH_Data(QSFPDD_P10[42],0);
	AMP_C_RX2 = Get_CH_Data(QSFPDD_P10[42],1);
	AMP_C_RX3 = Get_CH_Data(QSFPDD_P10[43],2);
	AMP_C_RX4 = Get_CH_Data(QSFPDD_P10[43],3);

	//------------------------------------------------------------------------------------------//
	// RX1-RX4 Control                                                                         //
	//----------------------------------------------------------------------------------------//
	// RX1 Output AMP setting//
	if((Config_Error_flag&0x01)==0)
	{
		//For NRZ Mode Selected Only, Max index is 2 but max main value is 0x70 (112)
		if((Signal_Status!=PAM4_to_PAM4)&&(AMP_C_RX1==2))
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH0 = 0x7000;
		}
		else
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH0 = ( (uint16_t)OP_AMP_Table[AMP_C_RX1]<<8 );
		}
		Configuration_Error_code_Feedback(0,Config_In_Process);
		QSFPDD_P11[103]=((QSFPDD_P11[103]&= ~0x0F)|AMP_C_RX1);
		System_Side_Tab_Load |= 0x01;
	}

	// RX2 Output Swing setting//
	if((Config_Error_flag&0x02)==0)
	{
		//For NRZ Mode Selected Only, Max index is 2 but max main value is 0x70 (112)
		if((Signal_Status!=PAM4_to_PAM4)&&(AMP_C_RX2==2))
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH1 = 0x7000;
		}
		else
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH1 = ( (uint16_t)OP_AMP_Table[AMP_C_RX2]<<8 );
		}
		Configuration_Error_code_Feedback(1,Config_In_Process);
		QSFPDD_P11[103]=((QSFPDD_P11[103]&= ~0xF0)|(AMP_C_RX2<<4));
		System_Side_Tab_Load |= 0x02;
	}

	// RX3 Output Swing setting
	if((Config_Error_flag&0x04)==0)
	{
		//For NRZ Mode Selected Only, Max index is 2 but max main value is 0x70 (112)
		if((Signal_Status!=PAM4_to_PAM4)&&(AMP_C_RX3==2))
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH2 = 0x7000;
		}
		else
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH2 = ( (uint16_t)OP_AMP_Table[AMP_C_RX3]<<8 );
		}
		Configuration_Error_code_Feedback(2,Config_In_Process);
		QSFPDD_P11[104]=((QSFPDD_P11[104]&= ~0x0F)|AMP_C_RX3);
		System_Side_Tab_Load |= 0x04;
	}

	// RX4 Output Swing setting
	if((Config_Error_flag&0x08)==0)
	{
		//For NRZ Mode Selected Only, Max index is 2 but max main value is 0x70 (112)
		if((Signal_Status!=PAM4_to_PAM4)&&(AMP_C_RX4==2))
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH3 = 0x7000;
		}
		else
		{
			BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH3 = ( (uint16_t)OP_AMP_Table[AMP_C_RX4]<<8 );
		}
		Configuration_Error_code_Feedback(3,Config_In_Process);
		QSFPDD_P11[104]=((QSFPDD_P11[104]&= ~0xF0)|(AMP_C_RX4<<4));
		System_Side_Tab_Load |= 0x08;
	}
}

//------------------------------------------------------------------------------------------//
// DSP System Side Tab Load Control                                                        //
//----------------------------------------------------------------------------------------//
void QDD_DSP_Tap_ReLoader()
{
	//RX1
	if(System_Side_Tab_Load&0x01)
	{
		DSP87540_SystemSide_TX_FIR_SET(0);
		System_Side_Tab_Load &= ~0x01 ;
		Configuration_Error_code_Feedback( 0 , Accepted );
	}
	//RX2
	if(System_Side_Tab_Load&0x02)
	{
		DSP87540_SystemSide_TX_FIR_SET(1);
		System_Side_Tab_Load &= ~0x02 ;
		Configuration_Error_code_Feedback( 1 , Accepted );
	}
	//RX3
	if(System_Side_Tab_Load&0x04)
	{
		DSP87540_SystemSide_TX_FIR_SET(2);
		System_Side_Tab_Load &= ~0x04 ;
		Configuration_Error_code_Feedback( 2 , Accepted );
	}
	//RX4
	if(System_Side_Tab_Load&0x08)
	{
		DSP87540_SystemSide_TX_FIR_SET(3);
		System_Side_Tab_Load &= ~0x08 ;
		Configuration_Error_code_Feedback( 3 , Accepted );
	}

}

void QDD_MSA_CTLE_PRE_POST_AMP_CONTROL()
{
	if(QSFPDD_P10[16] & 0x0F)
	{
        Rx_Output_Limit_Check();
        RX_Pre_Control();
        RX_Post_Control();
        RX_AMP_OutSwing_Control();
        QDD_DSP_Tap_ReLoader();
		QSFPDD_P10[16] = 0x00;
	}
}

void P13_diagnostic_features()
{
    HOST_Side_Generator_Pattern_Control_P13();
    Media_Side_Generator_Pattern_Control_P13();
	Media_Side_LoopBack_P13();
	Host_Side_LoopBack_P13();
}
