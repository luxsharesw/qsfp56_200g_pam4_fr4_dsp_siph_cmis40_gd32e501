
#include "gd32e501.h"
#include "core_cm33.h"
#include "CMIS_MSA.h"
#include "stdint.h"
#include "Macom_TIA_03819.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "GD32E501_GPIO_Customize_Define.h"

#define ADC_SM            0
#define RXP_CH0123        1
#define TXB_TXP_CH0_SM    2
#define TXB_TXP_CH1_SM    3
#define TXB_TXP_CH2_SM    4
#define TXB_TXP_CH3_SM    5
#define OP_SM             6
#define TXLOLLOS          7
#define RXLOLLOS          8
#define Other_DDMI        9

#define SHOW_RSSI_Enable   1
#define SHOW_RSSI_Disable  0

uint8_t SHOW_RSSI_CH0 = 1 ;
uint8_t SHOW_RSSI_CH1 = 1 ;
uint8_t SHOW_RSSI_CH2 = 1 ;
uint8_t SHOW_RSSI_CH3 = 1 ;
int8_t Temperature_Indx = 0 ;
uint8_t Bef_ForceMute_CH0_CH3 = 0 ;
uint16_t Bias_Current_CH0,Bias_Current_CH1,Bias_Current_CH2,Bias_Current_CH3;

extern uint8_t PowerON_AWEN_flag;

int16_t Temp_CAL( int16_t Vaule , uint8_t Slop , uint8_t Shift , int16_t Offset)
{
	uint32_t Buffer_temp ;

	Buffer_temp = (uint32_t)Vaule * Slop ;
	Buffer_temp = (uint32_t)Buffer_temp >> Shift ;

	Vaule = (uint32_t)Buffer_temp + Offset * 256 ;

    return Vaule ;
}

uint16_t CAL_Function( uint16_t Vaule , uint8_t Slop , uint8_t Shift , int16_t Offset )
{
	uint32_t Buffer_temp ;

	Buffer_temp = (uint32_t)Vaule * Slop ;
	Buffer_temp = (uint32_t)Buffer_temp >> Shift ;

	Vaule = (uint32_t)Buffer_temp + Offset;

	return Vaule;
}

void Temperature_Monitor(int16_t tempsensor_value)
{
	int16_t Temp_Buffer;
	int8_t  Indx_Temp ;

	Temp_Buffer = tempsensor_value ;
	Temp_Buffer = Temp_CAL(tempsensor_value,CALIB_MEMORY_MAP.TEMP_SCALE1M,CALIB_MEMORY_MAP.TEMP_SCALE1L,CALIB_MEMORY_MAP.TEMP_OFFSET1);

	QSFPDD_A0[14] = Temp_Buffer >> 8 ;
	QSFPDD_A0[15] = Temp_Buffer ;

	Indx_Temp = QSFPDD_A0[14];
    
    if(Indx_Temp<-40)
        Indx_Temp = -40 ;

    if(Indx_Temp>85)
        Indx_Temp = 85 ;
    
    Indx_Temp = ( Indx_Temp + 40 );

	Temperature_Indx = (Indx_Temp>>1) ;
    
    //CALIB_MEMORY_MAP.Temp_Index = Temperature_Indx;
    
	if(Indx_Temp > 64) 
        Indx_Temp = 64;
}

void VCC_Monitor( uint16_t VCC_Vaule)
{
    int16_t  offset_Buffer_VCC = 0 ;
    offset_Buffer_VCC = (CALIB_MEMORY_MAP.VCC_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.VCC_OFFSET_LSB );
    
	VCC_Vaule = CAL_Function( VCC_Vaule , CALIB_MEMORY_MAP.VCC_SCALEM , CALIB_MEMORY_MAP.VCC_SCALEL , offset_Buffer_VCC ) ;

	QSFPDD_A0[16] = VCC_Vaule >> 8 ;
	QSFPDD_A0[17] = VCC_Vaule ;
}

void BIAS_TxPower_Monitor( uint8_t Channel)
{
	uint16_t MOD_Temp_Buffer = 0;
	uint16_t TXP_BUFFER = 0 ;  
    int16_t  offset_Buffer_B = 0 ;
    int16_t  offset_Buffer_P = 0 ;

	switch(Channel)
	{
		case 0:
				//Bias_Current_CH0 = TXBIAS_TX1_TX4(0x00) ;
                if(Bias_Current_CH0>0x8000)
					Bias_Current_CH0 = (uint16_t) (QSFPDD_P11[42] << 8) | QSFPDD_P11[43];
                
				TXP_BUFFER = Bias_Current_CH0 ;
        
                offset_Buffer_B = (CALIB_MEMORY_MAP.IBias0_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.IBias0_OFFSET_LSB );
                offset_Buffer_P = (CALIB_MEMORY_MAP.TXP0_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.TXP0_OFFSET_LSB );
        
				Bias_Current_CH0 = CAL_Function( Bias_Current_CH0 , CALIB_MEMORY_MAP.IBias0_SCALEM , CALIB_MEMORY_MAP.IBias0_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP0_SCALEM , CALIB_MEMORY_MAP.TXP0_SCALEL , offset_Buffer_P ) ;

				if((QSFPDD_P10[2] & 0x01) || (Tx_Disable_Flag & 0x01))
				{
					QSFPDD_P11[42] = 0 ;
					QSFPDD_P11[43] = 0 ;
					// CH0  Tx Power
					QSFPDD_P11[26] = 0 ;
					QSFPDD_P11[27] = 0 ;

					// Alarm & Waring trigger
					QSFPDD_P11[12] |= 0x01 ;
					QSFPDD_P11[14] |= 0x01 ;
					QSFPDD_P11[16] |= 0x01 ;
					QSFPDD_P11[18] |= 0x01 ;
				}
				else
				{
					QSFPDD_P11[42] = Bias_Current_CH0 >> 8 ;
					QSFPDD_P11[43] = Bias_Current_CH0 ;
					// CH0 TxPower
					QSFPDD_P11[26] = TXP_BUFFER >> 8 ;
					QSFPDD_P11[27] = TXP_BUFFER ;
				}
				break;
		case 1:
				//Bias_Current_CH1 = TXBIAS_TX1_TX4(0x20) ;
        		if(Bias_Current_CH1>0x8000)
					Bias_Current_CH1 = (uint16_t) (QSFPDD_P11[44] << 8) | QSFPDD_P11[45];
                        
				TXP_BUFFER = Bias_Current_CH1 ;
        
                offset_Buffer_B = (CALIB_MEMORY_MAP.IBias1_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.IBias1_OFFSET_LSB );
                offset_Buffer_P = (CALIB_MEMORY_MAP.TXP1_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.TXP1_OFFSET_LSB );

				Bias_Current_CH1 = CAL_Function( Bias_Current_CH1 , CALIB_MEMORY_MAP.IBias1_SCALEM , CALIB_MEMORY_MAP.IBias1_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP1_SCALEM , CALIB_MEMORY_MAP.TXP1_SCALEL , offset_Buffer_P ) ;

				if((QSFPDD_P10[2] & 0x02) || (Tx_Disable_Flag & 0x02))
				{
					QSFPDD_P11[44] = 0 ;
					QSFPDD_P11[45] = 0 ;
					// CH1  Tx Power
					QSFPDD_P11[28] = 0 ;
					QSFPDD_P11[29] = 0 ;

					// Alarm & Waring trigger
					QSFPDD_P11[12] |= 0x02 ;
					QSFPDD_P11[14] |= 0x02 ;
					QSFPDD_P11[16] |= 0x02 ;
					QSFPDD_P11[18] |= 0x02 ;
				}
				else
				{
					QSFPDD_P11[44] = Bias_Current_CH1 >> 8 ;
					QSFPDD_P11[45] = Bias_Current_CH1 ;
					// CH1  Tx Power
					QSFPDD_P11[28] = TXP_BUFFER >> 8 ;
					QSFPDD_P11[29] = TXP_BUFFER ;
				}
				break;
		case 2:
				//Bias_Current_CH2 = TXBIAS_TX1_TX4(0x40) ;
        		if(Bias_Current_CH2>0x8000)
					Bias_Current_CH2 = (uint16_t) (QSFPDD_P11[46] << 8) | QSFPDD_P11[47];
                        
				TXP_BUFFER = Bias_Current_CH2 ;
        
                offset_Buffer_B = (CALIB_MEMORY_MAP.IBias2_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.IBias2_OFFSET_LSB );
                offset_Buffer_P = (CALIB_MEMORY_MAP.TXP2_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.TXP2_OFFSET_LSB );

				Bias_Current_CH2 = CAL_Function( Bias_Current_CH2 , CALIB_MEMORY_MAP.IBias2_SCALEM , CALIB_MEMORY_MAP.IBias2_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP2_SCALEM , CALIB_MEMORY_MAP.TXP2_SCALEL , offset_Buffer_P ) ;

				if((QSFPDD_P10[2] & 0x04) || (Tx_Disable_Flag & 0x04))
				{
					QSFPDD_P11[46] = 0 ;
					QSFPDD_P11[47] = 0 ;
					// CH2  Tx Power
					QSFPDD_P11[30] = 0 ;
					QSFPDD_P11[31] = 0 ;

					// Alarm & Waring trigger
					QSFPDD_P11[12] |= 0x04 ;
					QSFPDD_P11[14] |= 0x04 ;
					QSFPDD_P11[16] |= 0x04 ;
					QSFPDD_P11[18] |= 0x04 ;
				}
				else
				{
					QSFPDD_P11[46] = Bias_Current_CH2 >> 8 ;
					QSFPDD_P11[47] = Bias_Current_CH2 ;
					// CH2  Tx Power
					QSFPDD_P11[30] = TXP_BUFFER >> 8 ;
					QSFPDD_P11[31] = TXP_BUFFER ;
				}
			    break;
		case 3:
				//Bias_Current_CH3 = TXBIAS_TX1_TX4(0x60) ;
                if(Bias_Current_CH3>0x8000)
					Bias_Current_CH3 = (uint16_t) (QSFPDD_P11[48] << 8) | QSFPDD_P11[49];
                
				TXP_BUFFER = Bias_Current_CH3 ;
        
                offset_Buffer_B = (CALIB_MEMORY_MAP.IBias3_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.IBias3_OFFSET_LSB );
                offset_Buffer_P = (CALIB_MEMORY_MAP.TXP3_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.TXP3_OFFSET_LSB );

				Bias_Current_CH3 = CAL_Function( Bias_Current_CH3 , CALIB_MEMORY_MAP.IBias3_SCALEM , CALIB_MEMORY_MAP.IBias3_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP3_SCALEM , CALIB_MEMORY_MAP.TXP3_SCALEL , offset_Buffer_P ) ;

				if((QSFPDD_P10[2] & 0x08) || (Tx_Disable_Flag & 0x08))
				{
					QSFPDD_P11[48] = 0 ;
					QSFPDD_P11[49] = 0 ;
					// CH3  Tx Power
					QSFPDD_P11[32] = 0 ;
					QSFPDD_P11[33] = 0 ;

					// Alarm & Waring trigger
					QSFPDD_P11[12] |= 0x08 ;
					QSFPDD_P11[14] |= 0x08 ;
					QSFPDD_P11[16] |= 0x08 ;
					QSFPDD_P11[18] |= 0x08 ;
				}
				else
				{
					QSFPDD_P11[48] = Bias_Current_CH3 >> 8 ;
					QSFPDD_P11[49] = Bias_Current_CH3 ;
					// CH3  Tx Power
					QSFPDD_P11[32] = TXP_BUFFER >> 8 ;
					QSFPDD_P11[33] = TXP_BUFFER ;
				}
				break;
		default:
				break;
	}

}

void RX_POWER_M_CH0_CH4(uint8_t Rx_CH)
{
	uint16_t RX_POWER_CH0,RX_POWER_CH1,RX_POWER_CH2,RX_POWER_CH3;
	uint16_t RX_POWER_CH4,RX_POWER_CH5,RX_POWER_CH6,RX_POWER_CH7;
    
    int16_t  offset_Buffer_RXP = 0 ;
    uint16_t RXLOS_Assert_Buffer = 0 ;
    uint16_t RXLOS_DeAssert_Buffer = 0 ;

	switch(Rx_CH)
	{
		case 0:
			    RX_POWER_CH0 = GET_RSSI_CH0_CH3( 3 );
        
                offset_Buffer_RXP = (CALIB_MEMORY_MAP.RX0_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.RX0_OFFSET_LSB );
                RXLOS_Assert_Buffer = (CALIB_MEMORY_MAP.Rx0_LOS_Assret_MSB << 8 | CALIB_MEMORY_MAP.Rx0_LOS_Assret_LSB  );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_MAP.Rx0_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_MAP.Rx0_LOS_DeAssret_LSB  );

			    RX_POWER_CH0 = CAL_Function( RX_POWER_CH0 , CALIB_MEMORY_MAP.RX0_SCALEM , CALIB_MEMORY_MAP.RX0_SCALEL , offset_Buffer_RXP );
			    if(RX_POWER_CH0>0xF000)
			    	RX_POWER_CH0 = 0;
				// RX RSSI CH 0
				if( RX_POWER_CH0 < RXLOS_Assert_Buffer )
					SHOW_RSSI_CH0 = SHOW_RSSI_Disable ;

				if ( RX_POWER_CH0 >= RXLOS_DeAssert_Buffer )
					SHOW_RSSI_CH0 = SHOW_RSSI_Enable ;

				if( SHOW_RSSI_CH0 == SHOW_RSSI_Enable )
				{
					QSFPDD_P11[58] = RX_POWER_CH0 >> 8 ;
					QSFPDD_P11[59] = RX_POWER_CH0 ;
				}
				else
				{
					QSFPDD_P11[58] = 0 ;
					QSFPDD_P11[59] = 1 ;
				}

				break;
		case 1:
			    RX_POWER_CH1 = GET_RSSI_CH0_CH3( 2 );
        
                offset_Buffer_RXP = (CALIB_MEMORY_MAP.RX1_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.RX1_OFFSET_LSB );
                RXLOS_Assert_Buffer = (CALIB_MEMORY_MAP.Rx1_LOS_Assret_MSB << 8 | CALIB_MEMORY_MAP.Rx1_LOS_Assret_LSB  );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_MAP.Rx1_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_MAP.Rx1_LOS_DeAssret_LSB  );        

			    RX_POWER_CH1 = CAL_Function( RX_POWER_CH1 , CALIB_MEMORY_MAP.RX1_SCALEM , CALIB_MEMORY_MAP.RX1_SCALEL , offset_Buffer_RXP );
			    if(RX_POWER_CH1>0xF000)
			    	RX_POWER_CH1 = 0;
				// RX RSSI CH 1
				if( RX_POWER_CH1 < RXLOS_Assert_Buffer )
					SHOW_RSSI_CH1 = SHOW_RSSI_Disable;

				if( RX_POWER_CH1 >= RXLOS_DeAssert_Buffer )
					SHOW_RSSI_CH1 = SHOW_RSSI_Enable;

				if( SHOW_RSSI_CH1 == SHOW_RSSI_Enable )
				{
					QSFPDD_P11[60] = RX_POWER_CH1 >> 8 ;
					QSFPDD_P11[61] = RX_POWER_CH1 ;
				}
				else
				{
					QSFPDD_P11[60] = 0;
					QSFPDD_P11[61] = 1 ;
				}

				break;
		case 2:
				RX_POWER_CH2 = GET_RSSI_CH0_CH3( 1 );
                offset_Buffer_RXP = (CALIB_MEMORY_MAP.RX2_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.RX2_OFFSET_LSB );
                RXLOS_Assert_Buffer = (CALIB_MEMORY_MAP.Rx2_LOS_Assret_MSB << 8 | CALIB_MEMORY_MAP.Rx2_LOS_Assret_LSB  );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_MAP.Rx2_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_MAP.Rx2_LOS_DeAssret_LSB  );             
        
				RX_POWER_CH2 = CAL_Function( RX_POWER_CH2 , CALIB_MEMORY_MAP.RX2_SCALEM , CALIB_MEMORY_MAP.RX2_SCALEL , offset_Buffer_RXP );
				if(RX_POWER_CH2>0xF000)
					RX_POWER_CH2 = 0;
				// RX RSSI CH 2
				if( RX_POWER_CH2 < RXLOS_Assert_Buffer )
					SHOW_RSSI_CH2 = SHOW_RSSI_Disable;

				if( RX_POWER_CH2 >= RXLOS_DeAssert_Buffer )
					SHOW_RSSI_CH2 = SHOW_RSSI_Enable;

				if(SHOW_RSSI_CH2 == SHOW_RSSI_Enable)
				{
					QSFPDD_P11[62] = RX_POWER_CH2 >> 8 ;
					QSFPDD_P11[63] = RX_POWER_CH2 ;
				}
				else
				{
					QSFPDD_P11[62] = 0;
					QSFPDD_P11[63] = 1 ;
				}

				break;
		case 3:
				RX_POWER_CH3 = GET_RSSI_CH0_CH3( 0 );
                offset_Buffer_RXP = (CALIB_MEMORY_MAP.RX3_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.RX3_OFFSET_LSB );
                RXLOS_Assert_Buffer = (CALIB_MEMORY_MAP.Rx3_LOS_Assret_MSB << 8 | CALIB_MEMORY_MAP.Rx3_LOS_Assret_LSB  );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_MAP.Rx3_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_MAP.Rx3_LOS_DeAssret_LSB  );    
        
				RX_POWER_CH3 = CAL_Function( RX_POWER_CH3 , CALIB_MEMORY_MAP.RX3_SCALEM , CALIB_MEMORY_MAP.RX3_SCALEL , offset_Buffer_RXP );
				if(RX_POWER_CH3>0xF000)
					RX_POWER_CH3 = 0;
				// RX RSSI CH 3
				if( RX_POWER_CH3 < RXLOS_Assert_Buffer )
					SHOW_RSSI_CH3 = SHOW_RSSI_Disable;

				if( RX_POWER_CH3 >= RXLOS_DeAssert_Buffer )
					SHOW_RSSI_CH3 = SHOW_RSSI_Enable;

				if(SHOW_RSSI_CH3==SHOW_RSSI_Enable)
				{
					QSFPDD_P11[64] = RX_POWER_CH3 >> 8 ;
					QSFPDD_P11[65] = RX_POWER_CH3 ;
				}
				else
				{
					QSFPDD_P11[64] = 0;
					QSFPDD_P11[65] = 1 ;
				}
				break;

		default:
			    break;
	}
}

void QSFPDD_DDMI_StateMachine(uint8_t StateMachine )
{
	switch(StateMachine)
	{
		case ADC_SM:
					Temperature_Monitor(GET_GD_Temperature());
					VCC_Monitor( GET_ADC_Value_Data( P3V3_TX_Mon )*2 );
					break;
        
		case RXP_CH0123:
					RX_POWER_M_CH0_CH4(0);
					RX_POWER_M_CH0_CH4(1);
					RX_POWER_M_CH0_CH4(2);
					RX_POWER_M_CH0_CH4(3);
					break;
        
		case TXB_TXP_CH0_SM:
//					if(CALIB_MEMORY_MAP.LUT_EN)
 //                       Bias_MOD_LUT_UpDate_Current(0); // Bias & mod LUT Control
 //                   BIAS_TxPower_Monitor( 0 );
					break;
                    
		case TXB_TXP_CH1_SM:
//					if(CALIB_MEMORY_MAP.LUT_EN)
 //                       Bias_MOD_LUT_UpDate_Current(1); // Bias & mod LUT Control					
//					BIAS_TxPower_Monitor( 1 );
					break;
                    
        case TXB_TXP_CH2_SM:
//					if(CALIB_MEMORY_MAP.LUT_EN)
//                        Bias_MOD_LUT_UpDate_Current(2); // Bias & mod LUT Control
//                    BIAS_TxPower_Monitor( 2 );
                    break;
                    
        case TXB_TXP_CH3_SM:
//					if(CALIB_MEMORY_MAP.LUT_EN)
 ////                       Bias_MOD_LUT_UpDate_Current(3); // Bias & mod LUT Control
 //                   BIAS_TxPower_Monitor( 3 );
                    break;
                    
		case OP_SM:
					if(PowerON_AWEN_flag==1)
						DDMI_AW_Monitor();
					break;
		case TXLOLLOS:
					if(PowerON_AWEN_flag==1)
						CMIS40_TXLOS_LOL_AW();
			        break;
		case RXLOLLOS:
					if(PowerON_AWEN_flag==1)
						CMIS40_RXLOS_LOL_AW();
			        break;

		case Other_DDMI:
					Get_module_Power_Monitor();
					break;

		default:
				break;
	}
}

