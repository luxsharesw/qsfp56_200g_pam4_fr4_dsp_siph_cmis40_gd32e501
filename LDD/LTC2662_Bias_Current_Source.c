#include "gd32e501.h"
#include "systick.h"
#include "GD32E501_SPI_Control.h"
#include "LDD_Control.h"
#include "GD32E501_GPIO_Customize_Define.h"
//--------------------------------------//
// LTC2662 Command define data
//--------------------------------------//
#define WriteCode_ByCH          0x00
#define WriteSpan_ByCH          0x60
#define WriteSpan_ALLCH         0xE0
#define WriteCode_Update_ByCH   0x30
#define PowerDown_ByCH          0x40
#define PowerDown_ALL           0x50
#define Config_Command          0x70
#define Monitor_Mux             0xB0
#define Update_ByCH             0x10
// DAC Current source 
#define DAC_CS_OUT_0            0x00
#define DAC_CS_OUT_1            0x01
#define DAC_CS_OUT_2            0x02
#define DAC_CS_OUT_3            0x03
#define DAC_CS_OUT_4            0x04
// SPI CS Pin control
#define  SET_SPI_CS_LOW()            gpio_bit_reset(GPIOA, GPIO_PIN_4)
#define  SET_SPI_CS_HIGH()           gpio_bit_set(GPIOA, GPIO_PIN_4)

uint8_t Read_LTC2662_FR_Code = 0;
uint8_t Read_LTC2662_Command_ADR_Code = 0;
uint8_t Read_LTC2662_DATA_MSB_Code = 0;
uint8_t Read_LTC2662_DATA_LSB_Code = 0;
//--------------------------------------------// 
// Interface is SPI Data is 24-Bit ( 3 Byte )    
//--------------------------------------------//
// Span codes setting 100mA
// Command
// 0xE0 0x00 0x06
// Write code to Bias current
// 0x00 0x?? 0x?? CH0 Bias current
// 0x01 0x?? 0x?? CH1 Bias current
// 0x02 0x?? 0x?? CH2 Bias current
// 0x03 0x?? 0x?? CH3 Bias current
// Update code
// 0x10 0x?? 0x?? CH0 Bias current
// 0x11 0x?? 0x?? CH1 Bias current
// 0x12 0x?? 0x?? CH2 Bias current
// 0x13 0x?? 0x?? CH3 Bias current
//--------------------------------------------// 
// Fault register ( FR )   
//--------------------------------------------//
// FR0 OC condition detected on OUT0
// FR1 OC condition detected on OUT1
// FR2 OC condition detected on OUT2
// FR3 OC condition detected on OUT3
// FR4 OC condition detected on OUT4
// FR5 Over Temperature Tj > 175 C
// FR6 Power Limit Vddx-Voutx > 10V and Current >= 200 mA
// FR7 Invalid Spi sequence length
//--------------------------------------------// 
// 32bit command formath
//--------------------------------------------//
//      01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 -------- 32
// SDI  X  X  X  X  X  X  X  X  C3 C2 C1 C0 A3 A2 A1 A0 | D15 - D00 |
// SDO  F7 F6 F5 F4 F3 F2 F1 F0 C3 C2 C1 C0 A3 A2 A1 A0 | D15 - D00 |

uint8_t Combination_ByteData(uint8_t Command,uint8_t Channel)
{
    uint8_t Comb_Data = 0 ;    
    Comb_Data = ( Command | Channel ) ;    
    return Comb_Data;
}

void SPI_LTC2662_Update_Current_ALL()
{
    SPI_RW_byteData(0x00);
    SPI_RW_byteData(0x90);
    SPI_RW_byteData(0x00);
    SPI_RW_byteData(0x00);
}

void Config_shutDown_Disable()
{
    Read_LTC2662_FR_Code = SPI_RW_byteData( 0x00 );
    // Setting all channel 
    Read_LTC2662_Command_ADR_Code = SPI_RW_byteData( 0x70 );
    Read_LTC2662_DATA_MSB_Code = SPI_RW_byteData( 0x00 );
    Read_LTC2662_DATA_LSB_Code = SPI_RW_byteData( 0x0E );    
    SPI_LTC2662_Update_Current_ALL();
}

void WriteSpan_Setttin_100mA_Range(uint8_t Data_Value)
{
    SET_SPI_CS_LOW();
    Config_shutDown_Disable();
    delay_1ms(10);  
    //Read_LTC2662_FR_Code = SPI_RW_byteData( 0x00 );
    // Setting all channel 
    Read_LTC2662_FR_Code = SPI_RW_byteData( 0x00 );
    Read_LTC2662_Command_ADR_Code = SPI_RW_byteData( WriteSpan_ALLCH );
    Read_LTC2662_DATA_MSB_Code = SPI_RW_byteData( 0x00 );
    Read_LTC2662_DATA_LSB_Code = SPI_RW_byteData( Data_Value );
    /*delay_1ms(10);
    Read_LTC2662_FR_Code = SPI_RW_byteData( 0x00 );
    Read_LTC2662_Command_ADR_Code = SPI_RW_byteData( 0x61 );
    Read_LTC2662_DATA_MSB_Code = SPI_RW_byteData( 0x00 );
    Read_LTC2662_DATA_LSB_Code = SPI_RW_byteData( Data_Value );
    delay_1ms(10);
    Read_LTC2662_FR_Code = SPI_RW_byteData( 0x00 );
    Read_LTC2662_Command_ADR_Code = SPI_RW_byteData( 0x62 );
    Read_LTC2662_DATA_MSB_Code = SPI_RW_byteData( 0x00 );
    Read_LTC2662_DATA_LSB_Code = SPI_RW_byteData( Data_Value );
    delay_1ms(10);
    Read_LTC2662_FR_Code = SPI_RW_byteData( 0x00 );
    Read_LTC2662_Command_ADR_Code = SPI_RW_byteData( 0x63 );
    Read_LTC2662_DATA_MSB_Code = SPI_RW_byteData( 0x00 );
    Read_LTC2662_DATA_LSB_Code = SPI_RW_byteData( Data_Value );
    */
    //SPI_LTC2662_Update_Current_ALL();
    SET_SPI_CS_HIGH();
}

// SPI Current source test function
void SPI_LTC2662_Command_TEST()
{
    LDD_Control_MEMORY_MAP.Read_data[0] = SPI_RW_byteData(LDD_Control_MEMORY_MAP.Write_data[0]);
    LDD_Control_MEMORY_MAP.Read_data[1] = SPI_RW_byteData(LDD_Control_MEMORY_MAP.Write_data[1]);
    LDD_Control_MEMORY_MAP.Read_data[2] = SPI_RW_byteData(LDD_Control_MEMORY_MAP.Write_data[2]);
    LDD_Control_MEMORY_MAP.Read_data[3] = SPI_RW_byteData(LDD_Control_MEMORY_MAP.Write_data[3]);
}

void SPI_LTC2662_BiasCurrent(uint8_t Channel ,uint8_t BCMSB,uint8_t BCLSB)
{
    uint8_t Data_channel_Value = 0;
    
    if(Channel==0)
        Data_channel_Value = 0x30;
    else if(Channel==1)
        Data_channel_Value = 0x31;
    else if(Channel==2)
        Data_channel_Value = 0x32;
    else if(Channel==3)
        Data_channel_Value = 0x33;
    
    SET_SPI_CS_LOW();      
    LDD_Control_MEMORY_MAP.Read_data[0] = SPI_RW_byteData(0x00);
    LDD_Control_MEMORY_MAP.Read_data[1] = SPI_RW_byteData(Data_channel_Value);
    LDD_Control_MEMORY_MAP.Read_data[2] = SPI_RW_byteData(BCMSB);
    LDD_Control_MEMORY_MAP.Read_data[3] = SPI_RW_byteData(BCLSB);
    SET_SPI_CS_HIGH();
}

void SPI_Current_Source_PowerDown(uint8_t Channel)
{
    switch(Channel)
    {
        case 0:
            SPI_RW_byteData(0x00);
            SPI_RW_byteData(0x40);
            SPI_RW_byteData(0x00);
            SPI_RW_byteData(0x00);
            break;
        case 1:
            SPI_RW_byteData(0x00);
            SPI_RW_byteData(0x41);
            SPI_RW_byteData(0x00);
            SPI_RW_byteData(0x00);
            break;
        case 2:
            SPI_RW_byteData(0x00);
            SPI_RW_byteData(0x42);
            SPI_RW_byteData(0x00);
            SPI_RW_byteData(0x00);
            break;
        case 3:
            SPI_RW_byteData(0x00);
            SPI_RW_byteData(0x43);
            SPI_RW_byteData(0x00);
            SPI_RW_byteData(0x00);
            break;
        default:
            break;
    }
}

void SET_Bias_Current_ALLCH()
{
    SPI_LTC2662_BiasCurrent( 0 , LDD_Control_MEMORY_MAP.BiasC_MSB_CH0 , LDD_Control_MEMORY_MAP.BiasC_LSB_CH0 );
    SPI_LTC2662_BiasCurrent( 1 , LDD_Control_MEMORY_MAP.BiasC_MSB_CH1 , LDD_Control_MEMORY_MAP.BiasC_LSB_CH1 );
    SPI_LTC2662_BiasCurrent( 2 , LDD_Control_MEMORY_MAP.BiasC_MSB_CH2 , LDD_Control_MEMORY_MAP.BiasC_LSB_CH2 );
    SPI_LTC2662_BiasCurrent( 3 , LDD_Control_MEMORY_MAP.BiasC_MSB_CH3 , LDD_Control_MEMORY_MAP.BiasC_LSB_CH3 );
//    SPI_LTC2662_Update_Current_ALL();
}




