#include "gd32e501.h"
#include "systick.h"
#include "GD32E501_GPIO_Customize_Define.h"
#include "GD32E501_M_I2C1_PB3_PB4.h"
#include "LDD_Control.h"
#include "CMIS_MSA.h"
//--------------------------------//
//Max5825 Slave Address
//--------------------------------//
// ADDR1   ADDR0   A3  A2  A1  A0
//   GND     GND    0   0   0   0
// 0 0 1 A3 A2 A1 A0 RW
// 0 0 1  0  0  0  0  0
#define Max5825_SADR       0x20
//--------------------------------//
// DAC0 Channel MEM Setting
//--------------------------------//
// CODEn Command           1000
// LOADn Command           1001
// CODEn_LOADn Command     1011
#define M5825_MEM_DAC0     0xB0
#define M5825_MEM_DAC1     0xB1
#define M5825_MEM_DAC2     0xB2
#define M5825_MEM_DAC3     0xB3
#define M5825_MEM_DAC4     0xB4
#define M5825_MEM_DAC5     0xB5
#define M5825_MEM_DAC6     0xB6
#define M5825_MEM_DAC7     0xB7
//----------------------------------------------------//
// vref use external or internal setting value
//----------------------------------------------------//
#define Ext_VREF              0
#define V25_VREF              1
#define V20_VREF              2
#define V41_VREF              3

struct LDD_Control_MEMORY LDD_Control_MEMORY_MAP;

void Max5825_DAC_CLR_C()
{
    // Active Low Clear
    Max5825_CLR_Low();
    delay_1ms(1);
    Max5825_CLR_High();
    delay_1ms(1);
}

// Low High Tirgger Loader data to dac output
void Max5825_DAC_REG_Relaod()
{
    Max5825_LOAD_Low(); 
    delay_1ms(1);
    Max5825_LOAD_High();
    delay_1ms(1);
}

void MAX5825_PowerOn()
{
    Max5825_DAC_CLR_C();
    Master_I2C1_TwoByte_PB34(Max5825_SADR,0x40,0xFF,0x00);
    Max5825_DAC_REG_Relaod();
}

void Max5825_Config()
{
    //LDAC_ENB = Enable bit4
    //CLEAR_ENB = Envable bit3
    Max5825_DAC_CLR_C();
    Master_I2C1_TwoByte_PB34(Max5825_SADR,0x50,0xFF,0x18);
    Max5825_DAC_REG_Relaod();
}

void MAX5825_VREF_SET(uint8_t VERF_MODE)
{
    Max5825_DAC_CLR_C();
    if(VERF_MODE==Ext_VREF)
        Master_I2C1_TwoByte_PB34(Max5825_SADR,0x24,0x00,0x00);
    else if(VERF_MODE==V25_VREF)
        Master_I2C1_TwoByte_PB34(Max5825_SADR,0x25,0x00,0x00);
    else if(VERF_MODE==V20_VREF)
        Master_I2C1_TwoByte_PB34(Max5825_SADR,0x26,0x00,0x00);
    else if(VERF_MODE==V41_VREF)
        Master_I2C1_TwoByte_PB34(Max5825_SADR,0x27,0x00,0x00);
    Max5825_DAC_REG_Relaod();
}

// This Channel by sch dac channel Configuration 
void MAX5825_DAC_output(uint8_t Channel ,uint16_t DAC_Value)
{
    uint8_t MSB_DAC = 0;
    uint8_t LSB_DAC = 0;   
    uint8_t Max5825_REG_ADR = 0;   
    
    // Max5825 OUT0  -> Sch HW DAC7
    // Max5825 OUT1  -> Sch HW DAC6
    // Max5825 OUT2  -> Sch HW DAC5
    // Max5825 OUT3  -> Sch HW DAC4
    // Max5825 OUT4  -> Sch HW DAC3
    // Max5825 OUT5  -> Sch HW DAC2
    // Max5825 OUT6  -> Sch HW DAC1
    // Max5825 OUT7  -> Sch HW DAC0  
    if(Channel==7)
        Max5825_REG_ADR = M5825_MEM_DAC0;
    else if(Channel==6)
        Max5825_REG_ADR = M5825_MEM_DAC1;
    else if(Channel==5)
        Max5825_REG_ADR = M5825_MEM_DAC2;
    else if(Channel==4)
        Max5825_REG_ADR = M5825_MEM_DAC3;
    else if(Channel==3)
        Max5825_REG_ADR = M5825_MEM_DAC4;
    else if(Channel==2)
        Max5825_REG_ADR = M5825_MEM_DAC5;
    else if(Channel==1)
        Max5825_REG_ADR = M5825_MEM_DAC6;
    else if(Channel==0)
        Max5825_REG_ADR = M5825_MEM_DAC7;
    
    Max5825_DAC_CLR_C();
    LSB_DAC = DAC_Value ;
    MSB_DAC = ( DAC_Value >> 8 );
    Master_I2C1_TwoByte_PB34(Max5825_SADR,Max5825_REG_ADR,MSB_DAC,LSB_DAC);
    Max5825_DAC_REG_Relaod();    
}

void Max5825_initial()
{
    MAX5825_PowerOn();
    Max5825_Config();
    // external Vref voltage is 3.3V
    MAX5825_VREF_SET(Ext_VREF);
}

void ADC_EAM_CH1_CH4_Mon()
{
    uint16_t Get_Adc_data_buffer = 0 ;
    // EAM ADC Voltage CH1
    Get_Adc_data_buffer = GET_ADC_Value_Data(EAM1_MON);
    LDD_Control_MEMORY_MAP.EAM_ADC_MSB_CH1 = Get_Adc_data_buffer>>8;
    LDD_Control_MEMORY_MAP.EAM_ADC_LSB_CH1 = Get_Adc_data_buffer;
    // EAM ADC Voltage CH2
    Get_Adc_data_buffer = GET_ADC_Value_Data(EAM2_MON);
    LDD_Control_MEMORY_MAP.EAM_ADC_MSB_CH2 = Get_Adc_data_buffer>>8;
    LDD_Control_MEMORY_MAP.EAM_ADC_LSB_CH2 = Get_Adc_data_buffer;
    // EAM ADC Voltage CH3
    Get_Adc_data_buffer = GET_ADC_Value_Data(EAM3_MON);
    LDD_Control_MEMORY_MAP.EAM_ADC_MSB_CH3 = Get_Adc_data_buffer>>8;
    LDD_Control_MEMORY_MAP.EAM_ADC_LSB_CH3 = Get_Adc_data_buffer;
    // EAM ADC Voltage CH4
    Get_Adc_data_buffer = GET_ADC_Value_Data(EAM4_MON);
    LDD_Control_MEMORY_MAP.EAM_ADC_MSB_CH4 = Get_Adc_data_buffer>>8;
    LDD_Control_MEMORY_MAP.EAM_ADC_LSB_CH4 = Get_Adc_data_buffer;     
}

void ADC_Heater_Mon()
{
    uint16_t Get_Adc_data_buffer = 0 ;
    // Heater Voltage CH1
    Get_Adc_data_buffer = GET_ADC_Value_Data(Heater_CH1_Mon);
    LDD_Control_MEMORY_MAP.Heater_ADC_MSB_CH1 = Get_Adc_data_buffer>>8;
    LDD_Control_MEMORY_MAP.Heater_ADC_LSB_CH1 = Get_Adc_data_buffer;   
    // Heater Voltage CH2
    Get_Adc_data_buffer = GET_ADC_Value_Data(Heater_CH2_Mon);
    LDD_Control_MEMORY_MAP.Heater_ADC_MSB_CH2 = Get_Adc_data_buffer>>8;
    LDD_Control_MEMORY_MAP.Heater_ADC_LSB_CH2 = Get_Adc_data_buffer;  
    // Heater Voltage CH3
    Get_Adc_data_buffer = GET_ADC_Value_Data(Heater_CH3_Mon);
    LDD_Control_MEMORY_MAP.Heater_ADC_MSB_CH3 = Get_Adc_data_buffer>>8;
    LDD_Control_MEMORY_MAP.Heater_ADC_LSB_CH3 = Get_Adc_data_buffer;  
    // Heater Voltage CH4
    Get_Adc_data_buffer = GET_ADC_Value_Data(Heater_CH4_Mon);
    LDD_Control_MEMORY_MAP.Heater_ADC_MSB_CH4 = Get_Adc_data_buffer>>8;
    LDD_Control_MEMORY_MAP.Heater_ADC_LSB_CH4 = Get_Adc_data_buffer;      
}

uint16_t DAC_DataValue_Left(uint8_t DAC_MSB , uint8_t DAC_LSB )
{
    uint16_t Dac_LeftData = 0 ;   
    Dac_LeftData = ( DAC_MSB<<8 | DAC_LSB ) ;
    Dac_LeftData = Dac_LeftData<<4 ;   
    return Dac_LeftData ;   
}

void SET_DAC_ALLCH_Value()
{   
    uint16_t Max5825_DAC_Left_data_Value=0;
    
    Max5825_DAC_Left_data_Value = DAC_DataValue_Left(LDD_Control_MEMORY_MAP.EAM_DAC_MSB_CH0, LDD_Control_MEMORY_MAP.EAM_DAC_LSB_CH0 );
    MAX5825_DAC_output( 0 , Max5825_DAC_Left_data_Value );
    
    Max5825_DAC_Left_data_Value = DAC_DataValue_Left(LDD_Control_MEMORY_MAP.EAM_DAC_MSB_CH1, LDD_Control_MEMORY_MAP.EAM_DAC_LSB_CH1 );
    MAX5825_DAC_output( 1 , Max5825_DAC_Left_data_Value );
    
    Max5825_DAC_Left_data_Value = DAC_DataValue_Left(LDD_Control_MEMORY_MAP.EAM_DAC_MSB_CH2, LDD_Control_MEMORY_MAP.EAM_DAC_LSB_CH2 );
    MAX5825_DAC_output( 2 , Max5825_DAC_Left_data_Value );
    
    Max5825_DAC_Left_data_Value = DAC_DataValue_Left(LDD_Control_MEMORY_MAP.EAM_DAC_MSB_CH3, LDD_Control_MEMORY_MAP.EAM_DAC_LSB_CH3 );    
    MAX5825_DAC_output( 3 , Max5825_DAC_Left_data_Value );
    
    Max5825_DAC_Left_data_Value = DAC_DataValue_Left(LDD_Control_MEMORY_MAP.Heater_DAC_MSB_CH4, LDD_Control_MEMORY_MAP.Heater_DAC_LSB_CH4 ); 
    MAX5825_DAC_output( 4 , Max5825_DAC_Left_data_Value );
    
    Max5825_DAC_Left_data_Value = DAC_DataValue_Left(LDD_Control_MEMORY_MAP.Heater_DAC_MSB_CH5, LDD_Control_MEMORY_MAP.Heater_DAC_LSB_CH5 ); 
    MAX5825_DAC_output( 5 , Max5825_DAC_Left_data_Value );
    
    Max5825_DAC_Left_data_Value = DAC_DataValue_Left(LDD_Control_MEMORY_MAP.Heater_DAC_MSB_CH6, LDD_Control_MEMORY_MAP.Heater_DAC_LSB_CH6 ); 
    MAX5825_DAC_output( 6 , Max5825_DAC_Left_data_Value );
    
    Max5825_DAC_Left_data_Value = DAC_DataValue_Left(LDD_Control_MEMORY_MAP.Heater_DAC_MSB_CH7, LDD_Control_MEMORY_MAP.Heater_DAC_LSB_CH7 ); 
    MAX5825_DAC_output( 7 , Max5825_DAC_Left_data_Value );
}

void PowerON_DAC_Initialize()
{
    Max5825_initial();
    //--------------------------------//
    // Setting dac output vaule
    //--------------------------------//
}

