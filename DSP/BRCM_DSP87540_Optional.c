#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "GD32E501_M_I2C2_PC78.h"
#include "BRCM_DSP87540_RW.h"
#include "BRCM_DSP_87540.h"
#include "string.h"

//-----------------------------------------------------------------------------------------------------//
// DSP87540 SystemSide Digital/Remote Loopback function
// LoopBack_mode = 1  Digital Loopback
// LoopBack_mode = 0  remote Loopbcak
//-----------------------------------------------------------------------------------------------------//
void DSP87540_SystemSide_Loopback_SET(uint8_t LoopBack_mode,uint8_t Lane_CH,uint8_t Enable)
{
    if(Lane_CH==0)
    {
        COMMAND_4700_DATA[0]  = 0x00000002 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x0000800A );
    }
    else if(Lane_CH==1)
    {
        COMMAND_4700_DATA[0]  = 0x00010002 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x0000800A );
    }
    else if(Lane_CH==2)
    {
        COMMAND_4700_DATA[0]  = 0x00020002 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x0000800A );
    }
    else if(Lane_CH==3)
    {
        COMMAND_4700_DATA[0]  = 0x00030002 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x0000800A );
    }
     //Digital Loopback
    if(LoopBack_mode==1)
    {
        if(Enable)
            COMMAND_4700_DATA[1]  = 0xCCCC0000 | 0x0100 ;
        else
            COMMAND_4700_DATA[1]  = 0xCCCC0000 & ~0x0100;
    }
    //Remote Loopback
    else
    {
        if(Enable)
            COMMAND_4700_DATA[1]  = 0xCCCC0001 | 0x0100 ;
        else
            COMMAND_4700_DATA[1]  = 0xCCCC0001 & ~0x0100 ;
    }
           
    DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84);
    DSP87540_CAPI_Command_response(0x5201CA8C,0x5201CA90);        
}

//-----------------------------------------------------------------------------------------------------//
// DSP87540 SystemSide TRX Squelch
// TRX_Side = 1  TX Squelch ( Moudle Rx ouput )
// TRX_Side = 0  RX Squelch
//-----------------------------------------------------------------------------------------------------//
void DSP87540_SystemSide_TRX_Squelch_SET(uint8_t Lane_CH ,uint8_t TRX_Side,uint8_t Enable)
{  
    if(Lane_CH==0)    
    {
        COMMAND_4700_DATA[0] = 0x00000008 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x00008006 );
    }
    else if(Lane_CH==1)    
    {
        COMMAND_4700_DATA[0] = 0x00010008 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x00008006 );
    }
    else if(Lane_CH==2)   
    {
        COMMAND_4700_DATA[0] = 0x00020008 ;        
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x00008006 );
    }
    else if(Lane_CH==3)   
    {
        COMMAND_4700_DATA[0] = 0x00030008 ;           
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x00008006 );
    }
    else if(Lane_CH==0x0F)
    {
        COMMAND_4700_DATA[0] = 0x00000008 ;           
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x0000000F , 0x5201CA84 , 0x00008006 );
    }        
     
    if(TRX_Side)
    {
        COMMAND_4700_DATA[1] = 0x00000200;
        if(Enable)
            COMMAND_4700_DATA[2] = 0x00000000 | 0x00000800 ;
        else
            COMMAND_4700_DATA[2] = 0x00000000 & ~0x00000800 ;
    }
    else
    {
        COMMAND_4700_DATA[1] = 0x00000400;
        if(Enable)
            COMMAND_4700_DATA[2] = 0x00000000 | 0x00001000 ;
        else
            COMMAND_4700_DATA[2] = 0x00000000 & ~0x00001000 ;
    }  
    
    DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84);
    DSP87540_CAPI_Command_response(0x5201CA8C,0x5201CA90);     
}

//-----------------------------------------------------------------------------------------------------//
// DSP87540 Line Digital/Remote Loopback function
// LoopBack_mode = 1  Digital Loopback
// LoopBack_mode = 0  remote Loopbcak
//-----------------------------------------------------------------------------------------------------//
void DSP87540_LineSide_Loopback_SET(uint8_t LoopBack_mode,uint8_t Lane_CH,uint8_t Enable)
{
    if(Lane_CH==0)
    {
        COMMAND_4700_DATA[0]  = 0x00000002 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x0000800A );
    }
    else if(Lane_CH==1)
    {
        COMMAND_4700_DATA[0]  = 0x00010002 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x0000800A );
    }
    else if(Lane_CH==2)
    {
        COMMAND_4700_DATA[0]  = 0x00020002 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x0000800A );
    }
    else if(Lane_CH==3)
    {
        COMMAND_4700_DATA[0]  = 0x00030002 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x0000800A );
    }
    //Digital Loopback
    if(LoopBack_mode==1)
    {
        if(Enable)
            COMMAND_4700_DATA[1]  = 0xCCCC0000 | 0x0100 ;
        else
            COMMAND_4700_DATA[1]  = 0xCCCC0000 & ~0x0100;
    }
    //Remote Loopback
    else
    {
        if(Enable)
            COMMAND_4700_DATA[1]  = 0xCCCC0001 | 0x0100 ;
        else
            COMMAND_4700_DATA[1]  = 0xCCCC0001 & ~0x0100 ;
    }
           
    DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94);
    DSP87540_CAPI_Command_response(0x5201CA9C,0x5201CAA0);        
}

//-----------------------------------------------------------------------------------------------------//
// DSP87540 LineSide TRX Squelch
// TRX_Side = 1  TX Squelch ( Moudle Rx ouput )
// TRX_Side = 0  RX Squelch
//-----------------------------------------------------------------------------------------------------//
void DSP87540_LineSide_TRX_Squelch_SET(uint8_t Lane_CH ,uint8_t TRX_Side,uint8_t Enable)
{  
    if(Lane_CH==0)    
    {
        COMMAND_4700_DATA[0] = 0x00000008 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x00008006 );
    }
    else if(Lane_CH==1)    
    {
        COMMAND_4700_DATA[0] = 0x00010008 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x00008006 );
    }
    else if(Lane_CH==2)   
    {
        COMMAND_4700_DATA[0] = 0x00020008 ;        
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x00008006 );
    }
    else if(Lane_CH==3)   
    {
        COMMAND_4700_DATA[0] = 0x00030008 ;           
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x00008006 );
    }
     
    if(TRX_Side)
    {
        COMMAND_4700_DATA[1] = 0x00000200;
        if(Enable)
            COMMAND_4700_DATA[2] = 0x00000000 | 0x00000800 ;
        else
            COMMAND_4700_DATA[2] = 0x00000000 & ~0x00000800 ;
    }
    else
    {
        COMMAND_4700_DATA[1] = 0x00000400;
        if(Enable)
            COMMAND_4700_DATA[2] = 0x00000000 | 0x00001000 ;
        else
            COMMAND_4700_DATA[2] = 0x00000000 & ~0x00001000 ;
    }  
    
    DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94);
    DSP87540_CAPI_Command_response(0x5201CA9C,0x5201CAA0);     
}
//-----------------------------------------------------------------------------------------------------//
// DSP87540 System Side PRBS Controls
// 
// 
//-----------------------------------------------------------------------------------------------------//
void DSP87540_SystemSide_PRBS_SET(uint8_t Pattern ,uint8_t Lane_CH ,uint8_t Enable)
{
    //Lane Select
    if(Lane_CH==0)
    {
        COMMAND_4700_DATA[0]  = 0x0000002C ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x0000800C );
    }
    else if(Lane_CH==1)
    {
        COMMAND_4700_DATA[0]  = 0x0001002C ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x0000800C );
    }
    else if(Lane_CH==2)
    {
        COMMAND_4700_DATA[0]  = 0x00020002C ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x0000800C );
    }
    else if(Lane_CH==3)
    {
        COMMAND_4700_DATA[0]  = 0x0003002C ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x0000800C );
    }
    
    //PRBS Generator
    if(Pattern!=SSPRQ_SystemSide)
        COMMAND_4700_DATA[1]  = 0x00000001;
    else
        COMMAND_4700_DATA[1]  = 0x00000004;
    
    //PRBS Enable
    if(Enable)
        COMMAND_4700_DATA[2]  = 0x00000001;
    else
        COMMAND_4700_DATA[2]  = 0x00000000;
    
    //PRBS Pattern
    if(Pattern!=SSPRQ_SystemSide)
        COMMAND_4700_DATA[3] = 0x00000000 | (Pattern<<8);
    //SSPRQ
    else
        COMMAND_4700_DATA[3] = 0x00000000;
    
    COMMAND_4700_DATA[4] = 0x00000000;
    COMMAND_4700_DATA[5] = 0x00000000;
    COMMAND_4700_DATA[6] = 0x00000000;
    COMMAND_4700_DATA[7] = 0x00000000;
    COMMAND_4700_DATA[8] = 0x00000000;
    COMMAND_4700_DATA[9] = 0x00000000;
    COMMAND_4700_DATA[10] = 0x00000000;
    COMMAND_4700_DATA[11] = 0x00000000;
    
    DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84);
    DSP87540_CAPI_Command_response(0x5201CA8C,0x5201CA90);   
}

//-----------------------------------------------------------------------------------------------------//
// DSP87540 Line Side PRBS Controls
// 
// 
//-----------------------------------------------------------------------------------------------------//
void DSP87540_LineSide_PRBS_SET(uint8_t Pattern ,uint8_t Lane_CH ,uint8_t Enable)
{
    //Lane Select
    if(Lane_CH==0)
    {
        COMMAND_4700_DATA[0]  = 0x0000002C ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x0000800C );
    }
    else if(Lane_CH==1)
    {
        COMMAND_4700_DATA[0]  = 0x0001002C ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x0000800C );
    }
    else if(Lane_CH==2)
    {
        COMMAND_4700_DATA[0]  = 0x00020002C ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x0000800C );
    }
    else if(Lane_CH==3)
    {
        COMMAND_4700_DATA[0]  = 0x0003002C ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x0000800C );
    }
    
    //PRBS Generator
    if(Pattern!=SSPRQ_LineSide)
        COMMAND_4700_DATA[1]  = 0x00000001;
    //SSPRQ Generator
    else
        COMMAND_4700_DATA[1]  = 0x00000004;
    
    //PRBS Enable
    if(Enable)
        COMMAND_4700_DATA[2]  = 0x00000001;
    else
        COMMAND_4700_DATA[2]  = 0x00000000;
    
    //PRBS Pattern
    if(Pattern!=SSPRQ_LineSide)
        COMMAND_4700_DATA[3] = 0x00000000 | Pattern;
    //SSPRQ
    else
        COMMAND_4700_DATA[3] = 0x00000007;
    
    COMMAND_4700_DATA[4] = 0x00000000;
    COMMAND_4700_DATA[5] = 0x00000000;
    COMMAND_4700_DATA[6] = 0x00000000;
    COMMAND_4700_DATA[7] = 0x00000000;
    COMMAND_4700_DATA[8] = 0x00000000;
    COMMAND_4700_DATA[9] = 0x00000000;
    COMMAND_4700_DATA[10] = 0x00000000;
    COMMAND_4700_DATA[11] = 0x00000000;
    
    DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94);
    DSP87540_CAPI_Command_response(0x5201CA9C,0x5201CAA0);   
}

uint16_t DSP87540_System_Side_LOL()
{
    // TX LOL�@bit0 - bit3, Lock:1,nolock:0
    // System Side Input
    uint16_t GET_LOL_STATUS = 0x0000;
    
    GET_LOL_STATUS = ~BRCM_Control_READ_Data(0x5201CA38);
    
    return GET_LOL_STATUS;
}

uint16_t DSP87540_System_Side_LOS()
{
    // TX LOS�@bit0 - bit3 , Lock:1,nolock:0
    // System Side Output
    uint16_t GET_LOS_STATUS = 0x0000;
    
    GET_LOS_STATUS = ~BRCM_Control_READ_Data(0x5201CA3C);
    
    return GET_LOS_STATUS;
}

uint16_t DSP87540_Line_Side_LOL()
{
    uint16_t Get_LOL_Status = 0x0000;
    // Rx LOL�@bit0 - bit3 , Lock:1,nolock:0
    // Line Side Input
    Get_LOL_Status = ~BRCM_Control_READ_Data(0x5201CA34);
    
    return Get_LOL_Status;
}
