#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "GD32E501_M_I2C2_PC78.h"
#include "BRCM_DSP87540_RW.h"
#include "BRCM_DSP_87540.h"
#include "string.h"
#include "Calibration_Struct.h"
#include "CMIS_MSA.h"

uint32_t COMMAND_4700_DATA[18]={0};

uint32_t INTF_CAPI_SW_CMD_ADR0    = 0x5201CA78;
uint32_t INTF_CAPI_SW_CMD_DATA0   = 0x0000800F;
uint32_t INTF_CAPI_SW_CMD_ADR1    = 0x5201CA74;
uint32_t INTF_CAPI_SW_CMD_DATA1   = 0x00008008;

uint8_t  DSP_Ready_Flag = 0 ;
                                
struct BRCM_87540_DSP_LS_PHY0_MEMORY BRCM_87540_DSP_LS_PHY0_MEMORY_MAP;
struct BRCM_87540_DSP_SS_PHY0_MEMORY BRCM_87540_DSP_SS_PHY0_MEMORY_MAP;

void BRCM_Control_WRITE_Data( uint32_t BRCM_ADDR , uint32_t Data_Value , uint16_t Data_Length )
{
	uint8_t ADDR_Byte3,ADDR_Byte2,ADDR_Byte1,ADDR_Byte0;
	uint8_t Write_data_buffer_0,Write_data_buffer_1,Write_data_buffer_2,Write_data_buffer_3;
	uint8_t LEN_LSB,LEN_MSB ;

	ADDR_Byte0 = BRCM_ADDR ;
	ADDR_Byte1 = BRCM_ADDR>>8 ;
	ADDR_Byte2 = BRCM_ADDR>>16 ;
	ADDR_Byte3 = BRCM_ADDR>>24 ;

	LEN_LSB = ( Data_Length & 0xFF ) ;
	LEN_MSB = ( ( Data_Length >> 8 ) & 0xFF ) ;

	Write_data_buffer_0 = Data_Value ;
	Write_data_buffer_1 = Data_Value >> 8 ;
    Write_data_buffer_2 = Data_Value >> 16 ;
    Write_data_buffer_3 = Data_Value >> 24 ;

	// Step1 : Write 0xFF to QSFP PAGE SELECT Control register
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_PAGE_SELECT , 0xFF );
	// Step2 : Write INDADDR0 - INDADDR3
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_IND_ADDR0 , ADDR_Byte0 );
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_IND_ADDR1 , ADDR_Byte1 );
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_IND_ADDR2 , ADDR_Byte2 );
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_IND_ADDR3 , ADDR_Byte3 );
	// Step3 : Write Data Length
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_IND_LEN0 , LEN_LSB );
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_IND_LEN1 , LEN_MSB );
	// Step4 : Write Data to WFIFO 0x03 write command
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_IND_CTRL , 0x03 );
	// Step5 : Write Data
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_WFIFO , Write_data_buffer_0 );
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_WFIFO , Write_data_buffer_1 );
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_WFIFO , Write_data_buffer_2 );
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_WFIFO , Write_data_buffer_3 );   
}

uint32_t BRCM_Control_READ_Data( uint32_t BRCM_ADDR )
{
	uint8_t ADDR_Byte3,ADDR_Byte2,ADDR_Byte1,ADDR_Byte0;
	uint8_t BRCM_59281_I2C_ADR ;
	uint8_t IIcount = 0;
	uint8_t Read_data_buffer_0,Read_data_buffer_1,Read_data_buffer_2,Read_data_buffer_3;
	uint32_t  return_DATA = 0x00000000 ;
	uint8_t TEMP_BUUFFER;

	ADDR_Byte0 = BRCM_ADDR ;
	ADDR_Byte1 = BRCM_ADDR>>8 ;
	ADDR_Byte2 = BRCM_ADDR>>16 ;
	ADDR_Byte3 = BRCM_ADDR>>24 ;

	// Step1 : Write 0xFF to QSFP PAGE SELECT Control register
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_PAGE_SELECT , 0xFF );
	// Step2 : Write INDADDR0 - INDADDR3
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_IND_ADDR0 , ADDR_Byte0 );
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_IND_ADDR1 , ADDR_Byte1 );
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_IND_ADDR2 , ADDR_Byte2 );
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_IND_ADDR3 , ADDR_Byte3 );
	// Step3 : Write Data Length
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_IND_LEN0 , 4 );
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_IND_LEN1 , 0 );
	// Step4 : Write Data to WFIFO 0x03 write command
	Master_I2C2_ByteWrite_PC78( BRCM87540_I2C_ADR , BRCM_IND_CTRL , 0x01 );
	// Step5 : Write Data
	Read_data_buffer_0 = Master_I2C2_ByteREAD_PC78( BRCM87540_I2C_ADR , BRCM_RFIFO );
	Read_data_buffer_1 = Master_I2C2_ByteREAD_PC78( BRCM87540_I2C_ADR , BRCM_RFIFO );
	Read_data_buffer_2 = Master_I2C2_ByteREAD_PC78( BRCM87540_I2C_ADR , BRCM_RFIFO );
	Read_data_buffer_3 = Master_I2C2_ByteREAD_PC78( BRCM87540_I2C_ADR , BRCM_RFIFO );
    
    return_DATA |= Read_data_buffer_0 ;
    return_DATA |= Read_data_buffer_1 <<8 ;
    return_DATA |= Read_data_buffer_2 <<16 ;
    return_DATA |= Read_data_buffer_3 <<24 ;
    
	return return_DATA;
}

//-----------------------------------------------------------------------------------------------------//
// DSP87540 Chip Status uc Ready/Avs Done/SPI_Dev_ready
//-----------------------------------------------------------------------------------------------------//
void DS87540_Check_SPI_EEPROM_Done()
{
    //Bit0 : uc_ready       
    //Bit1 : avs_done
    //Bit3 : spi_dev_ready
    //Ready is 0x0B
    BRCM_Control_READ_Data(0x5201D4A0);
}
//-----------------------------------------------------------------------------------------------------//
// DSP87540_CAPI_CW_CMD
//-----------------------------------------------------------------------------------------------------//
void DSP87540_CAPI_CW_CMD(uint32_t INTF_ADR0,uint32_t INTF_DATA0,uint32_t INTF_ADR1,uint32_t INTF_DATA1)
{
    INTF_CAPI_SW_CMD_ADR0 = INTF_ADR0 ;
    INTF_CAPI_SW_CMD_DATA0 = INTF_DATA0 ;
    INTF_CAPI_SW_CMD_ADR1 = INTF_ADR1 ;
    INTF_CAPI_SW_CMD_DATA1 = INTF_DATA1 ;
}

void DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(uint32_t Read_ADDRESS)
{
    uint32_t Read_Buffer = 0;
    uint16_t Iconnt = 0;
    uint16_t For_count = 0;
    uint32_t Base_Offset = 0;
    uint16_t DataSize_Buffer =0;
    
    Read_Buffer = BRCM_Control_READ_Data(Read_ADDRESS);
    
    DataSize_Buffer = COMMAND_4700_DATA[0] ;
    For_count = DataSize_Buffer/4 ;
    
    if(For_count>=1)
        For_count = For_count - 1;
    // Step 1 : set data size and command
    //data size                      Bit0-15 : capi_config_info_t
    //intf_util_assign_tocken Bit16-31: This utility incrementally generate a new token
    BRCM_Control_WRITE_Data( COMMAND_Base_ADR , COMMAND_4700_DATA[0] , BRCM_Default_Length );
    //ref_clk CAPI_REF_CLK_FRQ_156_25_MHZ_ETHERNET
    //func_mode Bit16-31: CAPI_MODE_200G     
    for(Iconnt=0;Iconnt<=For_count;Iconnt++)
    {
        Base_Offset = ( Iconnt+1 ) ;
        BRCM_Control_WRITE_Data( COMMAND_Base_ADR + ( Base_Offset*4 ) , COMMAND_4700_DATA[Base_Offset] , BRCM_Default_Length );
    }
    //INTF_CAPI2FW_CW_CMD_LANE_MASK_GPREG
    BRCM_Control_WRITE_Data( INTF_CAPI_SW_CMD_ADR0 , INTF_CAPI_SW_CMD_DATA0 , BRCM_Default_Length );
    //INTF_CAPI2FW_CW_CMD_REQUEST_GPREG
    BRCM_Control_WRITE_Data( INTF_CAPI_SW_CMD_ADR1 , INTF_CAPI_SW_CMD_DATA1 , BRCM_Default_Length );
}
//-----------------------------------------------------------------------------------------------------//
// DSP87540 intf_capi2fw_command_response
//-----------------------------------------------------------------------------------------------------//
void DSP87540_CAPI_Command_response(uint32_t CHK_ADR0,uint32_t CHK_ADR1)
{
    uint32_t Read_Buffer;
    uint32_t IIcount = 0 ;
    uint32_t Time_out_count = 20;
    // Check whether the FW has processed the command 
    // INTF_CAPI2FW_CW_CMD_RESPONSE_GPREG = 0x00008000
    while(1)
    {
        Read_Buffer = BRCM_Control_READ_Data(CHK_ADR0);      
        if(Read_Buffer==0x00008000)
            break;
        else
        {
            for(IIcount=0;IIcount<10000;IIcount++);
            
            if((Time_out_count--) == 0)
                break;
        }       
    }    
    // This is optional since the tocken verification is already completed 
    // INTF_CAPI2FW_CW_RSP_LANE_MASK_GPREG = 0x0000800F
    Read_Buffer = BRCM_Control_READ_Data(CHK_ADR1);
    
    // clear register data 
    BRCM_Control_WRITE_Data( 0x5201CA84 , 0x00000000 , BRCM_Default_Length );
    BRCM_Control_WRITE_Data( 0x5201CA94 , 0x00000000 , BRCM_Default_Length );
    BRCM_Control_WRITE_Data( 0x5201CA74 , 0x00000000 , BRCM_Default_Length );
    BRCM_Control_WRITE_Data( CHK_ADR0   , 0x00000000 , BRCM_Default_Length );
    BRCM_Control_WRITE_Data( CHK_ADR1   , 0x00000000 , BRCM_Default_Length ); 
}
//-----------------------------------------------------------------------------------------------------//
// DSP87540 line_lane_cfg_done
//-----------------------------------------------------------------------------------------------------//
void DSP87540_line_lane_cfg_done(uint8_t DSP_CHIP_MODE)
{   
    if((DSP_CHIP_MODE==Mode1_4x53G_PAM4_4x53G_PAM4_1PORT)||
        (DSP_CHIP_MODE==Mode3_4x25G_NRZ_4x25G_NRZ_1PORT)||
        (DSP_CHIP_MODE==Mode4_1x53G_PAM4_1x53G_PAM4_4PORT)||
        (DSP_CHIP_MODE==Mode2_2x53G_PAM4_2x53G_PAM4_2PORT)||
        (DSP_CHIP_MODE==Mode13_1x25G_NRZ_1x25G_NRZ_4PORT))
    {
        BRCM_Control_READ_Data(0x5201CDB8);    
        BRCM_Control_READ_Data(0x5201CB40);
        BRCM_Control_READ_Data(0x5201CB44);
        BRCM_Control_READ_Data(0x5201CB48);
        BRCM_Control_READ_Data(0x5201CB4C);    
        BRCM_Control_READ_Data(0x5201CB80);
        BRCM_Control_READ_Data(0x5201CB84);
        BRCM_Control_READ_Data(0x5201CB88);
        BRCM_Control_READ_Data(0x5201CB8C);
    }
    else if((DSP_CHIP_MODE==Mode8_4X25G_NRZ_2X50G_PAM4_1PORT)||(DSP_CHIP_MODE==Mode17_4x25G_NRZ_2x53G_PAM4_1PORT))
    {
        BRCM_Control_READ_Data(0x5201CDB8);  
        BRCM_Control_READ_Data(0x5201CB40);    
        BRCM_Control_READ_Data(0x5201CB44); 
        BRCM_Control_READ_Data(0x5201CB80);
        BRCM_Control_READ_Data(0x5201CB84);
        BRCM_Control_READ_Data(0x5201CB88);
        BRCM_Control_READ_Data(0x5201CB8C);
    }
    else if(DSP_CHIP_MODE==Mode5_4x26G_NRZ_2x53G_PAM4_1PORT)
    {
        BRCM_Control_READ_Data(0x5201CDB8);    
        BRCM_Control_READ_Data(0x5201CB40);
        BRCM_Control_READ_Data(0x5201CB44);   
        BRCM_Control_READ_Data(0x5201CB80);
        BRCM_Control_READ_Data(0x5201CB84);
        BRCM_Control_READ_Data(0x5201CB88);
        BRCM_Control_READ_Data(0x5201CB8C);
    }
    else if(DSP_CHIP_MODE==Mode12_4x26G_NRZ_4x26G_NRZ_1PORT)
    {
        BRCM_Control_READ_Data(0x5201CDB8);    
        BRCM_Control_READ_Data(0x5201CB40);
        BRCM_Control_READ_Data(0x5201CB44);   
        BRCM_Control_READ_Data(0x5201CB80);
        BRCM_Control_READ_Data(0x5201CB84);
        BRCM_Control_READ_Data(0x5201CB88);
        BRCM_Control_READ_Data(0x5201CB8C);
    }
}
//-----------------------------------------------------------------------------------------------------//
// DSP87540 host_lane_cfg_done
//-----------------------------------------------------------------------------------------------------//
void DSP87540_host_lane_cfg_done()
{
    BRCM_Control_READ_Data(0x5201D494);
    BRCM_Control_READ_Data(0x5201D494);
    BRCM_Control_READ_Data(0x5201D494);
    BRCM_Control_READ_Data(0x5201D494);  
    BRCM_Control_READ_Data(0x5201D490);
    BRCM_Control_READ_Data(0x5201D490);
    BRCM_Control_READ_Data(0x5201D490);
    BRCM_Control_READ_Data(0x5201D490);
    BRCM_Control_READ_Data(0x5201D490);

}
//-----------------------------------------------------------------------------------------------------//
// DSP87540 System Side TRX Polarity
// TRX_Side_SEL = 1  System side Tx
// TRX_Side_SEL = 0  System side Rx
//-----------------------------------------------------------------------------------------------------//
void DSP87540_SystemSide_TRX_Polarity_SET(uint8_t Lane_CH , uint8_t TRX_Side_SEL)
{
    if(Lane_CH==0)
    {
        COMMAND_4700_DATA[0] = 0x00000002;
        
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_TX_Polarity_CH0);
        else
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_RX_Polarity_CH0);
        
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x00008004 );
    }
    else if(Lane_CH==1)
    {
        COMMAND_4700_DATA[0] = 0x00010002;
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_TX_Polarity_CH1);
        else
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_RX_Polarity_CH1);
        
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x00008004 );        
    }
    else if(Lane_CH==2)
    {
        COMMAND_4700_DATA[0] = 0x00020002;
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_TX_Polarity_CH2);
        else
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_RX_Polarity_CH2);
        
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x00008004 );      
    }
    else if(Lane_CH==3)
    {
        COMMAND_4700_DATA[0] = 0x00030002;
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_TX_Polarity_CH3);
        else
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_RX_Polarity_CH3);
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x00008004 );  
    }  
    DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84);
    DSP87540_CAPI_Command_response(0x5201CA8C,0x5201CA90);
}
//-----------------------------------------------------------------------------------------------------//
// DSP87540 System Side TX FIR Setting
//-----------------------------------------------------------------------------------------------------//
void DSP87540_SystemSide_TX_FIR_SET( uint8_t Lane_CH )
{
    if(Lane_CH==0)
    {
        COMMAND_4700_DATA[0]  = 0x00000044 ;
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x00000060 ;
        
        if(Signal_Status==PAM4_to_PAM4)
            //TXFIR_TAPS_PAM4_6TAP
            COMMAND_4700_DATA[3]  = 0x00000003 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH0) << 16 ;
        else
            //TXFIR_TAPS_NRZ_6TAP
            COMMAND_4700_DATA[3]  = 0x00000001 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH0) << 16 ;
        
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH0) << 16  
                                           | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH0) ;
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST2_CH0) << 16 
                                           | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH0) ;
        COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST3_CH0) ;
        COMMAND_4700_DATA[7]  = 0x00000000 ;
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x00008012 );
    }
    else if(Lane_CH==1)
    {
        COMMAND_4700_DATA[0]  = 0x00000044 ;
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x00000060 ;
        
        if(Signal_Status==PAM4_to_PAM4)
            //TXFIR_TAPS_PAM4_6TAP
            COMMAND_4700_DATA[3]  = 0x00000003 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH1) << 16 ;
        else
            //TXFIR_TAPS_NRZ_6TAP
            COMMAND_4700_DATA[3]  = 0x00000001 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH1) << 16 ;
        
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH1) << 16  
                                           | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH1) ;
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST2_CH1) << 16 
                                           | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH1) ;
        COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST3_CH1) ;
        COMMAND_4700_DATA[7]  = 0x00000000 ;
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x00008012 );
    }
    else if(Lane_CH==2)
    {
        COMMAND_4700_DATA[0]  = 0x00000044 ;
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x00000060 ;
        
        if(Signal_Status==PAM4_to_PAM4)
            //TXFIR_TAPS_PAM4_6TAP
            COMMAND_4700_DATA[3]  = 0x00000003 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH2) << 16 ;
        else
            //TXFIR_TAPS_NRZ_6TAP
            COMMAND_4700_DATA[3]  = 0x00000001 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH2) << 16 ;
        
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH2) << 16  
                                           | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH2) ;
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST2_CH2) << 16 
                                           | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH2) ;
        COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST3_CH2) ;
        COMMAND_4700_DATA[7]  = 0x00000000 ;
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x00008012 );
    }
    else if(Lane_CH==3)
    {
        COMMAND_4700_DATA[0]  = 0x00000044 ;
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x00000060 ;
        
        if(Signal_Status==PAM4_to_PAM4)
            //TXFIR_TAPS_PAM4_6TAP
            COMMAND_4700_DATA[3]  = 0x00000003 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH3) << 16 ;
        else
            //TXFIR_TAPS_NRZ_6TAP
            COMMAND_4700_DATA[3]  = 0x00000001 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH3) << 16 ;
        
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH3) << 16  
                                           | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH3) ;
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST2_CH3) << 16 
                                           | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH3) ;
        COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST3_CH3) ;
        COMMAND_4700_DATA[7]  = 0x00000000 ;
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x00008012 );
    }
    DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84);
    DSP87540_CAPI_Command_response(0x5201CA8C,0x5201CA90);    
}

//-----------------------------------------------------------------------------------------------------//
// DSP87540 Line Side TRX Polarity
// TRX_Side_SEL = 1  System side Tx
// TRX_Side_SEL = 0  System side Rx
//-----------------------------------------------------------------------------------------------------//
void DSP87540_LineSide_TRX_Polarity_SET(uint8_t Lane_CH , uint8_t TRX_Side_SEL)
{
    if(Lane_CH==0)
    {
        COMMAND_4700_DATA[0] = 0x00000002;
        
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_TX_Polarity_CH0);
        else
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH0);
        
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x00008004 );
    }
    else if(Lane_CH==1)
    {
        COMMAND_4700_DATA[0] = 0x00010002;
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_TX_Polarity_CH1);
        else
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH1);
        
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x00008004 );        
    }
    else if(Lane_CH==2)
    {
        COMMAND_4700_DATA[0] = 0x00020002;
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_TX_Polarity_CH2);
        else
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH2);
        
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x00008004 );      
    }
    else if(Lane_CH==3)
    {
        COMMAND_4700_DATA[0] = 0x00030002;
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_TX_Polarity_CH3);
        else
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH3);
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x00008004 );  
    }  
    DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94);
    DSP87540_CAPI_Command_response(0x5201CA9C,0x5201CAA0);
}

//-----------------------------------------------------------------------------------------------------//
// DSP87540 Line Side TX FIR Setting
//-----------------------------------------------------------------------------------------------------//
void DSP87540_LineSide_TX_FIR_SET( uint8_t Lane_CH )
{
    uint8_t Level_Shift_Enable=1;
    
    //If Line side optical is NRZ, do not set level shift
    if(Signal_Status==NRZ_to_NRZ)
    {
        Level_Shift_Enable=0;
    }

    if(Lane_CH==0)
    {      
        COMMAND_4700_DATA[0]  = 0x00000044 ;
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x00000079 ;
        
        if(Level_Shift_Enable)
            //TXFIR_TAPS_PAM4_6TAP
            COMMAND_4700_DATA[3]  = 0x00000003 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH0) << 16 ;
        else
            //TXFIR_TAPS_NRZ_6TAP
            COMMAND_4700_DATA[3]  = 0x00000001 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH0) << 16 ;
            
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_Main_CH0) << 16  
                                           | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE1_CH0) ;
        
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST2_CH0) << 16 
                                           | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST1_CH0) ;
        
        if(Level_Shift_Enable)
        {
            COMMAND_4700_DATA[6]  = 0x00000000 | BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_00_CH0 << 24 
                                               | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH0) ;
            
            COMMAND_4700_DATA[7]  = 0x01000000 | BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_01_CH0 
                                               | BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_10_CH0 << 8 
                                               | BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_11_CH0 << 16;
        }
        else
        {
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH0) ;
            COMMAND_4700_DATA[7]  = 0x01000000 ;
        }
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x00008012 );
    }
    else if(Lane_CH==1)
    {      
        COMMAND_4700_DATA[0]  = 0x00000044 ;
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x00000079 ; 

        if(Level_Shift_Enable)
            //TXFIR_TAPS_PAM4_6TAP
            COMMAND_4700_DATA[3]  = 0x00000003 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH1) << 16 ;
        else
            //TXFIR_TAPS_NRZ_6TAP
            COMMAND_4700_DATA[3]  = 0x00000001 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH1) << 16 ;
        
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_Main_CH1) << 16  
                                           | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE1_CH1) ;
        
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST2_CH1) << 16 
                                           | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST1_CH1) ;
        if(Level_Shift_Enable)
        {
            COMMAND_4700_DATA[6]  = 0x00000000 | BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_00_CH1 << 24 
                                               | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH1) ;
            
            COMMAND_4700_DATA[7]  = 0x01000000 | BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_01_CH1 
                                               | BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_10_CH1 << 8 
                                               | BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_11_CH1 << 16;
        }
        else
        {
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH1) ;
            COMMAND_4700_DATA[7]  = 0x01000000 ;
        }
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x00008012 );
    }
    else if(Lane_CH==2)
    {      
        COMMAND_4700_DATA[0]  = 0x00000044 ;
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x00000079 ; 
        
        if(Level_Shift_Enable)
            //TXFIR_TAPS_PAM4_6TAP
            COMMAND_4700_DATA[3]  = 0x00000003 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH2) << 16 ;
        else
            //TXFIR_TAPS_NRZ_6TAP
            COMMAND_4700_DATA[3]  = 0x00000001 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH2) << 16 ;
        
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_Main_CH2) << 16  
                                           | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE1_CH2) ;
        
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST2_CH2) << 16 
                                           | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST1_CH2) ;
        if(Level_Shift_Enable)
        {
            COMMAND_4700_DATA[6]  = 0x00000000 | BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_00_CH2 << 24 
                                               | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH2) ;
            
            COMMAND_4700_DATA[7]  = 0x01000000 | BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_01_CH2 
                                               | BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_10_CH2 << 8 
                                               | BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_11_CH2 << 16;
        }
        else
        {
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH2) ;
            COMMAND_4700_DATA[7]  = 0x01000000 ;
        }
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x00008012 );
    }
    else if(Lane_CH==3)
    {      
        COMMAND_4700_DATA[0]  = 0x00000044 ;
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x00000079 ; 
        
        if(Level_Shift_Enable)
            //TXFIR_TAPS_PAM4_6TAP
            COMMAND_4700_DATA[3]  = 0x00000003 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH3) << 16 ;
        else
            //TXFIR_TAPS_NRZ_6TAP
            COMMAND_4700_DATA[3]  = 0x00000001 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH3) << 16 ;
        
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_Main_CH3) << 16  
                                           | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE1_CH3) ;
        
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST2_CH3) << 16 
                                           | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST1_CH3) ;
        if(Level_Shift_Enable)
        {
            COMMAND_4700_DATA[6]  = 0x00000000 | BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_00_CH3 << 24 
                                               | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH3) ;
            
            COMMAND_4700_DATA[7]  = 0x01000000 | BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_01_CH3 
                                               | BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_10_CH3 << 8 
                                               | BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_11_CH3 << 16;
        }
        else
        {
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH3) ;
            COMMAND_4700_DATA[7]  = 0x01000000 ;
        }
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x00008012 );
    }

    DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94);
    DSP87540_CAPI_Command_response(0x5201CA9C,0x5201CAA0);    
}

//----------------------------------------------------------------------------------//
// DSP87540 Line_Side_ALL_CH14_Control_P86
//----------------------------------------------------------------------------------//
void Line_Side_ALL_CH1_CH4_Control_P86()
{
    DSP87540_LineSide_TRX_Polarity_SET( 0 , TX_Side );
    DSP87540_LineSide_TRX_Polarity_SET( 1 , TX_Side );
    DSP87540_LineSide_TRX_Polarity_SET( 2 , TX_Side );
    DSP87540_LineSide_TRX_Polarity_SET( 3 , TX_Side );
    
    DSP87540_LineSide_TRX_Polarity_SET( 0 , RX_Side );
    DSP87540_LineSide_TRX_Polarity_SET( 1 , RX_Side );
    DSP87540_LineSide_TRX_Polarity_SET( 2 , RX_Side );
    DSP87540_LineSide_TRX_Polarity_SET( 3 , RX_Side );
    
    DSP87540_LineSide_TX_FIR_SET(0);
    DSP87540_LineSide_TX_FIR_SET(1);
    DSP87540_LineSide_TX_FIR_SET(2);
    DSP87540_LineSide_TX_FIR_SET(3);
}
//----------------------------------------------------------------------------------//
// DSP87540 System_Side_ALL_CH1_CH4_Control_P87
//----------------------------------------------------------------------------------//
void System_Side_ALL_CH1_CH4_Control_P87()
{
    DSP87540_SystemSide_TRX_Polarity_SET( 0 , TX_Side );   
    DSP87540_SystemSide_TRX_Polarity_SET( 1 , TX_Side );
    DSP87540_SystemSide_TRX_Polarity_SET( 2 , TX_Side );
    DSP87540_SystemSide_TRX_Polarity_SET( 3 , TX_Side );
    
    DSP87540_SystemSide_TRX_Polarity_SET( 0 , RX_Side );   
    DSP87540_SystemSide_TRX_Polarity_SET( 1 , RX_Side );
    DSP87540_SystemSide_TRX_Polarity_SET( 2 , RX_Side );
    DSP87540_SystemSide_TRX_Polarity_SET( 3 , RX_Side );
    
    DSP87540_SystemSide_TX_FIR_SET( 0 );
    DSP87540_SystemSide_TX_FIR_SET( 1 );
    DSP87540_SystemSide_TX_FIR_SET( 2 );
    DSP87540_SystemSide_TX_FIR_SET( 3 );
    
}
//----------------------------------------------------------------------------------//
// Trigger_CMD_Update_DSP_REG
//----------------------------------------------------------------------------------//
void Trigger_CMD_Update_DSP_REG()
{
	Line_Side_ALL_CH1_CH4_Control_P86();
	System_Side_ALL_CH1_CH4_Control_P87();
//  DSP87540_LineSide_TRX_Polarity_SET( 0 , TX_Side );
}

void DSP87540_EEPROM_AVS_UcReady_CHECK()
{
	uint16_t  JJ_Count = 0 ;
	uint16_t  II_Count = 0 ;
    uint32_t  DataBuffer = 0 ;
    while(1)
    {
        DataBuffer = BRCM_Control_READ_Data(0x5201D4A0);
        // AVS & EEPROM Download & uMIC Ready value is 0x0B
        if( DataBuffer == 0x0B )
        {
            DSP_Ready_Flag = 1;
            break;
        }
        else
        {
            JJ_Count++;
            delay_1ms(10);
            
            if( JJ_Count >= 2000 )
            {
                JJ_Count = 0 ;
                DSP_Ready_Flag = 0 ;
                break;
            }
        }
    }   

}

//-----------------------------------------------------------------------------------------------------//
// Get DSP87540 temperature 
//-----------------------------------------------------------------------------------------------------/
uint16_t GET_DSP87540_Temperature()
{
    uint32_t ReadDSP_Buffer ;    
    uint16_t GET_Temperature = 0 ;

    ReadDSP_Buffer = BRCM_Control_READ_Data(0x5201C800);
    // Check bit15 = 1 Temperature data is correct
    // Alibaba Byte70-71 DSP dia temperature
    GET_Temperature = ReadDSP_Buffer ;
    
    return GET_Temperature;
}
//-----------------------------------------------------------------------------------------------------//
// DSP 87540 CHIP MODE
//-----------------------------------------------------------------------------------------------------//
void SET_CHIP_MODE( uint8_t DSP_CHIP_MODE )
{
	uint16_t Read_Buffer;
	uint16_t Break_count = 0 ;
	uint16_t ChipMode_CHECK;
    //mode1
	if( DSP_CHIP_MODE == Mode1_4x53G_PAM4_4x53G_PAM4_1PORT )
	{
        // Step1 : DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW
        // Step2 : DSP87540_CAPI_Command_response
        // Step3 : Delay 20 ms
        // Step4 : is_chip_mode_config_in_progress >= 200
        // Step5 : port_mode_cfg_log if port_mode_cfg_log != 0xF =>FW report the CHIP MODE CONFIG fail
        // Step6 : line_lane_cfg_done
        // Step7 : host_lane_cfg_done
        // COMMAND Vaule definition
        // 4700 Data Size
        // 4704 Function mode & refernce CLK
        // 4708 Mux_type & FEC Term
        // 470C Host FEC type & Line FEC Type
        // 4710 LW_BR & BH_BR
        // 4714 Line_Lane_Mask & Line_Lane_modulation
        // 4718 Line_Lane_Mask & Line_Lane_modulation
        // 471C Power Down & Status
        COMMAND_4700_DATA[0] = 0x0000001C;
        COMMAND_4700_DATA[1] = 0x00020000;
        COMMAND_4700_DATA[2] = 0x00000000;
        COMMAND_4700_DATA[3] = 0x00000000;
        COMMAND_4700_DATA[4] = 0x00000000;
        COMMAND_4700_DATA[5] = 0x000F0001;
        COMMAND_4700_DATA[6] = 0x000F0001;
        COMMAND_4700_DATA[7] = 0x00000002;  
        DSP87540_CAPI_CW_CMD( 0x5201CA78 , 0x0000800F , 0x5201CA74 , 0x00008008 );
        DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74);
        DSP87540_CAPI_Command_response(0x5201CA7C,0x5201CA80);
        delay_1ms(100);
        BRCM_Control_READ_Data(0x5201D4AC);
        BRCM_Control_READ_Data(0x5201D480);
        BRCM_Control_READ_Data(0x5201D480);
        DSP87540_line_lane_cfg_done(DSP_CHIP_MODE);
        DSP87540_host_lane_cfg_done();
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        System_Side_ALL_CH1_CH4_Control_P87();
    }
    //mode2
    else if ( DSP_CHIP_MODE == Mode2_2x53G_PAM4_2x53G_PAM4_2PORT )
    {
        COMMAND_4700_DATA[0] = 0x0000001C;
        COMMAND_4700_DATA[1] = 0x00030000;
        COMMAND_4700_DATA[2] = 0x00000000;
        COMMAND_4700_DATA[3] = 0x00000000;
        COMMAND_4700_DATA[4] = 0x00000000;
        COMMAND_4700_DATA[5] = 0x000F0001;
        COMMAND_4700_DATA[6] = 0x000F0001;
        COMMAND_4700_DATA[7] = 0x00000002; 
        DSP87540_CAPI_CW_CMD( 0x5201CA78 , 0x0000800F , 0x5201CA74 , 0x00008008 );
        DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74);
        DSP87540_CAPI_Command_response(0x5201CA7C,0x5201CA80);
        delay_1ms(100);
        BRCM_Control_READ_Data(0x5201D4AC);
        BRCM_Control_READ_Data(0x5201D480);
        BRCM_Control_READ_Data(0x5201D480);
        DSP87540_line_lane_cfg_done(DSP_CHIP_MODE);
        DSP87540_host_lane_cfg_done();
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        System_Side_ALL_CH1_CH4_Control_P87();
    }
    //mode3
    else if ( DSP_CHIP_MODE == Mode3_4x25G_NRZ_4x25G_NRZ_1PORT )
    {
        COMMAND_4700_DATA[0] = 0x0000001C;
        COMMAND_4700_DATA[1] = 0x00030000;
        COMMAND_4700_DATA[2] = 0x00000000;
        COMMAND_4700_DATA[3] = 0x00000000;
        COMMAND_4700_DATA[4] = 0x00020002;
        COMMAND_4700_DATA[5] = 0x000F0000;
        COMMAND_4700_DATA[6] = 0x000F0000;
        COMMAND_4700_DATA[7] = 0x00000002; 
        DSP87540_CAPI_CW_CMD( 0x5201CA78 , 0x0000800F , 0x5201CA74 , 0x00008008 );
        DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74);
        DSP87540_CAPI_Command_response(0x5201CA7C,0x5201CA80);
        delay_1ms(100);
        BRCM_Control_READ_Data(0x5201D4AC);
        BRCM_Control_READ_Data(0x5201D480);
        BRCM_Control_READ_Data(0x5201D480);
        DSP87540_line_lane_cfg_done(DSP_CHIP_MODE);
        DSP87540_host_lane_cfg_done();
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        System_Side_ALL_CH1_CH4_Control_P87();
    }
    //mode4
    else if ( DSP_CHIP_MODE == Mode4_1x53G_PAM4_1x53G_PAM4_4PORT )
    {
        COMMAND_4700_DATA[0] = 0x0000001C;
        COMMAND_4700_DATA[1] = 0x00040000;
        COMMAND_4700_DATA[2] = 0x00000000;
        COMMAND_4700_DATA[3] = 0x00000000;
        COMMAND_4700_DATA[4] = 0x00000000;
        COMMAND_4700_DATA[5] = 0x000F0001;
        COMMAND_4700_DATA[6] = 0x000F0001;
        COMMAND_4700_DATA[7] = 0x00000002; 
        DSP87540_CAPI_CW_CMD( 0x5201CA78 , 0x0000800F , 0x5201CA74 , 0x00008008 );
        DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74);
        DSP87540_CAPI_Command_response(0x5201CA7C,0x5201CA80);
        delay_1ms(100);
        BRCM_Control_READ_Data(0x5201D4AC);
        BRCM_Control_READ_Data(0x5201D480);
        BRCM_Control_READ_Data(0x5201D480);
        DSP87540_line_lane_cfg_done(DSP_CHIP_MODE);
        DSP87540_host_lane_cfg_done();
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        System_Side_ALL_CH1_CH4_Control_P87();
    }
    //mode5
    else if ( DSP_CHIP_MODE == Mode5_4x26G_NRZ_2x53G_PAM4_1PORT )
    {
        COMMAND_4700_DATA[0] = 0x0000001C;
        COMMAND_4700_DATA[1] = 0x00030000;
        COMMAND_4700_DATA[2] = 0x00000000;
        COMMAND_4700_DATA[3] = 0x00000000;
        COMMAND_4700_DATA[4] = 0x00000003;
        COMMAND_4700_DATA[5] = 0x00030001;
        COMMAND_4700_DATA[6] = 0x000F0000;
        COMMAND_4700_DATA[7] = 0x00000002; 
        DSP87540_CAPI_CW_CMD( 0x5201CA78 , 0x00008003 , 0x5201CA74 , 0x00008008 );
        DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74);
        DSP87540_CAPI_Command_response(0x5201CA7C,0x5201CA80);
        delay_1ms(100);
        BRCM_Control_READ_Data(0x5201D4AC);
        BRCM_Control_READ_Data(0x5201D480);
        BRCM_Control_READ_Data(0x5201D480);
        DSP87540_line_lane_cfg_done(DSP_CHIP_MODE);
        DSP87540_host_lane_cfg_done();
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        System_Side_ALL_CH1_CH4_Control_P87();
    }
    //mode8
    else if ( DSP_CHIP_MODE == Mode8_4X25G_NRZ_2X50G_PAM4_1PORT )
    {
        COMMAND_4700_DATA[0] = 0x0000001C;
        COMMAND_4700_DATA[1] = 0x00030000;
        COMMAND_4700_DATA[2] = 0x00000000;
        COMMAND_4700_DATA[3] = 0x00000000;
        COMMAND_4700_DATA[4] = 0x00010002;
        COMMAND_4700_DATA[5] = 0x00030001;
        COMMAND_4700_DATA[6] = 0x000F0000;
        COMMAND_4700_DATA[7] = 0x00000002; 
        DSP87540_CAPI_CW_CMD( 0x5201CA78 , 0x00008003 , 0x5201CA74 , 0x00008008 );
        DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74);
        DSP87540_CAPI_Command_response(0x5201CA7C,0x5201CA80);
        delay_1ms(100);
        BRCM_Control_READ_Data(0x5201D4AC);
        BRCM_Control_READ_Data(0x5201D480);
        BRCM_Control_READ_Data(0x5201D480);
        DSP87540_line_lane_cfg_done(DSP_CHIP_MODE);
        DSP87540_host_lane_cfg_done();
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        System_Side_ALL_CH1_CH4_Control_P87();
    }
    //mode9
    else if (( DSP_CHIP_MODE == Mode9_1x10G_NRZ_1x10G_NRZ_4PORT )||(DSP_CHIP_MODE == Mode9_1x10G_NRZ_1x10G_NRZ_1PORT))
    {
        COMMAND_4700_DATA[0] = 0x0000001C;
        COMMAND_4700_DATA[1] = 0x00060000;
        COMMAND_4700_DATA[2] = 0x00000000;
        COMMAND_4700_DATA[3] = 0x00000000;
        COMMAND_4700_DATA[4] = 0x00040004;
        COMMAND_4700_DATA[5] = 0x000F0000;
        COMMAND_4700_DATA[6] = 0x000F0000;
        COMMAND_4700_DATA[7] = 0x00000002; 
        DSP87540_CAPI_CW_CMD( 0x5201CA78 , 0x0000800F , 0x5201CA74 , 0x00008008 );
        DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74);
        DSP87540_CAPI_Command_response(0x5201CA7C,0x5201CA80);
        delay_1ms(100);
        BRCM_Control_READ_Data(0x5201D4AC);
        BRCM_Control_READ_Data(0x5201D480);
        BRCM_Control_READ_Data(0x5201D480);
        DSP87540_line_lane_cfg_done(DSP_CHIP_MODE);
        DSP87540_host_lane_cfg_done();
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        System_Side_ALL_CH1_CH4_Control_P87();
    }
    //mode12
    else if ( DSP_CHIP_MODE == Mode12_4x26G_NRZ_4x26G_NRZ_1PORT )
    {
        COMMAND_4700_DATA[0] = 0x0000001C;
        COMMAND_4700_DATA[1] = 0x00030000;
        COMMAND_4700_DATA[2] = 0x00000000;
        COMMAND_4700_DATA[3] = 0x00000000;
        COMMAND_4700_DATA[4] = 0x00030003;
        COMMAND_4700_DATA[5] = 0x00030000;
        COMMAND_4700_DATA[6] = 0x000F0000;
        COMMAND_4700_DATA[7] = 0x00000002; 
        DSP87540_CAPI_CW_CMD( 0x5201CA78 , 0x00008003 , 0x5201CA74 , 0x00008008 );
        DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74);
        DSP87540_CAPI_Command_response(0x5201CA7C,0x5201CA80);
        delay_1ms(100);
        BRCM_Control_READ_Data(0x5201D4AC);
        BRCM_Control_READ_Data(0x5201D480);
        BRCM_Control_READ_Data(0x5201D480);
        DSP87540_line_lane_cfg_done(DSP_CHIP_MODE);
        DSP87540_host_lane_cfg_done();
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        System_Side_ALL_CH1_CH4_Control_P87();
    }
    //mode13
    else if ( DSP_CHIP_MODE == Mode13_1x25G_NRZ_1x25G_NRZ_4PORT )
    {
        COMMAND_4700_DATA[0] = 0x0000001C;
        COMMAND_4700_DATA[1] = 0x00050000;
        COMMAND_4700_DATA[2] = 0x00000000;
        COMMAND_4700_DATA[3] = 0x00000000;
        COMMAND_4700_DATA[4] = 0x00020002;
        COMMAND_4700_DATA[5] = 0x000F0000;
        COMMAND_4700_DATA[6] = 0x000F0000;
        COMMAND_4700_DATA[7] = 0x00000002; 
        DSP87540_CAPI_CW_CMD( 0x5201CA78 , 0x0000800F , 0x5201CA74 , 0x00008008 );
        DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74);
        DSP87540_CAPI_Command_response(0x5201CA7C,0x5201CA80);
        delay_1ms(100);
        BRCM_Control_READ_Data(0x5201D4AC);
        BRCM_Control_READ_Data(0x5201D480);
        BRCM_Control_READ_Data(0x5201D480);
        DSP87540_line_lane_cfg_done(DSP_CHIP_MODE);
        DSP87540_host_lane_cfg_done();
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        System_Side_ALL_CH1_CH4_Control_P87();
    }
    //mode17
    else if ( DSP_CHIP_MODE == Mode17_4x25G_NRZ_2x53G_PAM4_1PORT )
    {
        COMMAND_4700_DATA[0] = 0x0000001C;
        COMMAND_4700_DATA[1] = 0x00030000;
        COMMAND_4700_DATA[2] = 0x00000004;
        COMMAND_4700_DATA[3] = 0x00010002;
        COMMAND_4700_DATA[4] = 0x00000002;
        COMMAND_4700_DATA[5] = 0x00030001;
        COMMAND_4700_DATA[6] = 0x000F0000;
        COMMAND_4700_DATA[7] = 0x00000002; 
        DSP87540_CAPI_CW_CMD( 0x5201CA78 , 0x00008003 , 0x5201CA74 , 0x00008008 );
        DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74);
        DSP87540_CAPI_Command_response(0x5201CA7C,0x5201CA80);
        delay_1ms(100);
        BRCM_Control_READ_Data(0x5201D4AC);
        BRCM_Control_READ_Data(0x5201D480);
        BRCM_Control_READ_Data(0x5201D480);
        DSP87540_line_lane_cfg_done(DSP_CHIP_MODE);
        DSP87540_host_lane_cfg_done();
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        System_Side_ALL_CH1_CH4_Control_P87();
    }
}

void LineSide_ALLCH_SQ(uint8_t EN_C)
{
    DSP87540_LineSide_TRX_Squelch_SET( 0 , RX_Side , EN_C );
    DSP87540_LineSide_TRX_Squelch_SET( 1 , RX_Side , EN_C );
    DSP87540_LineSide_TRX_Squelch_SET( 2 , RX_Side , EN_C );
    DSP87540_LineSide_TRX_Squelch_SET( 3 , RX_Side , EN_C );
}

//----------------------------------------------------------------------------------//
// DSP87540 Initialize 
//----------------------------------------------------------------------------------//
void DSP87540_Init(uint8_t DSP_CHIP_MODE)
{ 
    DSP87540_EEPROM_AVS_UcReady_CHECK();  
    if(DSP_Ready_Flag)
    {
        //DSP_MODE_SET=Get_Config_Chip_Mode();
        if((DSP_MODE_SET==Mode1_4x53G_PAM4_4x53G_PAM4_1PORT)&&(Error_code_Pass==0))
        {
            //LineSide_ALLCH_SQ( Function_EN );
            Line_Side_ALL_CH1_CH4_Control_P86();
            System_Side_ALL_CH1_CH4_Control_P87();
            //LineSide_ALLCH_SQ( Function_DIS );
            //Set Current Value for GUI to Read
        }
        else
        {
            SET_CHIP_MODE(DSP_MODE_SET);
        }
    }
}
